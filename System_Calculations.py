"""
---------------------------
    System_Calculations
---------------------------

Divirod, 8 Mar 17, M. Estes

Basic systems engineering calculations for the Divirod Soil Moisture Monitoring System
"""

from Constants import constants
import numpy as np
import scipy.special as spec
from scipy.interpolate import interp1d, interp2d
from math import pi, sqrt, sin, cos, tan
import itertools

c = constants()

class SystemCalcs(constants):
    """
    Class defines a number of methods that perform system model calculations
    """

    def calcSatelliteDistance (self, elevation):
        """
        Given an apparent elevation angle to the GPS satellite from a point on the ground, calculate the distance to the
        satellite.
        :param elevation:       Satellite elevation angle from point on the ground, numpy array of length N, 0.0 to <90.0 (deg)
        :return satDistance:    Distance between satellite and point on the the ground, numpy array of length N (m)
        """

        elevRad = c.degToRad * elevation

        #  Calculate angles...
        beta = np.arcsin(c.rEarth / (c.rEarth + c.gpsOrbitHeight) * np.cos(elevRad))
        alpha = np.pi / 2.0 - elevRad - beta

        #  Calculate satellite height at ground point...
        satDistance = (c.rEarth + c.gpsOrbitHeight) * np.sin(alpha) / np.cos(elevRad)

        return satDistance

    def calcGPSPowerAtGround (self, elevation):
        """
        Given an apparent elevation angle to the GPS satellite from a point on the ground, calculate the power incident
        on the receive antenna.
        :param elevation:       Satellite elevation angle from a point on the ground, numpy array of length N,
                                0.0 to <90.0 (deg)
        :return power:          Microwave power incident on receive antenna (dBm)
        """

        #  Calculate distance to satellite from ground...
        d = self.calcSatelliteDistance(elevation)

        #  Calculate microwave power loss between satellite and ground...
        lossdB = -10.0 * np.log10((4.0 * np.pi * d / c.gpsWavelength)**2.0)

        #  Calculate microwave power at receive antenna...
        power = c.gpsPower + lossdB

        return power

    def getReflectionSpot (self, h, elevation):
        """
        Given a receiver height above the ground and a satellite elevation angle, calculate the refelction spot dimensions
        on the ground and the horizontal distance between the receiver and the reflection spot center point.
        :param rcvrHeight:      Average height of the GPS receiver above the ground, numpy array of length N (m)
        :param elevation:       Satellite elevation angle from point on the ground, numpy array of length N (deg)
        :return spotParams:     List of spot parameters - [spot width (m), spot length (m), spot distance (m)], Nx3 numpy array
        """

        sinEL = np.sin(c.degToRad * elevation)
        tanEL = np.tan(c.degToRad * elevation)
        cosEL = np.cos(c.degToRad * elevation)

        #  Calculate horizontal distance between spot center point and receiver...
        d = h / tanEL

        #  Calculate distances between receiver and spot and satellite and spot...
        d1 = h / sinEL
        d2 = self.calcSatelliteDistance(elevation)

        #  Calculate width of first Fresnel zone (spot) transverse to line between spot and receiver...
        w = 2.0 * np.sqrt(c.gpsWavelength*d1*d2 / (d1+d2))

        #  Calculate length of first Fresnel zone along line between spot and receiver...
        w1 = np.sqrt(np.power(c.gpsWavelength / tanEL, 2.0) + 4.0*c.gpsWavelength*d) - c.gpsWavelength/tanEL
        w2 = np.sqrt(np.power(c.gpsWavelength / tanEL, 2.0) + 4.0*c.gpsWavelength*d) + c.gpsWavelength/tanEL
        l1 = 0.5 * w1 / sinEL
        l2 = 0.5 * w2 / sinEL

        #  Calculate min and max angles from receiver to l1 and l2...
        theta1 = np.arctan(sinEL / (cosEL - l1/h))
        theta2 = np.arctan(sinEL / (cosEL + l2/h))

        results = np.zeros((elevation.shape[0], 6))
        results[:, 0] = w
        results[:, 1] = l1
        results[:, 2] = l2
        results[:, 3] = d
        results[:, 4] = theta1
        results[:, 5] = theta2

        return results

    def getReflectionElevs (self, h, elevation):
        """
        Given a satellite elevation angle, calculates and returns the minimum and maximum elevation angles of the
        Fresnel reflection spot.
        :param h:               Receiver mean height above ground; scalar (m)
        :param elevation:       Satellite elevation angle; numpy array of length N (deg)
        :return minEL, maxEL:   Minimum and maximum elevation angles of Fresnel reflection spot; numpy arrays
                                of length N (deg)
        """

        sinEL = np.sin(c.degToRad * elevation)
        cosEL = np.cos(c.degToRad * elevation)
        w = self.getReflectionSpot(h, elevation)
        minEL = c.radToDeg * np.arctan(sinEL / (cosEL - w/h))
        maxEL = c.radToDeg * np.arctan(sinEL / (cosEL + w/h))

        return minEL, maxEL

    def getReflectionWindow (self, h, elevation):
        """
        Calculates and returns weighting windows for electric field amplitude (not power) at each satellite
        elevation angle.  Window length is equal to the length of the elevation parameter array.
        :param h:               Receiver mean height above ground; scalar (m)
        :param elevation:       Satellite elevation angle; numpy array of length N (deg)
        :return windows:        Field amplitude weighting windows (0.0.. 1.0) for each satellite elevation; numpy
                                array of size NxN
        """

        numPts = elevation.shape[0]
        windows = np.ones((numPts, numPts))

        for e in elevation:
            minEL, maxEL = self.getReflectionElevs(h, e)
            x1 = c.airyDiskZero * (elevation[elevation <= e] - e) / minEL
            x2 = c.airyDiskZero * (elevation[elevation > e] - e) / maxEL
            w1 = self.getAiryDiskAmplitude(x1)
            w2 = self.getAiryDiskAmplitude(x2)
            windows[e, :] = np.concatenate((w1, w2))

        return windows

    def calcAiryDisk (self, spotWidth, spotLength, x, y):
        """
        Given spot width (full width) and spot length (full length), and relative distances from spot center to edges of
        width and length (radial distances to a point, x,y), calculate and return the value of the airy disk function.
        :param spotWidth:       Full width (diameter) of elliptical airy disk spot, numpy array of length N (m)
        :param spotLength:      Full length (diameter) of elliptical airy disk spot, numpy array of length N (m)
        :param x:               Relative distance between spot center and width-wise edge of airy disk, numpy array of length N (m)
        :param y:               Relative distance between spot center and length-wise edge of airy disk, numpy array of length N (m)
        :return intensity:      Relative intensity of airy disk function at (x, y) --> intensity at (0, 0) = 1.0, numpy array of length N
        """

        #  Calculate scaling factors for width and length...
        widthScale = c.airyDiskZero / (0.5 * spotWidth)
        lengthScale = c.airyDiskZero / (0.5 * spotLength)

        xScaled = widthScale * x
        yScaled = lengthScale * y

        #  Calculate effective radius to (x, y)...
        r = np.sqrt(xScaled**2.0 + yScaled**2.0)

        nonZero = np.where(r > 0.)
        zero = np.where(r == 0.)

        intensity = np.zeros(r.shape[0])
        intensity[nonZero] = 4.0 * (spec.j1(r[nonZero]) / r[nonZero])**2.0
        intensity[zero] = 1.0

        return intensity


    def getAiryDiskAmplitude2 (self, h, elevation, x, y):
        """
        Calculate and return the amplitudes of the Airy Disk function - the reflection spot on the ground -
        over the range of x and y at a given elevation angle, receiver height.
        function.
        :param h:               Receiver mean height above ground; scalar (m)
        :param elevation:       Satellite elevation angle; scalar (deg)
        :param x:               Axis values along the ground perpendicular to the line-of-sight; numpy array
                                of length N (m)
        :param y:               Axis values along the ground along the line-of-sight; numpy array of length N
        :return amplitude:      Relative amplitude of airy disk function at (x, y) --> amplitude at (0, 0) = 1.0;
                                numpy array of size NxN
        """

        #  Calculate first Fresnel zone ellipse on ground...
        xy = np.asarray(list(itertools.product(x, y)))
        sinEL = np.sin(c.degToRad*elevation)
        tanEL = np.tan(c.degToRad*elevation)

        w1 = 0.5 * np.sqrt(c.gpsWavelength/tanEL**2.0 + 4.*c.gpsWavelength*h/sinEL) - 0.5*c.gpsWavelength/tanEL
        w2 = 0.5 * np.sqrt(c.gpsWavelength/tanEL**2.0 + 4.*c.gpsWavelength*h/sinEL) + 0.5*c.gpsWavelength/tanEL
        l1 = w1 / sinEL
        l2 = w2 / sinEL
        lavg = 0.5 * (l2 + l1)
        dl = 0.5 * (l2 - l1)
        w0 = np.sqrt(c.gpsWavelength*h/sinEL + c.gpsWavelength*dl*sinEL)

        #  Calculate radius length through (x,y) to edge of Fresnel zone (1st zero)...
        tol = 1.0E-9 * c.gpsWavelength
        with np.errstate(divide='ignore', invalid='ignore'):
            m = np.where((xy[:,0] > tol) | (xy[:,0] < -tol), xy[:,1]/xy[:,0], [0.])
        aa = np.power(1. / w0, 2.) + np.power(m / lavg, 2.)
        bb = -2. * m * dl / np.power(lavg, 2.)
        cc = np.power(dl / lavg, 2.) - 1.
        xe = np.where((xy[:,0] < tol) & (xy[:,0] > -tol), [0.], np.where(xy[:,0] >= 0.,
                                              (-0.5 * bb + 0.5 * np.sqrt(np.power(bb, 2.) - 4. * aa * cc)) / aa,
                                              (-0.5 * bb - 0.5 * np.sqrt(np.power(bb, 2.) - 4. * aa * cc)) / aa))
        ye = np.where((xy[:,0] > tol) | (xy[:,0] < -tol), m*xe, np.where(xy[:,1] < 0, [np.sign(xy[:,1])*(lavg-dl),
                                                                         np.sign(xy[:,1])*(lavg+dl)]))
        re = np.sqrt(np.power(xe, 2.) + np.power(ye, 2.))

        #  Calculate normalized radius to the point (x, y)...
        with np.errstate(divide='ignore', invalid='ignore'):
            r = c.airyDiskZero * np.sqrt(np.power(xy[:,0], 2.)+np.power(xy[:,1], 2.)) / re

        #  Break r array into zero and non-zero arrays...
        nonZero = np.where(r > 0.)
        zero = np.where(r == 0.)

        #  Calculate Airy function at (x, y)...
        amplitude = np.zeros(xy.shape[0])
        amplitude[nonZero] = 2. * spec.j1(r[nonZero]) / r[nonZero]
        amplitude[zero] = 1.

        return np.column_stack(np.split(amplitude, x.shape[0]))


    def getAiryDiskAmplitude (self, h, elevation, yx):
        """
        Calculate and return the amplitudes of the Airy Disk function - the reflection spot on the ground -
        over the range of x and y at a given elevation angle, receiver height.
        function.
        :param h:               Receiver mean height above ground; scalar (m)
        :param elevation:       Satellite elevation angle; scalar (deg)
        :param yx:              Array of (y, x) pairs; numpy array of size Nx2 (m, m)
        :return amplitude:      Relative amplitude of airy disk function at (x, y) --> amplitude at (0, 0) = 1.0;
                                numpy array of size NxN
        """

        #  Calculate first Fresnel zone ellipse on ground...
        sinEL = np.sin(c.degToRad*elevation)
        tanEL = np.tan(c.degToRad*elevation)
        y0 = h / tanEL

        w1 = 0.5 * np.sqrt(c.gpsWavelength/tanEL**2.0 + 4.*c.gpsWavelength*h/sinEL) - 0.5*c.gpsWavelength/tanEL
        w2 = 0.5 * np.sqrt(c.gpsWavelength/tanEL**2.0 + 4.*c.gpsWavelength*h/sinEL) + 0.5*c.gpsWavelength/tanEL
        l1 = w1 / sinEL
        l2 = w2 / sinEL
        lavg = 0.5 * (l2 + l1)
        dl = 0.5 * (l2 - l1)
        w0 = np.sqrt(c.gpsWavelength*h/sinEL + c.gpsWavelength*dl*sinEL)

        #  Calculate radius length through (x,y) to edge of Fresnel zone (1st zero)...
        tol = 1.0E-9 * c.gpsWavelength
        with np.errstate(divide='ignore', invalid='ignore'):
            m = np.where((yx[:,1] > tol) | (yx[:,1] < -tol), (yx[:,0]-y0)/yx[:,1], 0.)
        aa = np.power(1. / w0, 2.) + np.power(m / lavg, 2.)
        bb = -2. * m * dl / np.power(lavg, 2.)
        cc = np.power(dl / lavg, 2.) - 1.
        xe = np.where((yx[:,1] < tol) & (yx[:,1] > -tol), 0., np.where(yx[:,1] >= 0.,
                                              (-0.5 * bb + 0.5 * np.sqrt(np.power(bb, 2.) - 4. * aa * cc)) / aa,
                                              (-0.5 * bb - 0.5 * np.sqrt(np.power(bb, 2.) - 4. * aa * cc)) / aa))
        ye = np.where((yx[:,1] > tol) | (yx[:,1] < -tol), m*xe, np.where(yx[:,0] < 0, np.sign(yx[:,0])*(lavg-dl),
                                                                         np.sign(yx[:,0])*(lavg+dl)))
        re = np.sqrt(np.power(xe, 2.) + np.power(ye, 2.))

        #  Calculate normalized radius to the point (x, y)...
        with np.errstate(divide='ignore', invalid='ignore'):
            r = c.airyDiskZero * np.sqrt(np.power(yx[:,1], 2.) + np.power(yx[:,0]-y0, 2.)) / re

        #  Break r array into zero and non-zero arrays...
        nonZero = np.where(r > 0.)
        zero = np.where(r == 0.)

        #  Calculate Airy function at (x, y)...
        amplitude = np.zeros(yx.shape[0])
        amplitude[nonZero] = 2. * spec.j1(r[nonZero]) / r[nonZero]
        amplitude[zero] = 1.

        return np.column_stack(np.split(amplitude, np.unique(yx[:,0]).size))


    def getAiryDiskAutocorr (self, h, elevation):
        """
        Calculates the autocorrelation of the Airy Disk function at each elevation angle.
        :param h:
        :param elevation:
        :return:
        """

        #  Create x and y grid values...
        num_x = 400
        num_y = 400
        num_elev = elevation.shape[0]
        dtheta = 88.9 / num_y
        theta = c.degToRad * np.arange(0.1, 89., dtheta)

        i = np.arange(0, num_x)
        j = np.arange(0, num_y)
        ji = np.asarray(list(itertools.product(j, i)))
        yx = np.zeros(ji.shape)
        yx[:,0] = h / np.tan(theta[ji[:,0]])
        dx = (10./num_x) * np.sqrt(c.gpsWavelength*h/np.sin(theta[ji[:,0]]))
        yx[:,1] = ji[:,1] * dx
        dy = h / np.power(np.sin(theta[ji[:,0]]), 2.) * dtheta

        step = 1
        autocorr = np.zeros((num_elev, int(num_y/step)))
        for i in range(num_elev):
            f1 = self.getAiryDiskAmplitude(h, elevation[i], yx)
            norm = 0.5 * ((f1*f1)[1:,1:] + (f1*f1)[1:,:-1] + (f1*f1)[:-1,1:] + (f1*f1)[:-1,:-1]).sum()
            k = 0
            for j in range(0, num_y, step):
                f2 = self.getAiryDiskAmplitude(h, c.radToDeg*theta[j], yx)
                autocorr[i, k] = 0.5 * ((f1*f2)[1:,1:] + (f1*f2)[1:,:-1] + (f1*f2)[:-1,1:] + (f1*f2)[:-1,:-1]).sum() \
                                 * dx[j] * dy[j]
                autocorr[i, k] /= norm * dx[j] * dy[j]
                k += 1

        return autocorr


    def getAiryDiskAutocorr2 (self, d):
        """
        Calculates the autocorrelation of the 'jinc(x,y)' function (Airy disk) along one axis for
        the separation of the two jinc functions, d.  The calculation is in dimensionless space, (x,y), where
        jinc(0) = 1.0 and jinc(r) = 0 at r = 3.832.
        :param d:               Separation of the two 'jinc' functions; numpy array of length N
        :return autocorr:       Autocorrelation of the 'jinc' function at the separations, d; numpy array of
                                length N
        """

        #  Set constants...
        max_xy = 40. * c.airyDiskZero
        num_xy = 800
        tol = 1.0E-9 * c.airyDiskZero
        num_d = d.shape[0]

        #  Calculate autocorrelation at each separation, d...
        x = np.linspace(0., max_xy, num_xy)
        y = np.linspace(-max_xy, max_xy, 2*num_xy)
        dx = max_xy / num_xy
        dy = dx
        xy = np.asarray(list(itertools.product(x, y)))
        autocorr = np.zeros(num_d)
        r1 = np.sqrt(np.power(xy[:,0], 2.) + np.power(xy[:,1], 2.))

        for i in range(num_d):
            r2 = np.sqrt(np.power(xy[:,0], 2.) + np.power(xy[:,1]-d[i], 2.))
            f1 = np.where(r1 > tol, 2.*spec.j1(r1) / r1, [1.])
            f2 = np.where(r2 > tol, 2.*spec.j1(r2) / r2, [1.])
            autocorr[i] = 2.*(f1 * f2).sum() * dx * dy

        return autocorr / ((2. * f1 * f1).sum() * dx * dy)


    def calcAiryDiskPower (self, spotWidth, radius):
        """
        Given a reflection spot width (full width) and a radius, calculate the encircled power in the center portion of the
        airy disk out to the radius (in the spot width dimension).  Returned power is normalized to 1.0 (max).
        :param spotWidth:       Full width (diameter) of elliptical airy disk spot, numpy array of length N (m)
        :param radius:          Radius in spot width dimension with which to calculate encircled power, numpy array of length N (m)
        :return relPower:       Relative encircled power within radius, numpy array of length N
        """

        #  Calculate scaled radius...
        widthScale = c.airyDiskZero / (0.5 * spotWidth)
        rScaled = widthScale * radius

        #  Calculate encircled power out to radius...
        relPower = 1.0 - spec.j0([rScaled])**2.0 - spec.j1([rScaled])**2.0

        return relPower


    def findAiryDiskRadius (self, spotWidth, relPower):
        """
        Given an Airy disk pattern of full width, spotWidth, an a relative encircled power, relPower, calculate and
        return the radius at which the encircled power equals relPower
        :param spotWidth:       Full width (diameter) of Airy disk spot; scalar (m)
        :param relPower:        Relative encircled power (0.0 to 1.0) at which to find radius; scalar
        :return radius:         Radius at which encircled power equals relPower; scalar
        """

        #  Create array of x-y (radius-power) points from radius = 0 to 1 (normalized)...
        numPts = 101
        radii = np.linspace(0.0, 1.0, numPts)
        power = self.calcAiryDiskPower(spotWidth, 0.5 * radii * spotWidth)
        power /= power[numPts-1]

        #  Find radius points between which relative power lies...
        interpFunc = interp1d(power, radii)

        return 0.5 * spotWidth * interpFunc(relPower)

    def calcReflectionCoefs (self, epsilon, mu, depth, elevation):
        """
        Given a set of soil/vegetation dielectric permittivities (epsilon, complex), magnetic permeabilities (mu, complex)
        and associated layer depths along with a set of satellite elevation angles (incidence angle = elevation angle),
        calculate and return the s- and p-polarized field reflectivities at each elevation/incidence angle.  The upper
        layer is assumed to be air, infinitely thick, and is not included in the parameter values epsilon, mu, and depth
        :param epsilon:         Relative dielectric permittivities (complex) of each layer, starting with the top layer;
                                numpy array of length N.  The value of the last epsilon is assumed to be the underlying
                                soil with infinite depth
        :param mu:              Relative magnetic permeabilities (complex) of each layer, starting with the top layer;
                                numpy array of length N.  The value of the last mu is assumed to be the underlying soil
                                with infinite depth
        :param depth:           Depth/thickness of each layer, starting with top layer; numpy array of length N (m).  The
                                value of the last depth is ignored as this layers is assumed to be infinitely thick
        :param elevation:       Satellite elevation angle from point on the ground; numpy array of length M (deg)
        :return rs, rp:         Complex field reflection coefficients for s- and p-polarizations
        """

        #  Get number of layers, elevations...
        numLayers = epsilon.shape[0] + 1
        numElevs = elevation.shape[0]

        if numLayers != depth.shape[0] + 1:
            print('calcReflectionCoefs:  Error - Size of epsilon and depth arrays not equal.')
            return np.zeros(numElevs), np.zeros(numElevs)

        #  Calculate the complex index of refraction of each layer...
        n = np.zeros(numLayers, dtype=complex)
        n[0] = 1.0 + 0.0j
        n[1:] = np.lib.scimath.sqrt(epsilon * mu)

        #  Create new layer depth arrary whose size matches n...
        d = np.zeros(numLayers)
        d[0] = 0.0
        d[1:] = depth

        #  NOTE:  Propagation angles are relative to the normal of the surface/interface

        #  Calculate propagtion angle (rad) through each layer...
        theta = np.zeros((numElevs, numLayers))
        theta[:, 0] = c.degToRad * (90.0 - elevation)

        for i in range(1, numLayers):
            theta[:, i] = np.abs(np.arcsin(n[i-1].real / n[i].real * np.sin(theta[:, i-1])))

        #  Calculate transfer matrices for s- and p-polarizations...
        ident = np.identity(2)
        ms = np.ndarray(shape=(numElevs, 2, 2), dtype=complex)
        ms[:] = ident
        mp = np.ndarray(shape=(numElevs, 2, 2), dtype=complex)
        mp[:] = ident
        mTemp = np.zeros((numElevs, 2, 2), dtype=complex)
        k0 = 2.0 * pi / c.gpsWavelength
        p = np.ones(numLayers, dtype=complex)
        p[1:] = np.lib.scimath.sqrt(epsilon / mu)
        q = 1.0 / p

        for i in range(1, numLayers):
            cosTheta = np.cos(theta[:, i])
            #  s-polarization...
            mTemp[:, 0, 0] = np.cos(k0 * n[i] * d[i] * cosTheta)
            mTemp[:, 1, 1] = mTemp[:, 0, 0]
            mTemp[:, 0, 1] = -1.0j / (p[i] * cosTheta) * np.sin(k0 * n[i] * d[i] * cosTheta)
            mTemp[:, 1, 0] = -1.0j * (p[i] * cosTheta) * np.sin(k0 * n[i] * d[i] * cosTheta)
            ms = np.matmul(ms, mTemp)

            #  p-polarization...
            mTemp[:, 0, 1] = -1.0j / (q[i] * cosTheta) * np.sin(k0 * n[i] * d[i] * cosTheta)
            mTemp[:, 1, 0] = -1.0j * (q[i] * cosTheta) * np.sin(k0 * n[i] * d[i] * cosTheta)
            mp = np.matmul(mp, mTemp)

        #  Calculate reflectivities...
        if numLayers > 2:
            #  s-polarization...
            p1 = p[0] * np.cos(theta[:, 0])
            pl = p[numLayers-1] * np.cos(theta[:, numLayers-1])
            rsNum = (ms[:, 0, 0] + ms[:, 0, 1] * pl) * p1 - (ms[:, 1, 0] + ms[:, 1, 1] * pl)
            rsDenom = (ms[:, 0, 0] + ms[:, 0, 1] * pl) * p1 + (ms[:, 1, 0] + ms[:, 1, 1] * pl)
            rs = rsNum / rsDenom

            #  p-polarization...
            q1 = q[0] * np.cos(theta[:, 0])
            ql = q[numLayers-1] * np.cos(theta[:, numLayers-1])
            rpNum = (mp[:, 0, 0] + mp[:, 0, 1] * ql) * q1 - (mp[:, 1, 0] + mp[:, 1, 1] * ql)
            rpDenom = (mp[:, 0, 0] + mp[:, 0, 1] * ql) * q1 + (mp[:, 1, 0] + mp[:, 1, 1] * ql)
            rp = rpNum / rpDenom

        elif numLayers == 2:
            cosTheta1 = np.cos(theta[:, 0])
            cosTheta2 = np.cos(theta[:, 1])
            #  s-polarization...
            rs = (n[0] * cosTheta1 - n[1] * cosTheta2) / (n[0] * cosTheta1 + n[1] * cosTheta2)

            #  p-polarization...
            rp = (n[1] * cosTheta1 - n[0] * cosTheta2) / (n[1] * cosTheta1 + n[0] * cosTheta2)

        else:
            print('calcReflectionCoefs:  Error - numLayers < 2')
            return np.zeros(numElevs), np.zeros(numElevs)

        return rs, rp

    def dBmToWatts (self, dBmValue):
        """
        Given a power in dBm, convert it to linear Watts and return the value in Watts
        :param dBmvalue:        Scalar power value (dBm)
        :return watts:          Power in Watts (W)
        """

        #  Convert and return dBm value to linear Watts...

        return 10.0**((dBmValue - 30.0) / 10.0)

    def absoluteSquared (self, x):
        """
        Given a complex value x, calculate and return the absolute value squared of x
        :param x:               Complex value; scalar or numpy array of length N
        :return abs(x)**2.0:    Absolute value squared of x; scalar or numpy array of length N
        """
        #  Return absolute value squared...

        return x.real**2.0 + x.imag**2.0

    def calcFieldVector (self, incidentAngles, incidentFields):
        """
        Calculates the complex electric field vector in [x, y, z] space for an incident wave with propagation
        direction given by its azimuth and elevation angles.  The axes are as follows:
                    x-axis      Points east
                    y-axis      Points up
                    z-axis      Points south
        :param incidentAngles:  Azimuth and elevation angles for incident wave(s); numpy array of size Nx2 (deg)
        :param incidentFields:  Electric field (complex) of incident wave(s) in [s/TE, p/TM] orientations; numpy array
                                of size Nx2 (V/m)
        :return fieldVectors:   Electric field (complex) of incident wave(s) in [x, y, z] space; numpy array of size
                                Nx3(V/m)
        """

        #  Create output array...
        numFields = incidentFields.shape[0]
        fieldVectors = np.zeros(shape=(numFields, 3), dtype='complex')
        fieldVectors[:, 0] = incidentFields[:, 0] * np.cos(c.degToRad * incidentAngles[:, 0])
        fieldVectors[:, 1] = incidentFields[:, 1] * np.cos(c.degToRad * incidentAngles[:, 1])
        fieldVectors[:, 2] = incidentFields[:, 0] * np.sin(c.degToRad * incidentAngles[:, 0]) \
                            - incidentFields[:, 1] * np.sin(c.degToRad * incidentAngles[:, 1]) \
                               * np.cos(c.degToRad * incidentAngles[:, 0])

        return fieldVectors

    def calcAntennaSignal (self, incidentFields, antenna):
        """
        Given the a series of incident microwave fields (characterized by complex amplitudes for s- and p-polarizations,
        azimuth angles and elevation angles) and the antenna gain pattern (characterized by complex amplitude gains for
        s- and p-polarizations, azimuth angles and elevation angles) calculate and return the antenna output signal.
        :param incidentFields:  Electric field amplitudes in [x, y, z] space (complex, V/m)
        :param waves:           Electric field amplitudes (complex, Jones vector representation of s- and p-polarized
                                components) of incident waves; numpy array of length Nx4 [azimuth, elevation,
                                amplitude_s, amplitude_p] (deg, deg, V/m, V/m)
        :param antenna:         Antenna amplitude response vector in [x, y, z] space (complex)
        :param antennaGain:     Antenna AMPLITUDE gain/radiation pattern (complex, Jones vector representation)
                                at different absolute azimuth and elevation angles; numpy array of size Mx4 [azimuth,
                                elevation, gain_el, gain_az] (deg, deg, unitless, unitless)
        :param antennaPolarization:     Normalized antenna polarization response (complex, Jones vector);
                                        2x1 numpy array [s-polarization response, p-polarization response]
        :return signal:         Antenna output signal; scalar (proportional to volts)
        """

        #  Get incident wave propagation angles (AZ, EL) and field amplitudes (s, p)...
        numWaves = incidentFields.shape[0]      #  Number of incident waves
        angles = incidentFields[:, :2].real     #  Azimuth and elevation angles of incident waves
        fields = incidentFields[:, 2:]          #  s- and p-polarized electric field amplitudes of incident waves

        #  Get the [x, y, z] vectors of the incident electric fields...
        fieldVectors = self.calcFieldVector(angles, fields)

        #  Calculate the antenna output signals from the incident fields...
        signals = 0.0
        for i in range(numWaves):
            signals += np.vdot(fieldVectors[i], antenna)

        #  Sum all signals and return value...

        return signals

    def calcReceivedSNR (self, signal):
        """
        Given an antenna output signal (or sum of signals), calculate and return the received POWER signal-to-noise
        ratio (SNR).
        :param signal:          Net antenna output signal (complex); numpy array of length N (proportional to volts)
        :return snr:            Received SNR (dB)
        """

        #  Calculate signal power, convert power to dBm, and subtract noise power to get SNR (dB)...

        return 10.0 * np.log10(np.absolute(signal)**2.0) - c.receiverSensitivity

    def calcSoilEpsilon (self, sand, clay, moisture):
        """
        Uses Hallikainen's fit model to calculate soil permittivity as a function of % sand, % silt, % clay, and
        % moisture (VMC).
        :param sand:            Volume fraction sand in soil mixture; scalar
        :param clay:            Volume fraction clay in soil mixture; scalar
        :param moisuture:       Volume soil moisture content fraction; numpy array of length N
        :return epsilon:        Relative soil dielectric permittivity (complex); numpy array of length N
        """

        #  Zeroeth order term...
        eps0 = (c.ar0 + c.ar1 * sand + c.ar2 * clay) + 1.0j * (c.ai0 + c.ai1 * sand + c.ai2 * clay)

        #  First order term...
        eps1 = ((c.br0 + c.br1 * sand + c.br2 * clay) + 1.0j * (c.bi0 + c.bi1 * sand + c.bi2 * clay)) * moisture

        #  Second order term...
        eps2 = ((c.cr0 + c.cr1 * sand + c.cr2 * clay) + 1.0j * (c.ci0 + c.ci1 * sand + c.ci2 * clay)) * moisture ** 2.0

        #  Sum and return modeled soil permittivity...

        return eps0 + eps1 + eps2

    def calcVegetationEpsilon (self, bulkEpsilon, density):
        """
        Estimates and returns vegetation effective permittivity (epsilon) at a relative density to the given
        permittivity value.
        :param bulkEpsilon:     Baseline vegetation permittivity (relative, complex); scalar
        :param density:         Density relative to baseline at which to calculate effective epsilon; numpy array of
                                length N
        :return epsilon:        Effective relative permittivity of vegetation at relative density
        """

        #  Return bulk permittivity value scaled by relative density...

        return np.lib.scimath.power(bulkEpsilon, density)