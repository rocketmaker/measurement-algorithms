"""
--------------------
Process Test Data.py
--------------------

Divirod, 17 Mar 17, M. Estes

This script:
1)  Reads in GPS data from a test data file (.csv) into a pandas dataframe, gpsData:

    Columns:            Time    EL    AZ    SNR (s) SNR (p) SNR (rhc)   SNR (lhc)
    Row indexes:        Series #, Set #, Sample #   (pandas MultiIndex)

    where SNR data is in dB, EL and AZ are in deg, and Time in seconds

2)  Assumes that test data falls within the following criteria:

    3 deg < EL < 75 deg
    100 deg < AZ < 260 deg

3)  Linearizes (from dB) and fits the DC average of SNR data

4)  Corrects linear SNR for antenna gain effects

5)  Estimates the mean SNR fringe frequency (vs. sin(EL)) and calculates the nominal receiver height, H0

6)  Fits the SNR fringes to a sinusoid with time-varying DC offset, amplitude, and phase

7)  Stores the results

"""

import numpy as np
import pandas as pd
from scipy.interpolate import interp1d
import scipy.signal as sig
from scipy.optimize import minimize
from numpy import pi
from math import sin, cos, exp, asin
import matplotlib.pyplot as plt
from Constants import constants

c = constants()


def linearizeSNR (snrData):
    """
    Given one set of SNR data, convert the SNR data (in dB) into linearized form (ratio of powers).
    :param seriesNum:       series number of data
    :param setNum:          set number of data
    :param setData:         one set of dataframe containing SNR data
    :return: linearSet:     pandas dataframe of linearized SNR data
    """

    return np.power(10.0, snrData / 10.0)

def normalizeSNR (angleData, snrLinearData, antennaGain):
    """
    Normalizes SNR data (linearized POWER at various azimuth and elevation angles) to antenna gain at different
    azimuth and elevation angles.  Method assumes that elevation and azimuth patterns are independent
    :param angleData:       Azimuth and elevation angles for each SNR datapoint; numpy array of size Nx2
                            [azimuth, elevation] (deg, deg)
    :param snrLinearData:   Linearized SNR data (ratio of powers); numpy array of length N
    :param antennaGain:     Antenna POWER gain/radiation pattern (complex, Jones vector representation) at different
                            absolute azimuth and elevation angles; numpy array of size Mx4 [azimuth, elevation,
                            gain_el, gain_az] (deg, deg, unitless, unitless)
    :return normalized SNR: SNR normalized to antenna gain/radiation pattern
    """

    #  Interpolate to get antenna gain response at incident wave angles...
    fAz = interp1d(antennaGain[:, 0], antennaGain[:, 2])
    fEl = interp1d(antennaGain[:, 1], antennaGain[:, 3])

    antennaGainInterp = fAz(angleData[:, 0]) * np.sqrt(fEl(angleData[:, 1])) * np.sqrt(fEl(-angleData[:, 1]))

    return snrLinearData / antennaGainInterp

def fitSNRAvg(setData):
    """
    Given one set of linearized SNR data, fit average (DC) value of SNR over time to a polynomial function.
    :param seriesNum:       series number of data
    :param setNum:          set number of data
    :param setData:         one set of dataframe containing SNR data
    :return: dcAvg:         numpy array of avg (DC) SNR over time
    """

    #  Degree of fitting polynomial...
    deg = 3

    t = setData['Time']

    #  Calculate fit function...
    fitCoefs = np.polyfit(t, setData['SNR Linear'].values, deg)
    fitFunc = np.poly1d(fitCoefs)

    #  Calculate fit points...
    avgSNR = fitFunc(t)

    return avgSNR

def calcReflectionSpot (setData):
    """
    Given one set of linearized SNR data, calculate the Fresnel zone spot radius (from sensor), diameter (perpendicular
    to the radius), and length (along the radius).
    :param seriesNum:   series number of data
    :param setNum:      set number of data
    :param setData:     one set of dataframe containing SNR data
    :return: spotData:  a (numPts x 3) numpy array of spot distance, diameter, and length
    """

    #  Constants...
    h = 2.16  # Height of sensor (m)
    dSat = 2.02E+7  # Orbital height of satellite (m)
    c = 2.998E+8  # Speed of light (m/s)
    freq = 1.575E+9  # Frequency of GPS carrier (Hz)
    wavelength = c / freq  # Microwave wavelength (m)

    #  Calculate reflection spot distance (along ground)...
    rSpot = h / np.tan(pi / 180.0 * setData['EL Smooth'].values)

    #  Calculate reflection spot diameter and length...
    dSensor = h / np.sin(pi / 180.0 * setData['EL Smooth'].values)
    fzDia = 2.0 * np.sqrt(wavelength * dSat * dSensor / (dSat + dSensor))
    fzLen = fzDia / np.sin(pi / 180.0 * setData['EL Smooth'].values)

    numPts = setData.shape[0]
    spotData = np.zeros((numPts, 3))
    spotData[:, 0] = rSpot
    spotData[:, 1] = fzDia
    spotData[:, 2] = fzLen

    return spotData

def smooth (x, window_len=101, window='hanning'):
    """
    From:  http://scipy-cookbook.readthedocs.io/items/SignalSmooth.html

    Smooth the data using a window with requested size.

    This method is based on the convolution of a scaled window with the signal.
    The signal is prepared by introducing reflected copies of the signal
    (with the window size) in both ends so that transient parts are minimized
    in the begining and end part of the output signal.

    input:
        x: the input signal
        window_len: the dimension of the smoothing window; should be an odd integer
        window: the type of window from 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'
            flat window will produce a moving average smoothing.

    output:
        the smoothed signal

    example:

    t=linspace(-2,2,0.1)
    x=sin(t)+randn(len(t))*0.1
    y=smooth(x)

    see also:

    numpy.hanning, numpy.hamming, numpy.bartlett, numpy.blackman, numpy.convolve
    scipy.signal.lfilter

    TODO: the window parameter could be the window itself if an array instead of a string
    NOTE: length(output) != length(input), to correct this: return y[(window_len/2-1):-(window_len/2)] instead of just y.
    """

    if x.ndim != 1:
        raise ValueError("smooth only accepts 1 dimension arrays.")

    if x.size < window_len:
        raise ValueError("Input vector needs to be bigger than window size.")

    if window_len < 3:
        return x

    if not window in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
        raise ValueError("Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'")

    s = np.r_[x[window_len - 1:0:-1], x, x[-1:-window_len:-1]]
    # print(len(s))
    if window == 'flat':  # moving average
        w = np.ones(window_len, 'd')
    else:
        w = eval('numpy.' + window + '(window_len)')

    y = np.convolve(w / w.sum(), s, mode='valid')

    return y

def calcH0 (dataframe):
    """
    Given a data set, use the Lomb-Scargle periodogram method to calculate the frequency of max power and use this
    value to calculate the average sensor height, H0.
    :param rcvrHeight:  Receiver height for data set - acts as initial guess for h0; scalar (m)
    :param setData:     Dataframe containing SNR data
    :return: h0:        Mean receiver height/reflection depth; numpy array of size Nx4 [H0 (s), H0 (p), H0 (lhc),
                        H0 (rhc)] (m)
    """

    #  Initialize output arrays...
    h0 = np.zeros((0, 4))
    periodogram = np.zeros((0, 4))

    #  Get number of series in dataset...
    numSeries = int(max(dataframe.index.tolist())[0])

    for i in range(numSeries+1):

        #  Get number of sets in series i...
        numSets = int(max(dataframe.loc[i].index.tolist())[0])

        #  Process each set in dataframe to find h0...
        for j in range(numSets+1):

            #  Frequency constants - frequency is in rad/unit sin(EL), where -1.0 < sin(EL) < 1.0...
            fGuess = 4.0 * pi * dataframe.loc[(i, j, 0), 'Receiver Height'] / c.gpsWavelength
            x = 0.6
            fMin = fGuess - x * fGuess
            fMax = fGuess + x * fGuess
            numPts = dataframe.loc[(i, j)].shape[0]
            fStep = 2.0 * x * fGuess / (numPts - 1)

            #  Set up data arrays...
            sinEL = np.sin(c.degToRad * dataframe.loc[(i, j), 'EL'].values)
            freq = np.arange(fMin, fMax, fStep)
            power_s = dataframe.loc[(i, j), 'SNR Normalized (s)'].values
            power_p = dataframe.loc[(i, j), 'SNR Normalized (p)'].values
            power_lhc = dataframe.loc[(i, j), 'SNR Normalized (lhc)'].values
            power_rhc = dataframe.loc[(i, j), 'SNR Normalized (rhc)'].values

            #  Check for flat elevation data (return if so)...
            if sinEL.min() == sinEL.max():
                return np.nan

            # Get periodogram...
            periodogramSet = np.zeros((numPts, 4))
            periodogramSet[:, 0] = sig.lombscargle(sinEL, power_s, freq)
            periodogramSet[:, 1] = sig.lombscargle(sinEL, power_p, freq)
            periodogramSet[:, 2] = sig.lombscargle(sinEL, power_lhc, freq)
            periodogramSet[:, 3] = sig.lombscargle(sinEL, power_rhc, freq)


            #  Find peak frequency...
            iMax_s = np.argmax(periodogramSet[:, 0])
            maxFreq_s = freq[iMax_s]
            iMax_p = np.argmax( periodogramSet[:, 1])
            maxFreq_p = freq[iMax_p]
            iMax_lhc = np.argmax(periodogramSet[:, 2])
            maxFreq_lhc = freq[iMax_lhc]
            iMax_rhc = np.argmax(periodogramSet[:, 3])
            maxFreq_rhc = freq[iMax_rhc]

            #  Calculate H0...
            h0Set = np.ndarray((numPts, 4))
            h0Set[:, 0] = maxFreq_s * c.gpsWavelength / (4.0 * pi)
            h0Set[:, 1] = maxFreq_p * c.gpsWavelength / (4.0 * pi)
            h0Set[:, 2] = maxFreq_lhc * c.gpsWavelength / (4.0 * pi)
            h0Set[:, 3] = maxFreq_rhc * c.gpsWavelength / (4.0 * pi)

            h0 = np.concatenate((h0, h0Set))
            periodogram = np.concatenate((periodogram, periodogramSet))

    return h0, periodogram

def calcHeff (xData, yData, window):
    """
    Given a data chunk (subset of set, use the Lomb-Scargle periodogram method to calculate the effective frequency of
    max power and use this value to calculate the effective sensor height, Heff.
    :param xData:           numpy array (numPts x 1) of smoothed elevation data
    :param yData:           numpy array (numPts x 1) of smoothed, zero-offset linear SNR data
    :return: Heff:          numpy array (numPts x 1) of H0 values (one value per data point)
    """
    #  Constants...
    aMin = 1
    aMax = 200
    aStep = 0.1
    c = 2.998E+8  # Speed of light (m/s)
    freq = 1.575E+9  # Frequency of GPS carrier (Hz)
    wavelength = c / freq  # Microwave wavelength (m)
    degToRad = pi / 180.0  # Converts angle in degrees to radians

    #  Set up data arrays...
    sinEL = np.sin(degToRad * xData)
    power = yData
    freq = np.arange(aMin, aMax, aStep)

    #  Check for flat elevation data (return if so)...
    if sinEL.min() == sinEL.max():
        return np.nan

    # Get periodogram...
    pdGram = sig.lombscargle(sinEL, power, freq)

    #  Find peak frequency...
    iMax = np.argmax(pdGram)
    fMax = freq[iMax]

    #  Calculate Heff...
    Heff = fMax * wavelength / (4.0 * pi)

    return Heff

def sinusoidModel (x, amp, freq, phase):
    """
    Given an array of independent values (x) and parameters (amp, freq, phase), computes a sinusoid.
    :return:    An array of dependent (y) values
    """

    return amp * np.sin(freq * x + phase)

def calcFitChiSq (guess, xData, yData, freq):
    """

    :param xData:
    :param yData:
    :return:
    """

    #  Calculate model fit...
    amp = guess[0]
    phase = guess[1]
    yModel = sinusoidModel(xData, amp, freq, phase)

    #  Calculate chi squared...
    numPts = yData.shape[0]
    chiSq = np.sum((yModel - yData) ** 2) / numPts

    return chiSq

def calcAmpPhaseFits (setData):
    """
    Given a set of data, calculates the nonlinear fit of the linear SNR vs. sin(EL) to sinusoidal segments.  Calculates
    sinusoid amplitude and phase as fitting parameters at each data point, and fits segments of one wavelength.
    :param seriesNum:   series number of data
    :param setNum:      set number of data
    :param setData:     one set of dataframe containing SNR data
    :return:  fitData:  a (numPts x 2) ndarray with best fit amplitude and phase at each data point
    """

    #  Constants...
    c = 2.998E+8  # Speed of light (m/s)
    freq = 1.575E+9  # Frequency of GPS carrier (Hz)
    wavelength = c / freq  # Microwave wavelength (m)
    degToRad = pi / 180.0  # Converts angle in degrees to radians
    radToDeg = 1.0 / degToRad  # Converts angle in radians to degrees
    ampMin = 1.0  # Linear sinusoidal fringe amplitude, upper bound for fit (V)
    ampMax = 100.  # Linear sinusoidal fringe amplitude, upper bound for fit (V)
    freqMin = 100.  # Fringe frequency, lower bound for fit (rad/sin(EL))
    freqMax = 200.  # Fringe frequency, upper bound for fit (rad/sin(EL))
    phaseMin = -pi  # Fringe phase, lower bound for fit (rad)
    phaseMax = pi  # Fringe phase, upper bound for fit (rad)

    #  Get average interferogram frequency from H0...
    avgFreq = 4.0 * pi * setData['H0'].mean() / wavelength

    #  Calculate fringe wavelength (in units of sinEL)...
    # waveFringe = wavelength * sinEL / (setData['H0'].values * (1.0 - np.cos(2.0 * degToRad
    #                       * setData['EL Smooth'].values)))
    waveFringe = 0.06
    freqFringe = 1.0 / waveFringe

    #  Find start and end points (1/2 waveFringe from each end of data set)...
    minEL = setData['EL Smooth'].min()
    maxEL = setData['EL Smooth'].max()
    startElev = radToDeg * asin(sin(degToRad * minEL) + 0.5 * waveFringe)
    endElev = radToDeg * asin(sin(degToRad * maxEL) - 0.5 * waveFringe)
    startPt = (np.abs(setData['EL Smooth'] - startElev)).argmin()
    endPt = (np.abs(setData['EL Smooth'] - endElev)).argmin()

    #  Make startPt the first point in time...
    if startPt > endPt:
        start = endPt
        endPt = startPt
        startPt = start

    # Check if data set is less than one fringe wavelength long...
    if (sin(degToRad * maxEL) - sin(degToRad * minEL)) < waveFringe:
        print('calcAmpPhaseFits:  EL range too small')
        return np.nan

    # Loop over data, every n points, fitting one fringe cycle at each nth point...

    #  Set up data arrays for processing...
    n = 10
    numPts = setData.shape[0]
    numJ = int(numPts / float(n))
    firstPt = setData.index.get_level_values(0).min()
    fitParams = np.zeros((numPts, 2))
    yFit = np.zeros((numJ, 1))
    chiSq = np.zeros((numJ, 1))
    heff = np.zeros((numJ, 1))
    j = 0

    for i in range(startPt, endPt + 1, n):

        #  Data index doesn't start at 0 - create index that does...
        # j = i - setData.index.get_level_values(0).min()

        #  Calculate first and last point of the data segment to fit...
        currentElev = setData.loc[(i), 'EL Smooth']
        startElev = radToDeg * asin(sin(degToRad * currentElev) - 0.5 * waveFringe)
        endElev = radToDeg * asin(sin(degToRad * currentElev) + 0.5 * waveFringe)
        startFringe = (np.abs(setData['EL Smooth'] - startElev)).argmin()
        endFringe = (np.abs(setData['EL Smooth'] - endElev)).argmin()

        #  Make startPt the first point in time...
        if startFringe > endFringe:
            start = endFringe
            endFringe = startFringe
            startFringe = start

        # Get x and y data to fit...
        data = setData.loc[startFringe:endFringe]
        xData = np.sin(degToRad * data['EL Smooth'].values)
        yData = data['SNR Smooth'].values - data['SNR Average'].values
        xmidPt = int(0.5 * (endFringe - startFringe))
        # heff[j] = calcHeff(xData, yData)
        # localFreq = 4.0 * pi * heff[j] / wavelength

        #  Set initial guess at fit parameters (amp, freq, phase)...
        if np.sum(fitParams[j - 1]) != 0 and j >= 1:
            initialGuess = fitParams[j - 1]
        else:
            if yData[xmidPt] > 0.0:
                phase = pi / 2.0
            else:
                phase = -pi / 2.0
            initialGuess = np.array([40.0, phase])

        # Fit data to sinusoidal model...
        try:
            res = minimize(calcFitChiSq, initialGuess, args=(xData, yData, avgFreq), method='nelder-mead',
                           options={'xtol': 1e-8, 'disp': False})
            # pfit, pcov = curve_fit(sinusoidModel, xData, yData, p0=initialGuess, bounds=([ampMin, freqMin, phaseMin],
            #   [ampMax, freqMax, phaseMax]))

        except RuntimeError:
            print('calcAmpPhaseFits:  RuntimeError - least squares minimization failed.')
        except ValueError:
            print('calcAmpPhaseFits:  ValueError - either xdata or ydata contain NaNs or incompatible options used.')
        else:
            # print('calcAmpPhaseFits:  Curve fit successfully.')

            # yFit[j] = sinusoidModel(sin(degToRad * setData.loc[(i), 'EL Smooth']), pfit[0], pfit[1], pfit[2])
            # fitParams[j] = pfit

            yFit[j] = sinusoidModel(sin(degToRad * setData.loc[(i), 'EL Smooth']), res.x[0], avgFreq, res.x[1])
            fitParams[j] = [res.x[0], res.x[1]]
            chiSq[j] = calcFitChiSq(res.x, xData, yData, avgFreq)

            # print('Point ', i, ' with fit params: ', fitParams[j], ' startFringe = ', startFringe,
            #   ' endFringe = ', endFringe)

        j += 1

    # Interpolate between fit params to fill in all data points...
    numJused = j
    results = np.zeros((numPts, 5))
    i = (startPt - firstPt) + n

    for j in range(1, numJused):
        #  Create interpolation functions...
        ampInterp = interp1d([j - 1, j], [fitParams[j - 1, 0], fitParams[j, 0]])
        phaseInterp = interp1d([j - 1, j], [fitParams[j - 1, 1], fitParams[j, 1]])
        yInterp = interp1d([j - 1, j], [yFit[j - 1, 0], yFit[j, 0]])
        chiSqInterp = interp1d([j - 1, j], [chiSq[j - 1, 0], chiSq[j, 0]])

        #  Create fractional x values to interpolate between successive j values...
        xNew = np.linspace(j - 1, j, n)

        #  Interpolate parameters...
        results[i - n:i, 0] = ampInterp(xNew)
        results[i - n:i, 1] = phaseInterp(xNew)
        results[i - n:i, 2] = yInterp(xNew)
        results[i - n:i, 3] = chiSqInterp(xNew)
        results[i - n:i, 4] = 0.0

        i += n

    return results

def filterSNR (setData):
    """
    Use a Bessel filter (low-pass) to smooth out the SNR (linear) data and remove the discretized steps.  Returns the
    smoothed SNR data as an ndarray.
    :param seriesNum:       series number of data
    :param setNum:          set number of data
    :param setData:         one set of dataframe containing SNR data
    :return: smoothedSNR    numpy array of SNR data (linear)
    """

    #  Constants...
    filterOrder = 2.  # Order of the Bessel filter
    filterFreq = 0.01  # Critical frequency of filter

    b, a = sig.bessel(filterOrder, filterFreq, btype='lowpass', analog=False, norm='phase')
    smoothedSNR = sig.filtfilt(b, a, setData['SNR Linear'], method='gust')

    return smoothedSNR

"""
Main Loop
"""

#  Initialize test data parameters...
rcvrHeight = 10.0

#   1)  Read in GPS data from a test data file (.csv) into a pandas dataframe, gpsData:
#   2)  Assumes that test data falls within the following criteria:

#  Read file...
inFile = 'Modeled Test Data 1 - gpsData.csv'
gpsData = pd.read_csv(inFile, index_col=[1, 2, 3])

#  Set column dtypes...
gpsData[['EL', 'AZ', 'SNR (s)', 'SNR (p)', 'SNR (lhc)', 'SNR (rhc)']] = \
    gpsData[['EL', 'AZ', 'SNR (s)', 'SNR (p)', 'SNR (lhc)', 'SNR (rhc)']].astype('float')

#   3)  Linearize (from dB) and fits the DC average of SNR data

gpsData['SNR Linear (s)'] = linearizeSNR(gpsData['SNR (s)'].values)
gpsData['SNR Linear (p)'] = linearizeSNR(gpsData['SNR (p)'].values)
gpsData['SNR Linear (lhc)'] = linearizeSNR(gpsData['SNR (lhc)'].values)
gpsData['SNR Linear (rhc)'] = linearizeSNR(gpsData['SNR (rhc)'].values)

#   4)  Corrects linear SNR for antenna gain effects

#  Create antenna parameters...
numAntPts = 181
antAzim = np.linspace(90.0, 270.0, numAntPts)
antElev = np.linspace(-90.0, 90.0, numAntPts)
maxGain = 1.0

antennaGain = np.zeros((numAntPts, 4))
antennaGain[:, 0] = antAzim
antennaGain[:, 1] = antElev
antennaGain[:, 2] = np.sqrt(maxGain) * np.cos(c.degToRad * antAzim) ** 2.0
antennaGain[:, 3] = np.sqrt(maxGain) * np.cos(c.degToRad * antElev) ** 2.0

gpsData['SNR Normalized (s)'] = normalizeSNR(gpsData[['AZ', 'EL']].values,
                                                     gpsData['SNR Linear (s)'].values, antennaGain)
gpsData['SNR Normalized (p)'] = normalizeSNR(gpsData[['AZ', 'EL']].values,
                                                     gpsData['SNR Linear (p)'].values, antennaGain)
gpsData['SNR Normalized (lhc)'] = normalizeSNR(gpsData[['AZ', 'EL']].values,
                                                       gpsData['SNR Linear (lhc)'].values, antennaGain)
gpsData['SNR Normalized (rhc)'] = normalizeSNR(gpsData[['AZ', 'EL']].values,
                                                       gpsData['SNR Linear (rhc)'].values, antennaGain)

#   5)  Estimate the mean SNR fringe frequency (vs. sin(EL)) and calculate the nominal receiver height, H0

h0, periodogram = calcH0(gpsData)
gpsData['H0 (s)'] = h0[:, 0]
gpsData['H0 (p)'] = h0[:, 1]
gpsData['H0 (lhc)'] = h0[:, 2]
gpsData['H0 (rhc)'] = h0[:, 3]
gpsData['Periodogram (s)'] = periodogram[:, 0]
gpsData['Periodogram (p)'] = periodogram[:, 1]
gpsData['Periodogram (lhc)'] = periodogram[:, 2]
gpsData['Periodogram (rhc)'] = periodogram[:, 3]

#   6)  Fit the SNR fringes to a sinusoid with time-varying DC offset, amplitude, and phase


#   7)  Extract SMC from SNR fits


#   8)  Stores the results


#   9)  Plot results

