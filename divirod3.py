# divirod3.py - library utilities for DIVIROD analysis
#
# In this version:
#  - the fitting params are amplitude and phase
#  - H0 remains fixed
#  - bins are in fixed positions in sin(EL)

import numpy as np
import scipy.io as sio
import matplotlib.pyplot as plt
import csv
from scipy.interpolate import interp1d
from scipy.optimize import minimize
from scipy.signal import lombscargle

def arrayStats( a):
    '''
    Print min and max values for array.
    '''
    nx,ny = a.shape
    for i in range(0,nx):
        for j in range(0,ny):
            buf = a[i,j]
            print(i, j, buf.dtype, buf.shape, buf.ndim, np.amin(buf), np.amax(buf))


# In[6]:
def getSeries(blob,j):
    '''
    Given the full blob of data from MATLAB, return the i-th series.
    '''
    
    series = dict()
    
    blobKeys = ('time','az','el','snr')
    myKeys   = ('time','az','el','snr_dB')
    
    # extract elements from ndarray...
    i = 0
    for k1,k2 in zip(myKeys,blobKeys):
        series[k1] = blob[k2][i,j][:,0]
    return series

# In[7]:

def cleanSeriesOld( s, plot=False):
    '''
    Given a series, remove bad points.
    NOTE: This did not work well and is superseded by 
    new version cleanSeries below.
    '''
        
    # get rid of bad elements...
    good = np.where( (s['time'] > 0) & (s['snr_dB'] > 0) )[0]

    # quit if no good data...
    goodCount = len(good)
    print('[cleanSeries] goodCount=%d' % goodCount)
    if goodCount < 1:
        print('[cleanSeries] No good data points')
        return None

    # extract good points...
    cleaned = dict()
    for key in s:
        # print '[cleanSeries] updating key=%s' % key
        cleaned[key] = s[key][good]

    # ensure that data are chronologically sorted...
    chronSort(cleaned)
        
    # ensure that time increase monotonically...
    uniqify(cleaned)
        
    # confirm ordering...
    orderCheck(cleaned['time'])

    if plot:
        plotSeries( s, xunits="sec",
                    yaxis=('az','el','snr_dB'),
                    symbols=('r','g','b'))
 
    return cleaned

def getPeaks( x, y, verbose=False):
    '''
    Given a sequence of (x,y) data, return a list of x values that 
    represent peaks in the data.   We define a peak as the midpoint 
    of the range between rising and falling values. 
    '''
    
    # confirm that x and y are 1-D arrays...
    if x.ndim != 1:
        raise ValueError('X is not a 1-D array')
    if y.ndim != 1:
        raise ValueError('Y is not a 1-D array')
        
    # confirm that x and y are the same shape...
    xsize = (x.shape)[0]
    ysize = (y.shape)[0]
    if xsize != ysize:
        raise ValueError( 'X and Y are not the same shape')
    
    # define an output list...
    xpeaks = list()
    
    # loop over points...
    left = None
    right = None
    for i in range(0,xsize):
        
        # skip first and last values...
        if i < 1 or i > (xsize-2):
            continue
        
        # are the values rising?
        if y[i] > y[i-1]:
            left = i
            
        # check for falling...
        if left is not None and y[i] > y[i+1]:
            right = i
            npts = right - left + 1
            mid = 0.5*(x[left] + x[right])
            if verbose:
                print("left:", left, "right:", right)
                print("x:", x[left], mid, x[right])
                # print "y:", y[left-1:right+2]
                print("size:", npts)

            xpeaks.append( mid)
            left = None
            right = None
            
    if len(xpeaks) == 0:
        return None
    else:
        return np.array(xpeaks)


# In[10]:

def x2y( x, y, xmark):
    '''
    Given an (x,y) array and a list of x values (xmark), 
    return a list of the corresponding y values
    '''
    
    ymark = list()
    for xm in xmark:
        
        # sort points by distance...
        good = getNearestXindex( x, xm)
        ym = np.mean( x[good])
        ymark.append(ym)
        
    return ymark


# In[11]:

def ynorm2y( y, ynorm):
    '''
    Given a normalized y value (ynorm=0 is the min, ynorm=1 is the max),
    return absolute y value.
    '''
    ymin = np.amin(y)
    ymax = np.amax(y)
    yrange = ymax-ymin
    yabs = ymin + ynorm*yrange
    return yabs


# In[12]:

def getYmarks( x, y, xmarks, ynorm=1):
    '''
    Given (x,y) arrays and an array of x values at which to make marks,
    return a set of y values for those marks.
    '''
    
    yabs = ynorm2y( y, ynorm)
    ymarks = xmarks*0.+yabs
    return ymarks


# In[13]:

def plotSeries( series,
                xaxis='time', yaxis=('az','el','snr_dB'),
                symbols=('r','g','b'),
                title=None, xunits=None, xmarks=None, ymarks=None, 
                xleft=None, xright=None,
                text=None, xtext=None, ytextNorm=None,
                yMinNorm=-0.05, yMaxNorm=1.05, markerStyle='ko'):
    '''
    Given a series structure, plot the series.

    optional args:
    text = list of text annotations to add to plot
    xtext = list of x axis locations (absolute) for text annotations
    ytextNorm = list of y axis locations (normalized) for text annoations
    '''

    # allocate figure space...   
    plt.figure(1, figsize=(16, 6))
    
    # generate x label...
    xlabel = xaxis
    if xunits is not None:
        xlabel = "%s [%s]" % (xlabel, xunits)

    # count y axis elements...
    ny = len(yaxis)

    for i in range(0,ny):
        
        plt.subplot(ny,1,i+1)
        x = series[xaxis]
        y = series[yaxis[i]]
        plt.plot( x, y, symbols[i])
        plt.xlabel( xlabel)
        plt.ylabel( yaxis[i])
        if title is not None and i == 0:
            plt.title( title)
        
        # add marks...
        if xmarks is not None:
            plt.plot( xmarks, ymarks, markerStyle)

        # add text...
        if xtext is not None and ytextNorm is not None:
            ntext = len(xtext)
            for i in range(0,ntext):
                yval = ynorm2y(y,ytextNorm[i])
                plt.text( xtext[i], yval, text[i], ha='center')

        # add range...
        if xleft is not None:
            yMin = ynorm2y( y, yMinNorm)
            yMax = ynorm2y( y, yMaxNorm)
            for (xl,xr) in zip(xleft,xright):
                plt.plot( [xl,xr,xr,xl,xl], [yMin,yMin,yMax,yMax,yMin], 'k-')

    plt.show()


# In[14]:

def getNearestXindex( x, xval):
    '''
    Given an array of values (x) and a specific search value (xval),
    return the index in x that is closest in value to xval.
    '''
    
    dist = np.abs(x - xval)
    minDist = np.amin(dist)
    good = np.where( dist == minDist)
    # print "xval=", xval
    # print "x=", x[good]
    # print "good type:", type(good), "value:", good

    return good[0][0]


# In[15]:

def getPeakRange( x, y, xpeak, minPoints=5000, verbose=False):
    '''
    Given (x,y) arrays and a "peak" located at xpeak, find the range of 
    x values that this peak covers and return the indices defining the 
    endpoints of the range occupied by the peak.
    
    Method: start at the xpeak and search to either side until reaching 
    (a) end of array,
    (b) an inflection point, or 
    (c) a large gap in x
    '''

    # get array size...
    buf = x.shape
    nx = buf[0]
    
    # define how much larger a gap must be than the preceding to be a
    # "bad" gap... 
    factor = 100.
    
    # get starting point...
    ipeak = getNearestXindex( x, xpeak)
    
    # search to left...
    i = ipeak
    while True:
        
        # quit if we are at the end...
        if i == 0:
            lmsg = "end at %d" % i
            break 

        # quit if we have found an inflection point...
        if y[i-1] > y[i]:
            lmsg = "inflection at %d y1=%f y2=%f" % (i, y[i-1], y[i])
            break
            
        # quit if we have found a big gap...
        gap1 = x[i+1] - x[i]
        gap2 = x[i] - x[i-1]
        if gap1 > 0 and gap2 >= factor*gap1:
            lmsg = "gap at %d (gap2=%f > gap1=%f)" % (i, gap2, gap1)
            break
            
        # continue search...
        i -= 1
        
    ileft = i
    
    # search to right...
    i = ipeak
    while True:
        
        # quit if we are at the end...
        if i >= nx-2:
            rmsg = "end at %d" % i
            break

        # quit if we have found an inflection point...
        if y[i] < y[i+1]:
            rmsg = "inflection at %d y1=%f y2=%f" % (i, y[i], y[i+1])
            break
            
        # quit if we have found a big gap...
        gap1 = x[i] - x[i-1]
        gap2 = x[i+1] - x[i]
        if gap1 > 0. and gap2 >= factor*gap1:
            rmsg = "gap at %d (gap2=%f < gap1=%f)" % (i, gap2, gap1)
            break
            
        # continue search...
        i += 1
        
    iright = i

    # impose the minPoints constraint if minPoints is defined...
    nPoints = iright - ileft + 1
    if minPoints is None:
        status = True
    else:
        if nPoints >= minPoints:
            status = True
        else:
            if verbose:
                print('[getPeakRange] Peak rejected for being too small (%d < minimum of %d points)' % (nPoints, minPoints))
            status = False
    
    if verbose:
        print("[getPeakRange] LEFT: %s RIGHT: %s WIDTH: %d STATUS: %s" % (lmsg, rmsg, nPoints, status))
    
    if status:
        return (ileft,iright)
    else:
        return None

# In[16]:

def getPeakRanges( s, tPeaks):
    '''
    Given a set of peaks, locate and return the indices defining the start and end of each peak.
    '''
    
    outList = []
    
    # loop over peaks...
    for tPeak in tPeaks:
        buf = getPeakRange( s['time'], s['el'], tPeak)
        if buf is not None:
            (i, j) = buf
            outList.append({'left':i,'right':j})

    return outList


# In[17]:

def extractPeakRange( s, tPeak):
    '''
    Given a series dict consisting of fields time, az, el, etc., and
    the index number in the arrays representing the location of a peak
    (tPeak), return the part of the series occupying the peak.
    '''
    
    # get the peak range...
    buf = getPeakRange( s['time'], s['el'], tPeak)
    if buf is None:
        return None
    (i, j) = buf
    # print "i=", i, "j=", j
    
    # define a container...
    p = dict()
    
    # fill the container...
    for key in ('time','az','el','snr_dB'):
        p[key] = s[key][i:j]
    return p

def extractIndexRange( s, i, j):
    '''
    Given a series dict (s) consisting of fields time, az, el, etc., and
    the start/end indices (i,j) to extract, return the part of the series 
    between the indices.
    '''
    
    # define a container...
    p = dict()
    
    # fill the container...
    for key in ('time','az','el','snr_dB'):
        p[key] = s[key][i:j]
    return p

def initBPM( s, key='bpm', ref=None):
    '''
    Given a series "s" and a existing reference array named "ref", 
    generate a boolnea bad pixel mask array of the same size as "ref"
    and store in series "s" as a new array named "key".  All entries 
    are initialized to zero (which means NOT bad.)
    '''
    
    # it's an error if we have no reference key...
    if ref is None:
        raise ValueError('reference key not defined')

    # create a boolean array of the same dimension as ref...
    s[key] = np.zeros_like( s[ref], dtype='bool')

def updateBPM( s, key='bpm', good=None, bad=None):
    '''
    Given a series 's' with a bad pixel mask stored in array 'key',
    update the pixel mask based on a list of either 'good' or 'bad'
    pixels.  Good and bad pixel lists are generated with np.where.
    '''

    if good is None and bad is None:
        raise ValueError('must set EITHER "good" OR "bad"')
    if good is not None and bad is not None:
        raise ValueError('cannot set both "good" and "bad"')

    # if given a good pixel mask, invert it...
    if good is not None:
        # print "got %d good pixels" % len(good)
        # good
        gpm = np.zeros_like( s[key], dtype='bool')
        gpm[:] = False
        gpm[good] = True

        # find bad pixels...
        buf = np.where( gpm == False )
        bad = buf[0]

    # save bad pixels...
    s[key][bad] = True
    
def bpmRange( s, key, bpmKey='bpm', minVal=140, maxVal=280):
    '''
    Given series 's' with a band 'key', locate pixels outside 
    the stated range defined by (minVal <= s[key] <= maxVal) and 
    set the BPM values to True.
    '''

    # find good pixels...
    buf = np.where( (s[key] >= minVal) & (s[key] <= maxVal) )
    good = buf[0]

    # apply mask...
    updateBPM( s, key=bpmKey, good=good)
    
# In[19]:

def processPass( blob, iSeries, iPeak):
    '''
    Given a blob, a series index "iSeries", and a peak index "iPeak",
    return the segment of the series for that peak.
    '''
    c = processSeries( blob, iSeries)
    
    # locate peaks...
    tPeaks = getPeaks( c['time'], c['el'])
    if tPeaks is None:
        print("No peaks!")
        return None

    # confirm that iPeak is a legal value...
    if iPeak < 0:
        raise ValueError('iPeak is less than 0')
    if iPeak > (len(tPeaks)-1):
        raise ValueError('iPeak exceeds number of peaks in data')            

    # extract peak...
    tPeak = tPeaks[iPeak]
    p = extractPeakRange( c,  tPeak)

    # add BPM...
    initBPM(p, ref='az')

    # add header...
    p['header'] = { "series":iSeries, "peak":iPeak }

    # confirm that the time is well ordered...
    orderCheck(p['time'])

    return p

# In[19]:

def processPassByTime( blob, iSeries, startTime, endTime, iPeak=None):
    '''
    Given a blob, a series index "iSeries", and the start and end times 
    of a peak, return the segment of the series for that peak.
    '''

    c = processSeries( blob, iSeries)

    # convert time to index...
    t = c['time']
    i = findNearestIndex( t, startTime)
    j = findNearestIndex( t, endTime)

    # extract range...
    p = extractIndexRange( c, i, j)

    # add BPM...
    initBPM(p, ref='az')

    # add header...
    p['header'] = { "series":iSeries, "peak":iPeak }

    # confirm that the time is well ordered...
    orderCheck(p['time'])

    return p

def plotPass( p, title=None):

    # measure median signal-to-noise...
    median_snr = np.median(p['snr_dB'])
    iSeries = p['header']['series']
    iPeak   = p['header']['peak']
    if title is None:
        title = "Series=%d Pass=%d S/N=%.1f" % (iSeries, iPeak, median_snr)
    plotSeries( p, title=title, xunits="days",
                yaxis=('az','el','snr_dB','bpm'),
                symbols=('r','g','b','k'))

def processSeries( blob, i):
    '''
    Given a blob, extract the i-th series.  Clean it and convert units 
    of time axis from sec to days.
    '''
    s = getSeries( blob, i)
    c = cleanSeries(s)

    # check for no good data...
    if c is None:
        return None

    # convert time axis from sec to days...
    c['time'] = sec2days( c['time'])
    return c
    
def sec2days( t):
    '''
    Given a numpy array representing time in seconds,
    convert units to days.  Array is edited in place.
    '''
    secPerDay = 60.*60.*24.
    return t/secPerDay
    
def getGoodPeaks( s, verbose=False):
    '''
    Given a series, locate all peaks, determine which ones
    are "good", and return a list of starting and ending coordinates.
    '''

    # locate peaks...
    tPeaks = getPeaks( s['time'], s['el'])
    if tPeaks is None:
        return None

    # set up output arrays...
    xStartList = list()
    xEndList = list()
    
    # get ranges for each peak...
    count = 0
    for tPeak in tPeaks:

        buf = getPeakRange( s['time'], s['el'], tPeak, verbose=verbose)
        if buf is None:
            continue
        count += 1
        (xStart, xEnd) = buf

        # update output lists...
        xStartList.append(xStart)
        xEndList.append(xEnd)

        if verbose:
            print("[plotPeaks]")
            print("peakIndex=", count)
            print("xStart=", xStart)
            print("xEnd=", xEnd)

    return xStartList, xEndList

def x2time( s, xList):
    '''
    Given a series and a list of coordinates,
    return corresponding list of times.
    '''

    # allcoate output list...
    timeList = list()

    # loop over list elements...
    for i in xList:
        t = s['time'][i]
        timeList.append(t)

    return timeList

def plotPeaks( s, index=None, verbose=False):
    '''
    Given a series, plot the series with peaks identified.
    Label plot with indicated index number.
    '''

    # measure median signal-to-noise...
    median_snr = np.median(s['snr_dB'])

    # get start/end coordinates for good peaks...
    xStartList,xEndList = getGoodPeaks( s, verbose=False)
    timeStartList = x2time( s, xStartList)
    timeEndList = x2time( s, xEndList)

    # make plot...
    title = "Series=%d S/N=%.1f" % (index, median_snr)
    plotSeries( s, title=title, xunits="Days", \
                xleft=timeStartList, xright=timeEndList)

def showFitInterp( x, y, yFit, i=None, j=None, frac=None, w=None,
             xlabel=None, ylabel=None):
    '''Show the fit and the difference'''
    
    plt.figure(figsize=(16, 4), dpi=80, facecolor='w', edgecolor='k')
    plt.subplot(1,2,1)
    nx = len(x)
    if i is None:
        i = 0
    elif i<0:
        i = int(-i*nx)
    if j is None:
        j = nx-1
    elif j<0:
        j = int(-j*nx)
    if frac is not None:
        i = 0
        j = int(frac*nx)
    print("i=%d j=%d" % (i,j))
    plt.plot( x[i:j], y[i:j])
    plt.plot( x[i:j], yFit[i:j])
    if xlabel is not None:
        plt.xlabel( xlabel)
    if ylabel is not None:
        plt.ylabel( "%s and fit" % ylabel)
    
    dy = y-yFit
    plt.subplot(1,2,2)
    plt.plot(x[i:j], dy[i:j])
    if xlabel is not None:
        plt.xlabel( xlabel)
    if ylabel is not None:
        plt.ylabel( "%s - fit" % ylabel)

    if w is not None:
        xmin = min(x[i:j])
        xmax = max(x[i:j])
        plt.plot([xmin, xmax, xmax, xmin, xmin],[-w,-w,w,w,-w],'g--')

    plt.show()

def yExtrapolate(xt,yt,x=None,i=None,j=None):
    '''
    Given lists of transition points (xt,yt) and indices of two points 
    to use for defining slope, return the y location at x extrapolated 
    from those points. 
    '''

    # measure slope between points i and j...
    dx = xt[j]-xt[i]
    dy = yt[j]-yt[i]
    slope = dy/dx

    # determine distance of x from xt[i]
    dx2 = x - xt[i]
    
    # translate that x difference into a y difference...
    dy2 = slope*dx2

    # generate y value at that location...
    y = yt[i] + dy2
    return y

def quantInterp( x,y):
    '''
    Given a set of quantized (x,y) values, locate the points at which 
    the values transition and use these as the points for defining new 
    interpolated values. Returns these new interpolated values.
    '''
    
    nx = x.shape[0]
    orderCheck(x)
    
    # allocate list to hold transition points...
    xt = list()
    yt = list()

    # loop over input array...
    for i in range(0,nx-1):

        j = i+1

        # if y value changes, store the midpoint as a transition spot...
        if y[i] != y[j]:
            xm = 0.5*(x[i]+x[j])
            ym = 0.5*(y[i]+y[j])
        
            xt.append(xm)
            yt.append(ym)

    # special case: it's possible that all values are identical, in
    # which case just return the input.
    if len(xt) == 0:
        return y
            
    # add a starting point that extrapolates existing slope...
    xStart = x[0]
    yStart = yExtrapolate(xt,yt,x=xStart,i=0,j=1)

    xt.insert(0, xStart)
    yt.insert(0, yStart)

    # add an ending point that extrapolates existing slope...
    nt = len(xt)
    i = nt-1
    j = nt-2

    xEnd = x[-1]
    yEnd = yExtrapolate(xt,yt,x=xEnd,i=i,j=j)

    xt.append(xEnd)
    yt.append(yEnd)

    # define interpolation function...
    f2 = interp1d(xt, yt, kind='cubic')

    # range check...
    xMin = np.amin(x)
    xMax = np.amax(x)
    
    xtMin = np.amin(xt)
    xtMax = np.amax(xt)

    # sanity check on range before we try to interpolate...
    if xMin < xtMin or xMax > xtMax:
        raise ValueError('Bad interpolator range: %f - %f exceeds %f - %f' %
                         (xMin, xMax, xtMin, xtMax))

    # use interpolator to get new values for ALL x...
    y2 = f2(x)
    return y2

def bpmSelect(p):
    '''
    Given a series with a bad pixel mask, return only the "good"
    elements in series.
    '''

    # locate good elements...
    buf = np.where( p['bpm'] == False)
    good = buf[0]

    # if we have no good points, return None...
    if len(good) < 1:
        return None

    # copy dict...
    q = p.copy()
    
    # copy over good portion of each ndarray...
    for key in p.keys():
        if isinstance(p[key],np.ndarray):
            q[key] = p[key][good]

    return q

def hTickPlot(xmin, xmax, y, dy=0.05, color='k'):
    '''
    Given array of xmin and xmax values, plus y and dy,
    plot horizontal ticks that stretch from xmin to xmax with y width dy (normalized).
    '''
    
    ySpan = np.amax(y) - np.amin(y)
    dyAbs = dy * ySpan
    
    for x1,x2,yy in zip(xmin, xmax, y):
        xtix = [x1,x1,x1,x2,x2,x2]
        y1 = yy - 0.5*dyAbs
        y2 = yy + 0.5*dyAbs
        ytix = [y2,y1,yy,yy,y1,y2]
        plt.plot( xtix, ytix, color)    

# In[23]:
def myModel( p, x, observedWavelength, H0):
    '''
    Given ordinal values (x) and parameters (p),
    return the model.
    '''
    
    # unpack params...
    amplitude,phi = p
    
#    # return "bad" values when we get an illegal amplitude...
#    if amplitude < 0:
#        yModel = 0*x + 1.e32
#    else:
#        yModel = np.abs(amplitude)*np.cos( 4.*np.pi*H0/observedWavelength * x + phi)

    yModel = np.abs(amplitude)*np.cos( 4.*np.pi*H0/observedWavelength * x + phi)
    
    return yModel

# In[24]:

def mychisq(p, x, y, observedWavelength, H0, verbose=False):
    '''
    Given parameter set p, and data points (x,y),
    compute chi^2 for the model by computing
    difference between model (x,yModel) and data (x,y)
    '''
    
    # compute model...
    yModel = myModel( p, x, observedWavelength, H0)

    # compute chisq...
    ny = y.shape[0]
    chisq = np.sum( (yModel - y)**2)/ny
        
    return chisq

# In[33]:
def plotChiSq( x, y, p0, p, observedWavelength, H0):
    '''
    Given starting (p0) and best-fit (p) parameter sets, plot the data 
    with the starting and ending guesses overlaid.
    '''

    # get initial model...
    yModel0 = myModel( p0, x, observedWavelength, H0)
    
    # get best-fit model...
    yModel = myModel( p, x, observedWavelength, H0)
    
    # compute chisq...
    chisq = np.sum( (yModel - y)**2)

    # make plot...
    plt.figure()
    plt.plot(x, y, 'b+', x, yModel0, 'g', x, yModel, 'r')
    plt.title("Chisq = %f" % chisq)
    plt.show()

# In[41]:

def deg2rad( t):
    '''
    Given a set of angular measurements in deg, convert to radians.
    '''
    
    tRad = t * np.pi/180.
    return tRad

def rad2deg( t, base=0):
    '''
    Given a set of angular measurements in radians, convert to deg and put on range [base,base+360].
    By default the range is [0, 360].
    '''
    
    tDeg = t * 180./np.pi

    # apply inverse of base...
    tDeg  -= base
    
    # bring the min value up to positive...
    while np.amin(tDeg) < 0:
        tDeg += 360
        
    # perform modulo operation to put all measurements on the range of 0-360...
    tDeg = tDeg % 360

    # apply base...
    tDeg += base
    
    return tDeg

# In[32]:

def showFit(x, y, p0, p, observedWavelength, H0):
    '''
    Given data (x,y), initial parameter values (p0), final parameter
    values (p), plus fixed parameters observedWavelength and H0,
    print out:
      names of parameters (amp, phase, yoffset)
      initial parameter value
      final parameter value
      change in parameter value (absolute)
      change in parameter value (fractional)
    '''
    chi2 = mychisq(p, x, y, observedWavelength, H0, verbose=True)

    names = ('Amp','Phase','Yoffset')
    print("%15s%12s%12s%12s%12s" % ("Parameter","Initial","Final","Change","Pct"))
    for n,i,f in zip(names,p0,p):
        print("%15s%12.5g%12.5g%12.5g%12.1f" % (n,i,f,f-i,100.*(f-i)/i))

def getWindow( x, y, width, increment, i, count=False):
    '''
    Given arrays x and y, window width "width" (in units of x), return
    the parts of x and y that lie within the i-th window.  When the
    count parameter is set to TRUE, then return only the COUNT of
    segments.
    '''
    
    nx = x.shape[0]
    xmin = np.amin(x)
    xmax = np.amax(x)
    
    if count:
        n = int((xmax-xmin)/increment)
        return n
        
    xc = xmin + i*increment
    xleft  = xc - width/2.
    xright = xleft + width
    
    # refuse to create a window more than 50% off the end...
    if xc > xmax:
        return None
    
    good = np.where((x >= xleft) & (x <= xright))[0]
    # print "%d good points in range %f - %f" % (len(good), xleft, xright)
    return x[good], y[good]

def AzEl2XY(az,el,H):
    '''
    Given receiver height, convert arrays of azimuth and elevation (degrees) into (x,y).
    '''
    
    # convert deg to rad...
    azRad = az*np.pi/180.
    elRad = el*np.pi/180.

    # get radii...
    R = H/np.tan(elRad)
    
    # convert to (x,y)
    x = R*np.sin(azRad)
    y = R*np.cos(azRad)
    
    return x,y

def maskBadAzEl( p1):
    '''
    Given a pass structure p1, return a version in which the data
    points outside of the "good" range in AZ/EL are removed.
    '''
    
    # set good AZ range for points...
    bpmRange( p1, 'az', minVal=140, maxVal=280)

    # set good EL range for points...
    bpmRange( p1, 'el', minVal=5, maxVal=70)

    # select good data...
    p1 = bpmSelect(p1)

    return p1

# In[4]:

def deQuantizeAzEl(p1, plot=False):
    '''
    Given a pass structure p1, dequantize the az and el values in place.
    '''
    
    # generate new AZ values...
    x = p1['time']
    orderCheck(x)

    y = p1['az']
    print(len(x), len(y))
    y2 = quantInterp( x,y)
    p1['az'] = y2

    # optional plot of old and new AZ values...
    if plot:
        showFitInterp( x,y,y2,frac=1.0,w=0.5,xlabel='time',ylabel='AZ')

    # generate new EL values...
    x = p1['time']
    y = p1['el']
    y2 = quantInterp( x,y)
    p1['el'] = y2
    
    # optional plot of old and new EL values...
    if plot:
        showFitInterp( x,y,y2,frac=1.0,w=0.5,xlabel='time',ylabel='EL')
        
    return p1


# In[5]:

def addSinEl( p1):
    '''
    Given a pass structure p1, add a sin_el field with the sin(el).
    '''

    p1['sin_el'] = np.sin(p1['el']*np.pi/180.)
    return p1


# In[6]:

def getRawPower(p1):
    '''
    Given a pass structure, add a power_raw field.
    '''
    
    p1['power_raw'] = 10.**(p1['snr_dB']/20.)
    return p1


# In[7]:

def detrend(p1):
    '''
    Given a pass structure, fit a 2nd-order polynomial to 
    linearized $S/N$ vs. $\sin(\theta)$ to obtain $S/N_{MPI}$
    and add a "power" field to structure.
    '''
    x = p1['sin_el']
    y = p1['power_raw']
    coeff = np.polyfit(x, y, 2)

    f = np.poly1d(coeff)
    y2 = f(x)

    p1['power'] = y-y2
    return p1


# In[8]:

def prepPeak(p1, plotBefore=False, plotAfter=False, debug=False):
    '''
    Given a pass structure, perform all basic processing to prepare
    for analysis.
    '''
    
    # optional plot of data before...
    if plotBefore:
        plotPass(p1)

    # exclude data points outside good range...
    p1 = maskBadAzEl( p1)

    # check for no good data...
    #print p1.keys()
    #for k in p1.keys():
    #    print k, len(p1[k])
    if p1 is None or len(p1['el']) == 0:
        if debug:
            print("[prepPeak] WARNING: No good data after maskBadAzEl")
        return None

    # de-quantize az and el values...
    p1 = deQuantizeAzEl( p1)
    
    # add the sin_el field...
    p1 = addSinEl( p1)
    
    # add the raw power field...
    p1 = getRawPower(p1)
    
    # remove trend from power...
    p1 = detrend(p1)

    # check for inflection...
    i = findInflection(p1['sin_el'])
    if i is not None:
        print("[prepPeak] WARNING: inflection at", i)
    
    # optional plot of data after...
    if plotAfter:
        plotPass(p1)
    
    return p1


# In[9]:

def analyzeLSP( p1, amin=1., amax=200., astep=0.1, plot=False):
    '''
    Given x,y data, use the lomb-scargle method to derive the
    peak frequency.    
    '''
    
    x = p1['sin_el']
    y = p1['power']
    f = np.arange(amin, amax, astep)
    pgram = lombscargle(x, y, f)
    
    imax = np.argmax(pgram)
    fmax = f[imax]
           
    # plot raw data...
    if plot:
        
        # generate array of phases...
        wmax = 2.*np.pi/fmax
        nx = (x.shape)[0]
        phase = np.zeros(nx)
        for i in range(0,nx):
            count = int(x[i]/wmax)
            phase[i] = (x[i]-count*wmax)/wmax*2.*np.pi
        
        plt.figure(1, figsize=(8, 8))
        plt.subplot(3, 1, 1)
        plt.plot(x, y, 'b')
        plt.xlabel( "sin(EL)")
        plt.ylabel( "Power")
    
        # plot frequency strength...
        plt.subplot(3, 1, 2)
        plt.plot(f, pgram)
        plt.xlabel( "Frequency")
        plt.ylabel( "PSD")

        axes = plt.gca()
        ymin, ymax = axes.get_ylim()
    
        plt.plot( [fmax,fmax], [ymin,ymax], 'r')

        # plot phase diagram...
        plt.subplot(3,1,3)
        plt.plot( phase, y, 'b,')
        plt.xlabel( "Phase")
        plt.ylabel( "y")
        plt.show()
    
    return fmax

# In[12]:

def fitToLeftOrRight( x, y, width, incr, p0, iMiddle, xfit, yfit, r, nSegments, 
                      direction='left', observedWavelength=0., H0=0.,
                      verbose=False, plot=False):
    '''
    Given (x,y) data, window parameters (width, incr),
    initial fit parameters (p0), identity of middle segment (iMiddle),
    '''

    # set initial param values...
    p = p0.copy()
    
    # set increment...
    if direction == 'left':
        step = -1
    else:
        step = 1
    
    # fit from middle moving left...
    i = iMiddle
    iStop = 0
    while True:
        
        # check for stop condition (different if moving left or right)...
        if direction == 'left':
            if i < 1:
                break
        else:
            if i >= nSegments:
                break

        # allocate window...
        good = getWindowFixed(x,y,width,incr,i,debug=False)

        # skip if nothing good found...
        if good is None:
            if verbose:
                print("Off edge")
            break

        # extract the data points that lie within the bin...
        x2 = x[good]
        y2 = y[good]

        # determine endpoints and center of bin...
        xc,xl,xr = getWindowFixed(x,y,width,incr,i,coords=True)
        if verbose:
            print("i=%2d nx=%5d xmin=%.3f xmax=%.3f" % (i, np.shape(x2)[0], np.amin(x2), np.amax(x2)))

        # perform fit...
        pStart = p
        res = minimize( mychisq, p, args=(x2,y2,observedWavelength,H0),                        method='nelder-mead', options={'xtol': 1e-8, 'disp': False})

        # if amplitude is bad, try change phase by 180 deg and try again...
        amplitude0,phi0 = p
        amplitude1 = np.abs(res.x[0])
        amplitudeFraction = np.abs(amplitude1/amplitude0)
        amplitudeFractionMin = 0.01
        if amplitudeFraction < amplitudeFractionMin:
            print("[fitToLeftOrRight] amplitudeFraction=", amplitudeFraction)
            print("[fitToLeftOrRight] changing phase by 180deg")
            phi0 += np.pi
            p = amplitude0,phi0
            pStart = p
            res = minimize( mychisq, p, args=(x2,y2,observedWavelength,H0),                        method='nelder-mead', options={'xtol': 1e-8, 'disp': False})
            
        
   
        # store results
        j = i-1
        r['xc'][j] = xc # requested bin center
        r['xCenter'][j] = np.mean(x2) # mean X position of points in bin
        r['xLeft'][j] = np.amin(x2)
        r['xRight'][j] = np.amax(x2)
        r['amp'][j] = np.abs(res.x[0])
        r['phase'][j] = res.x[1]
        r['outcome'][j] = res.success
        r['chisq'][j] = mychisq( res.x, x2, y2, observedWavelength, H0, verbose=False)
        if verbose:
            print(" amp=%10.5g Phase=%10.5g chisq=%10.5g Outcome=%s" % (r['amp'][j], r['phase'][j], r['chisq'][j], str(res.success)))

        # use this results as initial guess for next iteration...
        p = res.x

        if (not res.success) or (i == iStop) or plot:
            showFit( x2, y2, p0, p, observedWavelength, H0)
            plotChiSq( x2, y2, p0, p, observedWavelength, H0)
        if (not res.success) or (i == iStop):
            break
        
        # get best-fit model...
        yModel = myModel( p, x2, observedWavelength, H0)

        # save points...
        xfit.extend(x2)
        yfit.extend(yModel)

        # decrement...
        i += step

# In[13]:

def generateSmcData( blob, series, peak, plot=True):
    '''
    Given a series number and pass number, return the phase measurements.
    '''
    
    # read data into structire...
    p1 = processPass( blob, series, peak)
    
    # perform basic processing...
    p1 = prepPeak(p1)

    # check for no good data...
    if p1 is None:
        print('[generateSmcData] No good data.')
        return None
    
#    # show result...
#    plt.plot( p1['sin_el'], p1['power'])
#    plt.xlabel('sin(EL)')
#    plt.ylabel('Power')
#    plt.show()
    
    # define peak frequency...
    fmax = analyzeLSP( p1)
    wmax = 2.*np.pi/fmax
    # print "Frequency = %f" % fmax
    # print "Wavelength = %f" % wmax
    
    # determine observing wavelength...
    observingWavelength = 0.19 # meters
    Heff = fmax*observingWavelength/(4.*np.pi)
    # print "Heff=", Heff
    
    width = 2.*wmax
    incr = width/4.
    
    # define initial guess...
    observedFrequency = 1.5e9 # 1.5 GHz
    c = 2.9979245800e10 # centimeters / second
    observedWavelength = c/observedFrequency # wavelength is 19.986cm
    
    x = p1['sin_el']
    y = p1['power']
    
    # define initial guess...
    amplitude_guess = 0.5*(np.amax(y)-np.amin(y))
    H0 = 100. * Heff # convert m to cm
    print('H0=', H0)
    phi_guess = np.pi
    p0 = np.array([amplitude_guess,phi_guess])
    # print p0
    
    # remove DC signal...
    y = y - np.mean(y)

    # set window parameters...
    i = 0
    nSegments = getWindowFixed(x,y,width,incr,i,count=True,debug=False)
    
    # create data structures to hold results
    r = dict()
    r['xc'] = np.zeros(nSegments)
    r['xCenter'] = np.zeros(nSegments)
    r['xLeft'] = np.zeros(nSegments)
    r['xRight'] = np.zeros(nSegments)
    r['amp'] = np.zeros(nSegments)
    r['phase'] = np.zeros(nSegments)
    r['outcome'] = np.zeros(nSegments)
    r['chisq'] = np.zeros(nSegments)
    xfit = []
    yfit = []
    
    # fit to left...
    iMiddle = int(nSegments / 2. )
#    fitToLeft( x, y, width, incr, p0, iMiddle, xfit, yfit, r,
#        observedWavelength=observedWavelength,
#        verbose=True)
    fitToLeftOrRight( x, y, width, incr, p0, iMiddle, xfit, yfit, r, nSegments,
                      direction='left', observedWavelength=observedWavelength, 
                      H0=H0, verbose=True)   
    
    # fit to right...
#    fitToRight( x, y, width, incr, p0, iMiddle, xfit, yfit, r, nSegments,
#        observedWavelength=observedWavelength,
#        verbose=True)
    fitToLeftOrRight( x, y, width, incr, p0, iMiddle, xfit, yfit, r, nSegments,
                      direction='right', observedWavelength=observedWavelength,
                      H0=H0, verbose=True) 
    
    # Convert phases to degrees...
    r['phaseDeg'] = rad2deg( r['phase'])

    # remove jumps in phase...
    p = r['phaseDeg']
    fixPhaseJumps( p)
    r['phaseDeg'] = p
    
    good = np.where( r['outcome'])[0]
    ngood = len(good)

    plot = True
    if plot:
        plt.figure(figsize=(12, 15))

        plt.subplot(4,1,1)
        plt.plot(x,y,'k', xfit,yfit,'b,')
        plt.xlabel( "sin(EL)")
        plt.ylabel( "Power")
        axes = plt.gca()
        xmin, xmax = axes.get_xlim()

        plt.subplot(4,1,3)
        plt.plot( r['xc'][good], r['amp'][good],'b')
        plt.plot( r['xc'][good], r['amp'][good],'ks')
        hTickPlot( r['xLeft'][good], r['xRight'][good], r['amp'][good])
        plt.xlabel( "sin(EL)")
        plt.ylabel( "Amplitude")
        plt.xlim(xmin,xmax)

        plt.subplot(4,1,4)
        plt.plot( r['xc'][good], r['phaseDeg'][good],'r')
        plt.plot( r['xc'][good], r['phaseDeg'][good],'ks')
        hTickPlot( r['xLeft'][good], r['xRight'][good], r['phaseDeg'][good])
        plt.xlabel( "sin(EL)")
        plt.ylabel( "Phase")
        plt.xlim(xmin,xmax)

        plt.subplot(4,1,2)
        plt.plot( r['xc'][good], r['chisq'][good],'m')
        plt.plot( r['xc'][good], r['chisq'][good],'ks')
        plt.xlabel( "sin(EL)")
        plt.ylabel( "chisq")
        plt.xlim(xmin,xmax)

        plt.show()

    print('[generateSmcData] OK')
    return r

def countPeaks( blob, i):
    '''
    Given a blob and a series number (i), return the number of 
    GOOD peaks in series.
    '''

    # extract series from blob...
    c = processSeries( blob, i)
    if c is None:
        return 0

    # locate peaks...
    tPeaks = getPeaks( c['time'], c['el'])
    if tPeaks is None:
        nPeaks = 0
    else:
        # check whether peaks are good...
        nPeaks = tPeaks.size
        nGood = 0
        for i in range(0,nPeaks):
            buf = getPeakRange( c['time'], c['el'], tPeaks[i])
            if buf is not None:
                nGood += 1
        nPeaks = nGood

    return nPeaks

def fixPhaseJumps( p):
    '''
    Given list of angles (p) in units of deg, adjust values so that 
    adjacent values don't differ by more than 180 deg.
    '''

    n = p.size

    # loop over array elements...
    for i in range(1,n):
        while p[i]-p[i-1] < -180:
            p[i] += 360
        while p[i]-p[i-1] > 180:
            p[i] -= 360
        
def orderCheck(x):
    '''
    Confirm that the array is in order.
    '''
    nx = x.shape[0]
    for i in range(0,nx-1):
        if x[i] >= x[i+1]:
            raise ValueError('Array out of order i=%d/%d x1=%f x2=%f' %
                             (i,nx,x[i],x[i+1]))

def chronSort(p):
    '''
    Re-order the structure so that time increases monotonically.
    '''

    order = np.argsort( p['time'])
    for key in p:
        # print '[chronSort] updating key=%s' % key
        p[key] = p[key][order]

def uniqify(p, verbose=False):
    '''
    Given a dict p (assumed to be sorted by the 'time' field), return
    a version of p that eliminates data points that occur at the same
    time as a previous point.
    '''

    key = 'time'
    n = p[key].size
    if n < 2:
        raise ValueError('Structure too short; only %d values' % n)

    # construct list of all elements which increase time...
    good = list([0])
    badCount = 0
    for i in range(0,n-1):
        j = i+1
        if p[key][j] > p[key][i]:
            good.append(j)
        else:
            badCount += 1
            if verbose:
                print("[uniqify] WARNING: element %d at time=%f is not more than element %d at time=%f" % (j, p[key][j], i, p[key][i]))

    # revise the struct...
    for key in p:
        p[key] = p[key][good]

    print("[uniqify] Eliminated %d/%d duplicate time values" % (badCount,n))

def cleanSeries( s, plot=False, verbose=False):
    '''
    Given a series, remove bad points by eliminating data points
    that are out of chronological order, have no time associated with them,
    or have no measured S/N value.
    '''
    
    # eliminate data that are out of order...
    good = list([0])
    lastTime = s['time'][0]
    n = len(s['time'])
    nGood = 1
    nBad = 0
    for i in range(1,n):
        # pass this point if time increased...
        if s['time'][i] > lastTime:
            good.append(i)
            nGood += 1
            lastTime = s['time'][i]
        else:
            nBad += 1            

    if verbose:
        print('nGood=', nGood)
        print('nBad=', nBad)

    # quit if no good data...
    goodCount = len(good)
    if goodCount < 1:
        print('[cleanSeries] WARNING: No good data points')
        return None

    # extract good points...
    cleaned = dict()
    for key in s:
        if verbose:
            print('[cleanSeries] updating key=%s' % key)
        cleaned[key] = s[key][good]
        
    if plot:
        plt.figure(1)
        plotSeries( cleaned, xunits="sec",
                    yaxis=('az','el','snr_dB'),
                    symbols=('r','g','b'))
 
    # pass only good elements...
    good = np.where( (cleaned['time'] > 0) & (cleaned['snr_dB'] > 0) )[0]

    # quit if no good data...
    goodCount = len(good)
    if goodCount < 1:
        print('[cleanSeries] WARNING: No good data points')
        return None

    # extract good points...
    for key in cleaned:
        cleaned[key] = cleaned[key][good]
        
    if plot:
        plt.figure(2)
        plotSeries( cleaned, xunits="sec",
                    yaxis=('az','el','snr_dB'),
                    symbols=('r','g','b'))
 
    return cleaned

def countSeries( blob):
    '''
    Return the number of series in the passed blob.
    '''
    n = len(blob['time'][0,:])
    return n

def findNearestIndex( l, v):
    '''
    Given a list (l) and a value (v), return the index of the element 
    in list a which is closest in value to v.
    '''

    a = np.asarray(l)
    diff2 = (a-v)**2
    return np.argmin(diff2)


def getPeakDataForSeries( peakData, index, setName=None):
    '''
    Given index, return a list (outList) with elements 
    from the peak data list (peakData) for the series 
    with the indicated index number (index).
    '''
    
    # allocate output dict...
    outList = list()
        
    # loop over fields in peakData:
    seriesKey = 'series'
    for row in peakData:
        if row[seriesKey] == index:

            # skip this pass if the set does not match requested...
            if setName is not None:
                # print "checking set: '%s' vs. '%s'" % (row['set'],setName)
                if row['set'] != setName:
                    continue
            
            # copy over each key...
            outList.append( row)
                
    return outList


# In[160]:

def getSetNames( peakData):
    '''
    Given full list of set names, return the list of unique options.
    '''    
    buf = set([e['set'] for e in peakData])
    return sorted(list(buf))


# ## Read in the peak table

# In[161]:

def readPeakTable( csvFile, defaultSet='a'):
    '''
    Given a peak file (csvFile), read it in, set blank values for 
    'set' to the default, and return a list of dicts (peakData).
    '''
    
    # define data types...
    intType = 'i'
    floatType = 'f'
    charType = 'a'
    
    # define files found in file...
    keys = ['series','peak','startTime','endTime','deltaTime','set']
    datatype = [intType, intType, floatType, floatType, floatType, charType]

    # allocate input data structure...
    peakData = list()

    # read CSV file...
    with open( csvFile, 'r') as f:
        reader = csv.DictReader( f)
        
        # loop over rows...
        for inRow in reader:

            # allocate empty output row...
            outRow = dict()

            # loop over keys...
            for key,dtype in zip(keys,datatype):

                # default is to use string value...
                value = inRow[key]
                
                # perform type conversion for numeric values...
                if dtype == intType:
                    value = int(value)
                elif dtype == floatType:
                    value = float(value)
                    
                outRow[key] = value
                
            # empty values for 'set' get a default value...
            if outRow['set'] == '':
                outRow['set'] = defaultSet

            # add row to output list...
            peakData.append(outRow)
            
    return peakData


# ## Define new fitting routines

# In[162]:

def getH0( p, observedWavelength=None, plot=False):
    '''
    Given a peak structure (p), and the observing wavelength in cm,
    use LSP method to derive estimate for H0 in cm.
    '''
    
     # define peak frequency...
    fmax = analyzeLSP( p, plot=plot)
    
    # determine observing wavelength...
    H0 = fmax*observedWavelength/(4.*np.pi)    
    return H0


# In[163]:

def plotFit( r, x, y, xfit, yfit, good):
    '''Given fit structure (r), plus inbput data (x,y) and fit (xfit,yfit), plot the fit'''

    plt.figure(figsize=(12, 15))

    plt.subplot(4,1,1)
    plt.plot(x,y,'k', xfit,yfit,'b,')
    plt.xlabel( "sin(EL)")
    plt.ylabel( "Power")
    axes = plt.gca()
    xmin, xmax = axes.get_xlim()

    plt.subplot(4,1,3)
    plt.plot( r['xc'][good], r['amp'][good],'b')
    plt.plot( r['xc'][good], r['amp'][good],'ks')
    hTickPlot( r['xLeft'][good], r['xRight'][good], r['amp'][good])
    plt.xlabel( "sin(EL)")
    plt.ylabel( "Amplitude")
    plt.xlim(xmin,xmax)

    plt.subplot(4,1,4)
    plt.plot( r['xc'][good], r['phaseDeg'][good],'r')
    plt.plot( r['xc'][good], r['phaseDeg'][good],'ks')
    hTickPlot( r['xLeft'][good], r['xRight'][good], r['phaseDeg'][good])
    plt.xlabel( "sin(EL)")
    plt.ylabel( "Phase")
    plt.xlim(xmin,xmax)

    plt.subplot(4,1,2)
    plt.plot( r['xc'][good], r['chisq'][good],'m')
    plt.plot( r['xc'][good], r['chisq'][good],'ks')
    plt.xlabel( "sin(EL)")
    plt.ylabel( "chisq")
    plt.xlim(xmin,xmax)

    plt.show()


# In[164]:

def fitPhaseAmp( p1, H0, width=0.1, plotInitial=False, plotFinal=True, observedWavelength=None):
    '''
    Given a peak structure (p1), value for effective height (H0), 
    fit phase and amplitude.
    '''

    incr = width/4.

    x = p1['sin_el']
    y = p1['power']
    
    # define initial parameters...
    amplitude_guess = 0.5*(np.amax(y)-np.amin(y))
    phi_guess = np.pi
    p0 = np.array([amplitude_guess,phi_guess])
    
    # remove DC signal...
    y = y - np.mean(y)

    # set window parameters...
    i = 0
    nSegments = getWindowFixed(x,y,width,incr,i,count=True,debug=False)
    
    # create data structures to hold results
    r = dict()
    r['xc'] = np.zeros(nSegments)
    r['xCenter'] = np.zeros(nSegments)
    r['xLeft'] = np.zeros(nSegments)
    r['xRight'] = np.zeros(nSegments)
    r['amp'] = np.zeros(nSegments)
    r['phase'] = np.zeros(nSegments)
    r['outcome'] = np.zeros(nSegments)
    r['chisq'] = np.zeros(nSegments)
    xfit = []
    yfit = []
    
    # fit to left...
    iMiddle = int(nSegments / 2. )
    fitToLeftOrRight( x, y, width, incr, p0, iMiddle, xfit, yfit, r, nSegments,
                      direction='left', observedWavelength=observedWavelength, 
                      H0=H0, verbose=True, plot=plotInitial)   
    
    # fit to right...
    fitToLeftOrRight( x, y, width, incr, p0, iMiddle, xfit, yfit, r, nSegments,
                      direction='right', observedWavelength=observedWavelength,
                      H0=H0, verbose=True, plot=plotInitial) 
    
    # Convert phases to degrees...
    r['phaseDeg'] = rad2deg( r['phase'])

    # set the phase range to center on the mean phase value...
    p = r['phaseDeg']
    p2 = fixPhaseRange( p)
    r['phaseDeg'] = p2    
    
    # remove jumps in phase...
    p = r['phaseDeg']
    fixPhaseJumps( p)
    r['phaseDeg'] = p

    # add the az/el for each bin...
    r['az'] = np.zeros(nSegments)
    r['el'] = np.zeros(nSegments)
    r['time'] = np.zeros(nSegments)
    naz = (p1['az'].shape)[0]
    for i in range(nSegments):
        xc = r['xc'][i]
        j = getNearestXindex(x,xc)
        # print naz,xc,i,j
        r['az'][i]   = p1['az'][j]
        r['el'][i]   = p1['el'][j]
        r['time'][i] = p1['time'][j]

    # optional plot...
    if plotFinal:
        good = np.where( r['outcome'])[0]
        plotFit( r, x, y, xfit, yfit, good)
    
    return r, x, y, xfit, yfit


# In[165]:

def getFixedWindowList( xMin, xMax, width, increment):
    '''
    Given window width and increment, return left 
    and right coordinates of all possible windows.
    '''

    # set up list of all possible windows...
    xLeftList = list()
    xRightList = list()

    # define starting values...
    xLeft = xMin
    while True:
        
        # define right side...
        xRight = xLeft + width
        
        # add to list...
        xLeftList.append(xLeft)
        xRightList.append(xRight)
        
        # quit if the right meets or exceeds the edge...
        if xRight >= xMax:
            break
            
        # increment...
        xLeft += increment
        
    return xLeftList, xRightList


# In[166]:

def getOccupiedWindows(x, xLeftAll, xRightAll):
    '''
    Given an array of x values and a set of bin endpoints,
    return the list of bins for which the bin center lies 
    within the data range.
    '''
    
    # get data range...
    xMin = np.amin(x)
    xMax = np.amax(x)
    
    # create a list of the occupied windows...
    xLeftGood = list()
    xRightGood = list()
    for l,r in zip(xLeftAll, xRightAll):
        xc = 0.5*(l+r)
        if xc >= xMin and xc <= xMax:
            xLeftGood.append(l)
            xRightGood.append(r)
            
    return xLeftGood, xRightGood


# In[167]:

def getWindowFixed( x, y, width, increment, i, count=False, coords=False,
                    debug=False):
    '''Given arrays x and y, window width "width" (in units of x), return
    the indices of those elements of x and y that lie within the i-th
    window.  

    When the count parameter is set to TRUE, then return only the
    COUNT of segments.

    When COORDS parameter is true, then return the X coords of the
    center, left, and right edge of window.
    
    This version will use windows that are constrained to lie at fixed 
    locations in x.
    '''
    
    nx = x.shape[0]
    xMin = np.amin(x)
    xMax = np.amax(x)

    # get full window list...
    xStart = 0.
    xEnd = 1.
    xLeftAll, xRightAll = getFixedWindowList( xStart, xEnd, width, increment)
    if debug:
        print('All windows:')
        for ixl,ixr in zip(xLeftAll,xRightAll):
            print(ixl,ixr)
    
    # get occupied window list...
    xLeftOccupied, xRightOccupied = getOccupiedWindows(x, xLeftAll, xRightAll)
    if debug:
        print('Occupied windows:')
        for ixl,ixr in zip(xLeftOccupied,xRightOccupied):
            print(ixl,ixr)
            
    # when just counting, return number of bins...
    n = len(xLeftOccupied)
    if count:
        return n
    
    # confirm that the requested bin index is legal...
    if i >= n:
        raise ValueError( 'requested bin %d out of legal range %d-%d' \
                          % (i, 0, n-1))
    
    # choose the i-th bin...
    xLeft  = xLeftOccupied[i]
    xRight = xRightOccupied[i]
    xCenter = 0.5*(xLeft + xRight)

    # return only the bin center and endpoints if requested...
    if coords is True:
        return xCenter, xLeft, xRight
        
    good = np.where((x >= xLeft) & (x <= xRight))[0]
    if debug:
        print("%d good points in range %f - %f" % (len(good), xLeft, xRight))
    return good

def findInflection( p):
    '''
    Given an array, return the index of the first inflection 
    point. 
    '''

    # define the number of consecutive points that define the slope...
    w = 1
    n = (p.shape)[0]
    oldSlope = None
    for i in range(n):

        # adjust index as needed so that we don't go off the array edge...
        left  = np.amax([i-w,0])
        right = np.amin([i+w,n-1])
        
        # compute change in value...
        diff = p[right] - p[left]
        
        # skip if no slope...
        if diff == 0:
            continue
        
        # convert that into a +1 or -1...
        slope = diff/np.abs(diff)
        
        # check for change in slope...
        if oldSlope is not None and slope != oldSlope:
            return i
        
        # save this value...
        oldSlope = slope

    # reached end without inflection...
    return None

def theta2xy( theta):
    '''
    Given angle theta (in deg), return corresponding unit (x,y) vector.  Note that for theta=0, x=1, y=0.
    '''
    
    tRad = deg2rad(theta)
    x = np.cos(tRad)
    y = np.sin(tRad)
    return x,y


# In[110]:

def xy2theta( x,y):
    '''
    Given vector, return corresponding angle.
    '''  
    theta = np.arctan2(y,x)
    return rad2deg(theta)


# In[111]:

def meanAngle(angles):
    '''
    Given array of angles, use vector sum to compute and return the mean angle.
    NOTE: all angles in deg.
    '''
    
    # convert each angle to unit vector and sum...
    xsum = 0
    ysum = 0
    
    for a in angles:
        x,y = theta2xy(a)
        xsum += x
        ysum += y
            
    # check for sum to zero...
    if xsum == 0 and ysum == 0:
        raise ValueError( 'Angles sum to zero')
        
    m = xy2theta(xsum,ysum)
    return m


# In[112]:
def plotVectors(u, v, u0=None, v0=None, size=6):
    '''
    Given array of vectors (u,v), plot them.  If u0 and v0 are defined,
    plot this as a single red vector.
    '''

    # assemble soa array...
    n = u.shape[0]
    x = np.zeros(n)
    y = np.zeros(n)
    xymax = np.amax(np.abs([u,v]))

    fig = plt.figure(figsize=(size, size))
    plt.axis('equal')
    ax = plt.gca()
    ax.quiver(x,y,u,v,angles='xy',scale_units='xy',scale=1)
    if u0 is not None and v0 is not None:
        ax.quiver([0], [0], [u0], [v0],
                  angles='xy',scale_units='xy',scale=1,color='r')
    ax.set_xlim([-xymax,xymax])
    ax.set_ylim([-xymax,xymax])
    plt.draw()
    plt.show()

def plotAngles(a,a0=None):
    '''
    Given a set of angles, plot unit vectors.
    If angle a0 is defined, plot this as a red vector.
    '''

    # convert angles to unit vectors...
    na = a.shape[0]
    x = np.zeros(na)
    y = np.zeros(na)
    for i in range(na): 
        ix,iy = theta2xy(a[i])
        x[i] = ix
        y[i] = iy

    # generate optional red vector...
    if a0 is None:
        u0=None
        v0=None
    else:
        u0,v0 = theta2xy(a0)

    # plot...
    plotVectors(x,y,u0=u0,v0=v0)
    
def setPhaseRange(angles, center):
    '''
    Given a list of angles, adjust them so that they are all within +/-180 of zeroPoint (center).
    Written to return a scalar if "angles" is a scalar or a vector if "angles" is a vector.
    '''

    # allocate output array...
    try:
        scalar = False
        n = angles.shape[0]
        inAngles = angles
    except IndexError:
        n = 1
        scalar = True
        inAngles = np.array([angles])
    outAngles = np.zeros(n)
    
    # set limits...
    minAngle = center - 180
    maxAngle = center + 180
    
    # put mean angle into range...
    for i in range(n):
        m = inAngles[i]
        while m > maxAngle:
            m -= 360
        while m < minAngle:
            m += 360
        outAngles[i] = m
        # print i, minAngle, maxAngle, inAngles[i], outAngles[i]
        
    if scalar:
        return outAngles[0]
    else:
        return outAngles

def fixPhaseRange( angles):
    '''
    Given a list of angles (angles), determine the mean angle
    and adjust the angle range to be +/-180 deg from the mean.
    '''
    
    # get mean angle...
    m = meanAngle(angles)
    
    # put mean angle into range centered on 0...
    m = setPhaseRange(m, 0)
        
    # put remaining angles into range centered on m...
    outAngles = setPhaseRange(angles, m)
    
    return outAngles

def plotPhaseRange(a):
    '''
    Given a list of angles (angles), determine the mean angle
    and plot unit vectors for all.
    '''
    
    # get mean angle...
    a0 = meanAngle(a)
    plotAngles(a,a0)
