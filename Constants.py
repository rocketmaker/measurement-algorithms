"""
--------------------
    Constants.py
--------------------

Divirod, 6 Mar 17, M. Estes

Class definition of various physical and system constants.
"""

from math import pi
import numpy as np

class Constants():
    """

    """

    #  Constants...
    c = 2.998E+8                        #  Speed of light (m/sec)
    epsilon0 = 8.854E-12                #  Vacuum permittivity (F/m)
    mu0 = 4.0 * pi * 1.0E-7             #  Vacuum permeability (H/m)
    gpsFrequency = 1.57542E+9           #  GPS carrier frequency (Hz)
    gpsWavelength = c / gpsFrequency    #  GPS carrier wavelength (m)
    gpsPower = 56.5                     #  GPS transmitted power out of satellite (dBm)
    gpsOrbitHeight = 2.020E+7           #  Orbital height of GPS satellite (m)
    maxPRN = 32                         #  Largest PRN (satellite) number in GPS constellation
    ONE_DEG_LAT_M = 111035.             #  Length of 1 deg latitude at 40 deg N latitude (m)
    ONE_DEG_LON_M = 85394               #  Length of 1 deg longitude at 40 deg N latitude (m)
    antennaImpedance = 50.0             #  Antenna impedance (ohms)
    receiverSensitivity = -167.0        #  GPS receiver sensitivity (dbm)
    rEarth = 6.354E+6                   #  Radius of earth (approx., m)
    degToRad = pi / 180.0               #  Degrees to radians conversion (rad/deg)
    radToDeg = 1 / degToRad             #  Radians to degrees conversion (deg/rad)
    secondsPerDay = 3600 * 24           #  Number of seconds in a day
    airyDiskZero = 3.83170597           #  First zero of J1(x) Bessel function
    airyDisk50 = 1.4811459              #  Radius which encircles 50% of power in first zone
    airyDisk75 = 2.03375937             #  Radius which encircles 75% of power in first zone
    airyDisk90 = 2.51788393             #  Radius which encircles 90% of power in first zone
    halfSqrtTwo = 0.70710681            #  Square root 2 over 2

    #  Hallikainen soil permittivity model - fit constants @ 1.575 GHz (interpolated)...

    #  Real part of epsilon...
    ar0 = 2.901
    ar1 = -0.012
    ar2 = 0.000
    br0 = 4.834
    br1 = 0.407
    br2 = -0.097
    cr0 = 116.5
    cr1 = -0.433
    cr2 = -0.082

    #  Imaginary part of epsilon...
    ai0 = 0.143
    ai1 = -0.001
    ai2 = -0.002
    bi0 = 2.746
    bi1 = 0.020
    bi2 = -0.007
    ci0 = 17.15
    ci1 = -0.112
    ci2 = 0.257

    #  Field types from Hallikainen paper [sand, silt, clay]...
    fields = np.array([[51.5, 35.1, 13.4], [42.0, 49.5, 8.5], [30.6, 55.9, 13.5], [17.2, 63.8, 19.0], [5.0, 47.6, 47.4]])

