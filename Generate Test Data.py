"""
------------------
Generate Test Data
------------------

Divirod, 15 Mar 17, M. Estes

Creates modeled test data to be used to test and refine algorithms to extract soil moisture content from GPS SNR data.
Uses the basic code in 'Systems Engineering Calcs 2.py' to generate SNR signals from the following test cases:
    1)  Bare soil of the five types used in the Hallikainen papers (fields 1-5)
    2)  Soybeans on top of loam (field 3 in Hallikainen) of varying heights and densities
    3)  Alfalfa on top of loam of varying heights and densities

Elevation varies linearly between 3-75 deg, and azimuth is fixed at 180 deg.

Test data for algorithms structured like gpsData from 'Process Matlab Data.py' - 4 columns:  Time, AZ, EL, SNR.  Time is
simply one second per sample, starting at zero, for each set.  Series, sets, and samples indexes are consecutive tuples
of integers for the following cases:
    Series:     1)  Bare soil - field 1
                2)  Bare soil - field 2
                ...
                5)  Bare soil - field 5
                6)  Soybeans - height 1
                7)  Soybeans - height 2
                ...
                11) Soybeans - height 6
                12) Alfalfa - height 1
                13) Alfalfa - height 2
                ...
                18) Alfalfa - height 6
    Sets:       1)  Soil moisture = 0.0
                2)  Soil moisture = 0.1
                ...
                6)  Soil moisture = 0.5
    Samples:    1)  First sample in set = 0
                2)  Second sample in set = 1
                ...
                3601)  Last sample in set = 3600
"""

import numpy as np
import pandas as pd
import scipy.special as spec
import scipy.signal as sig
from math import sin, cos, tan, exp, pi, asin, acos, atan, sqrt
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from Constants import Constants
from System_Calculations import SystemCalcs

c = Constants()
sc = SystemCalcs()


#  Set up global variables...

#  Create output filenames...
outputFile1 = "Modeled Test Data 1 - results.csv"               #  Contains complete results dataframe
outputFile2 = "Modeled Test Data 1 - gpsData.csv"               #  Contains 4 column output for algorithms

#  Receiver height...
rcvrHeight = 10.0

#  Number of data points in each SNR set...
numElevs = 3601
elevation = np.linspace(3.0, 75.0, numElevs)
sinEL = np.sin(c.degToRad * elevation)
azimuth = np.zeros(numElevs)
azimuth += 180.0

#  Number of vegetation heights and moisture cases to create...
numFields = c.fields.shape[0]
numHeights = 6
numMoisture = 6

#  Get phase difference between direct and reflected beams...
fringeFreq = (4.0 * pi / c.gpsWavelength) * rcvrHeight
deltaPhase = fringeFreq * sinEL

gpsAmplitude = 10.0**(sc.calcGPSPowerAtGround(elevation) / 20.0)

#  Create antenna parameters...
#
#       Antenna vector [x y z] describes its orientation, amplitude gain, phase, and polarization response
#
#           where   x is along the east-west axis and points east,
#                   y is along the vertical axis and points up, and
#                   z is along the north-south axis and points south
#

maxGain = 1.0 + 0.0j
antenna_s = maxGain * np.array([1.0, 0.0, 0.0], dtype='complex')
antenna_p = maxGain * np.array([0.0, 1.0, 0.0], dtype='complex')
antenna_lhc = maxGain * c.halfSqrtTwo * np.array([1.0, 1.0j, 0.0], dtype='complex')
antenna_rhc = maxGain * c.halfSqrtTwo * np.array([1.0, -1.0j, 0.0], dtype='complex')

"""
numAntPts = 181
antAzim = np.linspace(90.0, 270.0, numAntPts)
antElev = np.linspace(-90.0, 90.0, numAntPts)
maxGain = 1.0 + 0.0j

#  Antenna gain vs. azimuth and elevation angles...
antennaGain = np.zeros((numAntPts, 4), dtype='complex')
antennaGain[:, 0] = antAzim
antennaGain[:, 1] = antElev
antennaGain[:, 2] = np.absolute(np.lib.scimath.sqrt(maxGain) * np.cos(c.degToRad * antAzim))
antennaGain[:, 3] = np.absolute(np.lib.scimath.sqrt(maxGain) * np.cos(c.degToRad * antElev))

#  S-polarized (TE) receiving antenna...
antenna_s = np.zeros(2, dtype='complex')
antenna_s[0] = 1.0
antenna_s[1] = 0.0

#  P-polarized (TM) receiving antenna...
antenna_p = np.zeros(2, dtype='complex')
antenna_p[0] = 0.0
antenna_p[1] = 1.0

#  LHC-polarized receiving antenna...
antenna_lhc = np.zeros(2, dtype='complex')
antenna_lhc[0] = c.halfSqrtTwo
antenna_lhc[1] = c.halfSqrtTwo * 1.0j

#  RHC-polarized receiving antenna...
antenna_rhc = np.zeros(2, dtype='complex')
antenna_rhc[0] = c.halfSqrtTwo
antenna_rhc[1] = -c.halfSqrtTwo * 1.0j
"""

#  Set up results dataframe...
columnNames = ['Azimuth', 'Elevation', 'Soil Type', 'SMC', 'Veg Type', 'Veg Height',
               'Reflection Coef (s)', 'Reflection Coef (p)', 'E Direct (s)', 'E Direct (p)',
               'E Reflected (s)', 'E Reflected (p)', 'SNR (s)', 'SNR (p)', 'SNR (lhc)', 'SNR (rhc)']
dataSet = pd.DataFrame(np.zeros((numElevs, len(columnNames))), columns=columnNames)
dataSet[['Soil Type', 'Veg Type']] = dataSet[['Soil Type', 'Veg Type']].astype('str')
dataSet[['Reflection Coef (s)', 'Reflection Coef (p)']] = dataSet[['Reflection Coef (s)',
                                                                   'Reflection Coef (p)']].astype('complex')
dataSet[['E Direct (s)', 'E Direct (p)']] = dataSet[['E Direct (s)', 'E Direct (p)']].astype('complex')
dataSet[['E Reflected (s)', 'E Reflected (p)']] = dataSet[['E Reflected (s)', 'E Reflected (p)']].astype('complex')
results = dataSet[:0]

#  Create test data dataframe for algorithms (gpsData in 'Process Matlab Data.py')...
seriesNum = 0
sampleNums = np.arange(numElevs)
colNames = ['Series #', 'Set #', 'Sample #', 'Time', 'Receiver Height', 'EL', 'AZ',
            'SNR (s)', 'SNR (p)', 'SNR (lhc)', 'SNR (rhc)']
dataSubset = pd.DataFrame(np.zeros((len(sampleNums), len(colNames))), columns=colNames)
gpsData = dataSubset[:0]

#  Create arrays of soil types,  dielectric constants, and reflectivities...
moisture = np.linspace(0.0, 0.5, numMoisture)
epsilon = np.zeros((numMoisture, numFields), dtype='complex')
soilEpsilon = epsilon

rs = np.zeros(numElevs, dtype='complex')
rp = np.zeros(numElevs, dtype='complex')

mu = np.array([1.0 + 0.0j])
depth = np.array([0.0])
one = np.ones(1)

#  Incident waves on receiving antenna (direct and reflected waves)...
numWaves = 2
waves = np.zeros((numWaves, 4), dtype='complex')

print('Generate Test Data:  Variables set up...')

#  Create bare soil test cases...

for i in range(numFields):
    soilEpsilon[:, i] = sc.calcSoilEpsilon(c.fields[i, 0], c.fields[i, 2], moisture)
    epsilon = soilEpsilon

    for j in range(numMoisture):
        #  Calculate ground reflection coefficients...
        rs, rp = sc.calcReflectionCoefs(np.asarray(epsilon[j, i]) * one, mu, depth, elevation)

        #  Fill results dataframe...
        dataSet[['Azimuth', 'Elevation']] = list(zip(azimuth, elevation))
        dataSet['Soil Type'] = str(i)
        dataSet['SMC'] = moisture[j]
        dataSet['Veg Type'] = 'Bare'
        dataSet[['Reflection Coef (s)', 'Reflection Coef (p)']] = list(zip(rs, rp))
        dataSet['E Direct (s)'] = (1.0 + 0.0j) * c.halfSqrtTwo * gpsAmplitude
        dataSet['E Direct (p)'] = (0.0 - 1.0j) * c.halfSqrtTwo * gpsAmplitude
        dataSet['Reflection Coef (s)'] = rs
        dataSet['Reflection Coef (p)'] = rp
        dataSet['E Reflected (s)'] = rs * dataSet['E Direct (s)'] * np.exp(-1.0j * deltaPhase)
        dataSet['E Reflected (p)'] = rp * dataSet['E Direct (p)'] * np.exp(-1.0j * deltaPhase)

        for k in range(numElevs):
            waves[0, 0] = azimuth[k]
            waves[0, 1] = elevation[k]
            waves[0, 2] = dataSet.loc[k, 'E Direct (s)']
            waves[0, 3] = dataSet.loc[k, 'E Direct (p)']
            waves[1, 0] = azimuth[k]
            waves[1, 1] = -elevation[k]
            waves[1, 2] = dataSet.loc[k, 'E Reflected (s)']
            waves[1, 3] = dataSet.loc[k, 'E Reflected (p)']

            dataSet.loc[k, 'SNR (s)'] = sc.calcReceivedSNR(sc.calcAntennaSignal(waves, antenna_s))
            dataSet.loc[k, 'SNR (p)'] = sc.calcReceivedSNR(sc.calcAntennaSignal(waves, antenna_p))
            dataSet.loc[k, 'SNR (lhc)'] = sc.calcReceivedSNR(sc.calcAntennaSignal(waves, antenna_lhc))
            dataSet.loc[k, 'SNR (rhc)'] = sc.calcReceivedSNR(sc.calcAntennaSignal(waves, antenna_rhc))

        #  Append dataSet to results dataframe...
        results = pd.concat([results, dataSet])

        #  Append 4 columns to gpsData dataframe...
        dataSubset['Series #'] = seriesNum
        dataSubset['Set #'] = j
        dataSubset['Sample #'] = sampleNums
        dataSubset['Time'] = sampleNums
        dataSubset['Receiver Height'] = rcvrHeight
        dataSubset['AZ'] = dataSet['Azimuth']
        dataSubset['EL'] = dataSet['Elevation']
        dataSubset['SNR (s)'] = dataSet['SNR (s)']
        dataSubset['SNR (p)'] = dataSet['SNR (p)']
        dataSubset['SNR (lhc)'] = dataSet['SNR (lhc)']
        dataSubset['SNR (rhc)'] = dataSet['SNR (rhc)']
        gpsData = pd.concat([gpsData, dataSubset])

        print('Generate Test Data:  Bare soil - Field # ', i, ', SMC # ', j, ' set up...')

    seriesNum += 1

#  Create soybean test cases...

#  Create soybean dielectric constant...
soyEpsilon = 1.04 + 0.004j
numDepth = 2
epsilon = np.zeros((numMoisture, numDepth), dtype='complex')
epsilon[:, 0] = soyEpsilon
epsilon[:, 1] = soilEpsilon[:, 1]

#  Set up multilayer reflection variables...
mu = np.array([1.0 + 0.0j, 1.0 + 0.0j])
vegHeight = np.linspace(0.0, 1.0, numHeights)
depth = np.zeros((numHeights, numDepth))
depth[:, 0] = vegHeight

for i in range(numHeights):

    for j in range(numMoisture):
        #  Calculate ground reflection coefficients...
        rs, rp = sc.calcReflectionCoefs(epsilon[j, :], mu, depth[i, :], elevation)

        #  Fill results dataframe...
        dataSet[['Azimuth', 'Elevation']] = list(zip(azimuth, elevation))
        dataSet['Soil Type'] = str(i)
        dataSet['SMC'] = moisture[j]
        dataSet['Veg Type'] = 'Soy'
        dataSet[['Reflection Coef (s)', 'Reflection Coef (p)']] = list(zip(rs, rp))
        dataSet['E Direct (s)'] = (1.0 + 0.0j) * c.halfSqrtTwo * gpsAmplitude
        dataSet['E Direct (p)'] = (0.0 - 1.0j) * c.halfSqrtTwo * gpsAmplitude
        dataSet['Reflection Coef (s)'] = rs
        dataSet['Reflection Coef (p)'] = rp
        dataSet['E Reflected (s)'] = rs * dataSet['E Direct (s)'] * np.exp(-1.0j * deltaPhase)
        dataSet['E Reflected (p)'] = rp * dataSet['E Direct (p)'] * np.exp(-1.0j * deltaPhase)

        for k in range(numElevs):
            waves[0, 0] = azimuth[k]
            waves[0, 1] = elevation[k]
            waves[0, 2] = dataSet.loc[k, 'E Direct (s)']
            waves[0, 3] = dataSet.loc[k, 'E Direct (p)']
            waves[1, 0] = azimuth[k]
            waves[1, 1] = -elevation[k]
            waves[1, 2] = dataSet.loc[k, 'E Reflected (s)']
            waves[1, 3] = dataSet.loc[k, 'E Reflected (p)']

            dataSet.loc[k, 'SNR (s)'] = sc.calcReceivedSNR(sc.calcAntennaSignal(waves, antenna_s))
            dataSet.loc[k, 'SNR (p)'] = sc.calcReceivedSNR(sc.calcAntennaSignal(waves, antenna_p))
            dataSet.loc[k, 'SNR (lhc)'] = sc.calcReceivedSNR(sc.calcAntennaSignal(waves, antenna_lhc))
            dataSet.loc[k, 'SNR (rhc)'] = sc.calcReceivedSNR(sc.calcAntennaSignal(waves, antenna_rhc))

        # Append dataSet to results dataframe...
        results = pd.concat([results, dataSet])

        #  Append 4 columns to gpsData dataframe...
        dataSubset['Series #'] = seriesNum
        dataSubset['Set #'] = j
        dataSubset['Sample #'] = sampleNums
        dataSubset['Time'] = sampleNums
        dataSubset['AZ'] = dataSet['Azimuth']
        dataSubset['EL'] = dataSet['Elevation']
        dataSubset['SNR (s)'] = dataSet['SNR (s)']
        dataSubset['SNR (p)'] = dataSet['SNR (p)']
        dataSubset['SNR (lhc)'] = dataSet['SNR (lhc)']
        dataSubset['SNR (rhc)'] = dataSet['SNR (rhc)']
        gpsData = pd.concat([gpsData, dataSubset])

        print('Generate Test Data:  Soybeans - Veg height # ', i, ', SMC # ', j, ' set up...')

    seriesNum += 1

#  Create alfalfa test cases...

#  Create alfalfa dielectric constant...
alfalfaEpsilon = 1.06 + 0.013j
numDepth = 2
epsilon = np.zeros((numMoisture, numDepth), dtype='complex')
epsilon[:, 0] = alfalfaEpsilon
epsilon[:, 1] = soilEpsilon[:, 1]

#  Set up multilayer reflection variables...
mu = np.array([1.0 + 0.0j, 1.0 + 0.0j])
vegHeight = np.linspace(0.0, 1.0, numHeights)
depth = np.zeros((numHeights, numDepth))
depth[:, 0] = vegHeight

for i in range(numHeights):

    for j in range(numMoisture):
        #  Calculate ground reflection coefficients...
        rs, rp = sc.calcReflectionCoefs(epsilon[j, :], mu, depth[i, :], elevation)

        #  Fill results dataframe...
        dataSet[['Azimuth', 'Elevation']] = list(zip(azimuth, elevation))
        dataSet['Soil Type'] = str(i)
        dataSet['SMC'] = moisture[j]
        dataSet['Veg Type'] = 'Alfalfa'
        dataSet[['Reflection Coef (s)', 'Reflection Coef (p)']] = list(zip(rs, rp))
        dataSet['E Direct (s)'] = (1.0 + 0.0j) * c.halfSqrtTwo * gpsAmplitude
        dataSet['E Direct (p)'] = (0.0 - 1.0j) * c.halfSqrtTwo * gpsAmplitude
        dataSet['Reflection Coef (s)'] = rs
        dataSet['Reflection Coef (p)'] = rp
        dataSet['E Reflected (s)'] = rs * dataSet['E Direct (s)'] * np.exp(-1.0j * deltaPhase)
        dataSet['E Reflected (p)'] = rp * dataSet['E Direct (p)'] * np.exp(-1.0j * deltaPhase)

        for k in range(numElevs):
            waves[0, 0] = azimuth[k]
            waves[0, 1] = elevation[k]
            waves[0, 2] = dataSet.loc[k, 'E Direct (s)']
            waves[0, 3] = dataSet.loc[k, 'E Direct (p)']
            waves[1, 0] = azimuth[k]
            waves[1, 1] = -elevation[k]
            waves[1, 2] = dataSet.loc[k, 'E Reflected (s)']
            waves[1, 3] = dataSet.loc[k, 'E Reflected (p)']

            dataSet.loc[k, 'SNR (s)'] = sc.calcReceivedSNR(sc.calcAntennaSignal(waves, antenna_s))
            dataSet.loc[k, 'SNR (p)'] = sc.calcReceivedSNR(sc.calcAntennaSignal(waves, antenna_p))
            dataSet.loc[k, 'SNR (lhc)'] = sc.calcReceivedSNR(sc.calcAntennaSignal(waves, antenna_lhc))
            dataSet.loc[k, 'SNR (rhc)'] = sc.calcReceivedSNR(sc.calcAntennaSignal(waves, antenna_rhc))

        # Append dataSet to results dataframe...
        results = pd.concat([results, dataSet])

        #  Append 4 columns to gpsData dataframe...
        dataSubset['Series #'] = seriesNum
        dataSubset['Set #'] = j
        dataSubset['Sample #'] = sampleNums
        dataSubset['Time'] = sampleNums
        dataSubset['AZ'] = dataSet['Azimuth']
        dataSubset['EL'] = dataSet['Elevation']
        dataSubset['SNR (s)'] = dataSet['SNR (s)']
        dataSubset['SNR (p)'] = dataSet['SNR (p)']
        dataSubset['SNR (lhc)'] = dataSet['SNR (lhc)']
        dataSubset['SNR (rhc)'] = dataSet['SNR (rhc)']
        gpsData = pd.concat([gpsData, dataSubset])

        print('Generate Test Data:  Alfalfa - Veg height # ', i, ', SMC # ', j, ' set up...')

    seriesNum += 1

#  Finally, write output data files...
results.to_csv(outputFile1)
gpsData.to_csv(outputFile2)

print('Generate Test Data:  Output files written.  Done.')
