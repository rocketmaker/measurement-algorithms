"""
------------
System_Parameters.py
------------

Divirod, 8 Mar 17, M. Estes

Class definition of system parameters.
"""

from math import pi
import numpy as np
import pandas as pd
import scipy.special as spec

class systemParams ():
    """

    """

    #  Constants...
    receiverSensitivity = -167.0        #  GPS receiver sensitivity (dbm)

    def __init__(self, systemFile):
        """

        """

        #  Read in system parameters from systemFile...

        #  Read in
        sysParams = pd.read_csv(systemFile, chunksize=1)

        #  Read in antenna gain pattern (complex)...
