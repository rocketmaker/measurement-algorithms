"""
---------------------------
Systems Engineering Calcs 1
---------------------------

Divirod, 3 Mar 17, M. Estes

Basic systems engineering calculations for the Divirod Soil Moisture Monitoring System:
    1)  Calculate phase and intensity pattern of Fresnel zone
        -   Calculate effective spot size at FWHM - plot total power vs. spot radius
    2)  Calculate reflection spot size
        -   Calculate spot size vs. sin(EL) and vs. radius and vs. receiver height
        -   Calculate overlap of adjacent samples for various sample times
        -   Calculate azimuthal overlap of adjacent satellite tracks vs. radius
    3)  Calculate effect of two receivers at difference heights
        -   Spot resolution
        -   Covariance between signals
    4)  Calculate effect of vegetation on signal
        -   At various vegetation densities
        -   At various vegetation heights
        -   At various elevation angles
    5)  Calculate RHCP vs. LHCP reflections
        -   At various SMCs
        -   At various vegetation densities and heights
        -   Calculate signal at L1 and L2 frequencies
"""

import Constants as constants
import numpy as np
import pandas as pd
import scipy.special as spec
import scipy.signal as sig
from math import sin, cos, tan, exp, pi, asin, acos, atan, sqrt
import cmath
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

c = constants.constants()

def calcSatelliteDistance (elevation):
    """
    Given an apparent elevation angle to the GPS satellite from a point on the ground, calculate the distance to the
    satellite.
    :param elevation:       Satellite elevation angle from point on the ground, numpy array of length N, 0.0 to <90.0 (deg)
    :return satDistance:    Distance between satellite and point on the the ground, numpy array of length N (m)
    """

    elevRad = c.degToRad * elevation

    #  Calculate angles...
    beta = np.arcsin(c.rEarth / (c.rEarth + c.gpsOrbitHeight) * np.cos(elevRad))
    alpha = np.pi / 2.0 - elevRad - beta

    #  Calculate satellite height at ground point...
    satDistance = (c.rEarth + c.gpsOrbitHeight) * np.sin(alpha) / np.cos(elevRad)

    return satDistance

def calcGPSPowerAtGround (elevation):
    """
    Given an apparent elevation angle to the GPS satellite from a point on the ground, calculate the power incident
    on the receive antenna.
    :param elevation:       Satellite elevation angle from a point on the ground, numpy array of length N, 0.0 to <90.0 (deg)
    :return power:          Microwave power incident on receive antenna (dBm)
    """

    #  Calculate distance to satellite from ground...
    d = calcSatelliteDistance(elevation)

    #  Calculate microwave power loss between satellite and ground...
    lossdB = -10.0 * np.log10((4.0 * np.pi * d / c.gpsWavelength)**2.0)

    #  Calculate microwave power at receive antenna...
    power = c.gpsPower + lossdB

    return power

def calcReflectionSpot (rcvrHeight, elevation):
    """
    Given a receiver height above the ground and a satellite elevation angle, calculate the refelction spot dimensions
    on the ground and the horizontal distance between the receiver and the reflection spot center point.
    :param rcvrHeight:      Average height of the GPS receiver above the ground, numpy array of length N (m)
    :param elevation:       Satellite elevation angle from point on the ground, numpy array of length N (deg)
    :return spotParams:     List of spot parameters - [spot width (m), spot length (m), spot distance (m)], Nx3 numpy array
    """

    elevRad = c.degToRad * elevation

    #  Calculate horizontal distance between spot center point and receiver...
    spotDistance = rcvrHeight / np.tan(elevRad)

    #  Calculate distances between receiver and spot and satellite and spot...
    d1 = rcvrHeight / np.sin(elevRad)
    d2 = calcSatelliteDistance(elevation)

    #  Calculate width of first Fresnel zone (spot) transverse to line between spot and receiver...
    spotWidth = 2.0 * np.sqrt(c.gpsWavelength * d1 * d2 / (d1 + d2))

    #  Calculate length of first Fresnel zone along line between spot and receiver...
    spotLength = spotWidth / np.sin(elevRad)

    results = np.zeros((elevation.shape[0], 3))
    results[:, 0] = spotWidth
    results[:, 1] = spotLength
    results[:, 2] = spotDistance

    return results

def calcAiryDisk (spotWidth, spotLength, x, y):
    """
    Given spot width (full width) and spot length (full length), and relative distances from spot center to edges of
    width and length (radial distances to a point, x,y), calculate and return the value of the airy disk function.
    :param spotWidth:       Full width (diameter) of elliptical airy disk spot, numpy array of length N (m)
    :param spotLength:      Full length (diameter) of elliptical airy disk spot, numpy array of length N (m)
    :param x:               Relative distance between spot center and width-wise edge of airy disk, numpy array of length N (m)
    :param y:               Relative distance between spot center and length-wise edge of airy disk, numpy array of length N (m)
    :return intensity:      Relative intensity of airy disk function at (x, y) --> intensity at (0, 0) = 1.0, numpy array of length N
    """

    #  Calculate scaling factors for width and length...
    widthScale = c.airyDiskZero / (0.5 * spotWidth)
    lengthScale = c.airyDiskZero / (0.5 * spotLength)

    xScaled = widthScale * x
    yScaled = lengthScale * y

    #  Calculate effective radius to (x, y)...
    r = np.sqrt(xScaled**2.0 + yScaled**2.0)

    nonZero = np.where(r > 0.)
    zero = np.where(r == 0.)

    intensity = np.zeros(r.shape[0])
    intensity[nonZero] = 4.0 * (spec.j1(r[nonZero]) / r[nonZero])**2.0
    intensity[zero] = 1.0

    return intensity

def calcAiryDiskPower (spotWidth, radius):
    """
    Given a reflection spot width (full width) and a radius, calculate the encircled power in the center portion of the
    airy disk out to the radius (in the spot width dimension).  Returned power is normalized to 1.0 (max).
    :param spotWidth:       Full width (diameter) of elliptical airy disk spot, numpy array of length N (m)
    :param radius:          Radius in spot width dimension with which to calculate encircled power, numpy array of length N (m)
    :return relPower:       Relative encircled power within radius, numpy array of length N
    """

    #  Calculate scaled radius...
    widthScale = c.airyDiskZero / (0.5 * spotWidth)
    rScaled = widthScale * radius

    #  Calculate encircled power out to radius...
    relPower = 1.0 - spec.j0(rScaled)**2.0 - spec.j1(rScaled)**2.0

    return relPower

def calcReflectionCoefs (epsilon, mu, depth, elevation):
    """
    Given a set of soil/vegetation dielectric permittivities (epsilon, complex), magnetic permeabilities (mu, complex)
    and associated layer depths along with a set of satellite elevation angles (incidence angle = elevation angle),
    calculate and return the s- and p-polarized field reflectivities at each elevation/incidence angle.
    :param epsilon:         Relative dielectric permittivities (complex) of each layer, starting with the top layer;
                            numpy array of length N.  The value of the first epsilon should be 1.0 as the top layer
                            is air.  The value of the last epsilon is assumed to be the underlying soil with infinite
                            depth
    :param mu:              Relative magnetic permeabilities (complex) of each layer, starting with the top layer;
                            numpy array of length N.  The value of the first mu should be 1.0 as the top layer is air.
                            The value of the last mu is assumed to be the underlying soil with infinite depth
    :param depth:           Depth/thickness of each layer, starting with top layer; numpy array of length N (m).  The
                            values of the first (air) and last depths are ignored as these layers are assumed to be
                            infinitely thick
    :param elevation:       Satellite elevation angle from point on the ground; numpy array of length M (deg)
    :return [rs, rp]:       Complex field reflection coefficients for s- and p-polarizations
    """

    #  Get number of layers, elevations...
    numLayers = epsilon.shape[0]
    numElevs = elevation.shape[0]

    if numLayers != depth.shape[0]:
        print('calcReflectionCoefs:  Size of epsilon and depth arrays not equal.')
        return

    #  Calculate the complex index of refraction of each layer...
    n = np.lib.scimath.sqrt(epsilon * mu)
    n=[0] = 1.0 + 1.0j

    #  NOTE:  Propagation angles are relative to the normal of the surface/interface

    #  Calculate propagtion angle (rad) through each layer...
    theta = np.zeros((numLayers, numElevs))
    theta[0, :] = c.radToDeg * (90.0 - elevation)

    for i in range(1, numLayers):
        theta[i, :] = np.arcsin(n[i-1] / n[i] * np.sin(theta[i-1, :]))

    #  Calculate transfer matrices for s- and p-polarizations...
    ident = np.identity(2)
    ms = np.ndarray(shape=(numElevs, 2, 2))
    ms[:] = ident
    mp = np.ndarray(shape=(numElevs, 2, 2))
    mp[:] = ident
    mTemp = np.zeros((numElevs, 2, 2))
    k0 = 2.0 * pi / c.gpsWavelength
    p = np.lib.scimath.sqrt(epsilon / mu)
    q = 1.0 / p

    for i in range(1, numLayers-1):
        cosTheta = np.cos(theta[i, :]
        #  s-polarization...
        mTemp[:, 0, 0] = np.cos(k0 * n * depth * cosTheta))
        mTemp[:, 1, 1] = mTemp[:, 0, 0]
        mTemp[:, 0, 1] = -1.0j / (p[i] * cosTheta) * np.sin(k0 * n[i] * depth[i] * cosTheta)
        mTemp[:, 1, 0] = -1.0j * (p[i] * cosTheta) * np.sin(k0 * n[i] * depth[i] * cosTheta)
        ms[:] = np.dot(ms[:], mTemp[:])

        #  p-polarization...
        mTemp[:, 0, 1] = -1.0j / (q[i] * cosTheta) * np.sin(k0 * n[i] * depth * cosTheta)
        mTemp[:, 1, 0] = -1.0j * (q[i] * cosTheta) * np.sin(k0 * n[i] * depth * cosTheta)
        mp[:] = np.dot(mp[:], mTemp[:])

    #  Calculate reflectivities...
    p1 = p * np.cos(theta[0, :])
    pl = p * np.cos(theta[numLayers-1, :])
    rsNum = (ms[:, 0, 0] + ms[:, 0, 1] * pl) * p1 - (ms[:, 1, 0] + ms[:, 1, 1] * pl)
    rsDenom = (ms[:, 0, 0] + ms[:, 0, 1] * pl) * p1 + (ms[:, 1, 0] + ms[:, 1, 1] * pl)
    rs = rsNum / rsDenom

    q1 = q * np.cos(theta[0, :])
    ql = q * np.cos(theta[numLayers-1, :])
    rpNum = (ms[:, 0, 0] + ms[:, 0, 1] * ql) * q1 - (ms[:, 1, 0] + ms[:, 1, 1] * ql)
    rpDenom = (ms[:, 0, 0] + ms[:, 0, 1] * ql) * q1 + (ms[:, 1, 0] + ms[:, 1, 1] * ql)
    rp = rpNum / rpDenom

    return np.concatenate((rs, rp), axis=1)

def dBmToWatts (dBmValue):
    """
    Given a power in dBm, convert it to linear Watts and return the value in Watts
    :param dBmvalue:        Scalar power value (dBm)
    :return watts:          Power in Watts (W)
    """

    #  Convert dBm value to linear Watts...
    watts = 10.0**((dBmValue - 30.0) / 10.0)

    return watts

def absoluteSquared (x):
    """
    Given a complex value x, calculate and return the absolute value squared of x
    :param x:               Complex value; scalar or numpy array of length N
    :return abs(x)**2.0:    Absolute value squared of x; scalar or numpy array of length N
    """

    return x.real**2.0 + x.imag**2.0

def calcAntennaSignal (rs, rp, elevation, h, antenna):
    """
    Given the ground reflection coefficients, rs and rp, the elevation angle, the receiver height, h, and the antenna
    gain pattern (complex) for the four fundamental polarizations, calculate and return the antenna output signal.
    Method assumes that the incident GPS microwave carrier is purely right-hand circularly polarized (RHCP)
    :param rs:              Ground relectivity coefficient (complex) for s-polarization; numpy array of length N
    :param rp:              Ground relectivity coefficient (complex) for p-polarization; numpy array of length N
    :param elevation:       Satellite elevation angle; numpy array of length N (deg)
    :param h:               Receiver height above the ground; scalar (m)
    :param antenna:         Antenna gain/radiation pattern (complex) for all four polarizations; numpy array of
                            size Nx4 (dB, rad)
    :return signal:         Antenna output signal (proportional to volts)
    """

    #  Calculate field amplitudes of the direct wave...
    powerDirect = calcGPSPowerAtGround(elevation)
    amplitudeDirect_rhc = np.sqrt(dBmToWatts(powerDirect))

    #  Calculate geometric phase difference between direct and reflected waves (relative to reflecting surface)...
    deltaPath = np.exp(-1.0j * 4.0 * pi * h * np.sin(c.degToRad * elevation) / c.gpsWavelength)

    #  Calculate the s- and p-polarized amplitudes of the reflected wave...
    sqrtOneHalf = sqrt(0.5)
    amplitudeReflected_s = sqrtOneHalf * amplitudeDirect_rhc * rs * deltaPath
    amplitudeReflected_p = sqrtOneHalf * amplitudeDirect_rhc * rp * deltaPath

    #  Calculate the detected antenna signal...
    signal_s = antenna[:, 0] * amplitudeReflected_s
    signal_p = antenna[:, 1] * amplitudeReflected_p
    signal_rhc = antenna[:, 2] * 0.5 * (amplitudeReflected_s + amplitudeReflected_p)
    signal_lhc = antenna[:, 3] * 0.5 * (amplitudeReflected_s - amplitudeReflected_p)

    signal = signal_s + signal_p + signal_rhc + signal_lhc

    return signal

def calcReceivedSNR (signal):
    """
    Given an antenna output signal (or sum of signals), calculate and return the received signal-to-noise ratio (SNR)
    :param signal:          Net antenna output signal; numpy array of length N (proportional to volts)
    :return snr:            Received SNR (dB)
    """

    #  Calculate received SNR...
    snr = absoluteSquared(signal)

    return snr

"""
    1)  Calculate intensity pattern of the first Fresnel zone
        -   Calculate effective spot size at FWHM - plot total power vs. spot radius
"""

#


"""
    2)  Calculate reflection spot size
        -   Calculate spot size vs. sin(EL) and vs. radius and vs. receiver height
        -   Calculate overlap of adjacent samples for various sample times
        -   Calculate azimuthal overlap of adjacent satellite tracks vs. radius
"""

numElevs = 179
elevation = np.linspace(3.0, 75.0, numElevs)

numHeights = 15
rcvrHeight = np.linspace(2.0, 30.0, numHeights)

colNames = ['Receiver Height', 'sin(EL)', 'Spot Width', 'Spot Length', 'Spot Distance']
spot = pd.DataFrame(np.zeros((numElevs, 5)), columns=colNames)
allSpots = pd.DataFrame(np.zeros((0, 5)), columns=colNames)

#  Calculate and plot results...
outputFile = "Spot Size Plots.pdf"

with PdfPages(outputFile) as pdf:

    for h in range(numHeights):
        spot[['Spot Width', 'Spot Length', 'Spot Distance']] = calcReflectionSpot(rcvrHeight[h], elevation)
        spot['Receiver Height'] = rcvrHeight[h]
        spot['sin(EL)'] = np.sin(c.degToRad * elevation)

        allSpots = pd.concat((allSpots, spot), axis=0)

        fig, axs = plt.subplots()
        axs.plot(spot['sin(EL)'], spot['Spot Width'])
        plotTitle = 'Spot Width vs. sin(EL), Receiver Height = ' + str(rcvrHeight[h]) + ' m'
        axs.set_title(plotTitle)
        axs.set_xlabel('sin(Elevation)')
        axs.set_ylabel('Spot Width (m)')
        axs.grid()
        plt.show()
        plt.close()
        pdf.savefig(fig)

        fig, axs = plt.subplots()
        axs.plot(spot['sin(EL)'], spot['Spot Length'])
        plotTitle = 'Spot Length vs. sin(EL), Receiver Height = ' + str(rcvrHeight[h]) + ' m'
        axs.set_title(plotTitle)
        axs.set_xlabel('sin(Elevation)')
        axs.set_ylabel('Spot Length (m)')
        axs.grid()
        plt.show()
        plt.close()
        pdf.savefig(fig)

        fig, axs = plt.subplots()
        axs.plot(spot['sin(EL)'], spot['Spot Distance'])
        plotTitle = 'Spot Distance vs. sin(EL), Receiver Height = ' + str(rcvrHeight[h]) + ' m'
        axs.set_title(plotTitle)
        axs.set_xlabel('sin(Elevation)')
        axs.set_ylabel('Spot Distance (m)')
        axs.grid()
        plt.show()
        plt.close()
        pdf.savefig(fig)

        fig, axs = plt.subplots()
        axs.errorbar(spot['Spot Distance'], spot['Spot Width'], xerr=0.5 * spot['Spot Width'])
        plotTitle = 'Spot Width vs. Spot Distance, Receiver Height = ' + str(rcvrHeight[h]) + ' m'
        axs.set_title(plotTitle)
        axs.set_xlabel('Spot Distance (m)')
        axs.set_ylabel('Spot Width (m)')
        axs.grid()
        plt.show()
        plt.close()
        pdf.savefig(fig)

        fig, axs = plt.subplots()
        axs.errorbar(spot['Spot Distance'], spot['Spot Length'], xerr=0.5 * spot['Spot Length'])
        plotTitle = 'Spot Length vs. Spot Distance, Receiver Height = ' + str(rcvrHeight[h]) + ' m'
        axs.set_title(plotTitle)
        axs.set_xlabel('Spot Distance (m)')
        axs.set_ylabel('Spot Length (m)')
        axs.grid()
        plt.show()
        plt.close()
        pdf.savefig(fig)

    #  Calculate and plot maximum spot length, width, and distance vs. receiver height...
    sin3deg = sin(c.degToRad * 3.0)
    maxSpotDistance = allSpots[allSpots['sin(EL)'] == sin3deg]
    maxSpotLength = allSpots[allSpots['sin(EL)'] == sin3deg]
    maxSpotWidth = allSpots[allSpots['sin(EL)'] == sin3deg]

    fig, axs = plt.subplots()
    axs.semilogy(rcvrHeight, maxSpotDistance['Spot Distance'], label='Max Spot Distance')
    axs.semilogy(rcvrHeight, maxSpotLength['Spot Length'], label='Max Spot Length')
    axs.semilogy(rcvrHeight, maxSpotWidth['Spot Width'], label='Max Spot Width')
    axs.set_title('Max Spot Distance vs. Receiver Height')
    axs.set_xlabel('Receiver Height (m)')
    axs.set_ylabel('Spot Distance (m)')
    axs.legend()
    axs.grid()
    plt.show()
    plt.close()
    pdf.savefig(fig)

    #  Calculate and plot Fresnel zone diameter vs. sin(EL) for three different equations...
    h = 10.0
    sinEL = np.sin(c.degToRad * elevation)
    r1 = 2.0 * np.sqrt(c.gpsWavelength * h * calcSatelliteDistance(elevation) / (h + calcSatelliteDistance(elevation) * np.sin(c.degToRad * elevation)))
    r2 = 2.0 * np.sqrt(c.gpsWavelength * h / np.sin(c.degToRad * elevation))
    r3 = 2.0 * np.sqrt(c.gpsWavelength * h / np.sin(c.degToRad * elevation) + (c.gpsWavelength / (2.0 * np.sin(c.degToRad * elevation)))**2.0)

    fig, axs = plt.subplots()
    axs.plot(sinEL, r1, label='Wikipedia Equation')
    axs.plot(sinEL, r2, label='Simplified Wikipedia Equation')
    axs.plot(sinEL, r3, label='Larson Equation')
    axs.set_title('Comparison of Fresnel Zone Width Equations')
    axs.set_xlabel('sin(EL)')
    axs.set_ylabel('Fresnel Zone Diameter (m)')
    axs.legend()
    axs.grid()
    plt.show()
    plt.close()
    pdf.savefig(fig)

#    3)  Calculate effect of two receivers at difference heights
#        -   Spot resolution
#        -   Covariance between signals



#    4)  Calculate effect of vegetation on signal
#        -   At various vegetation densities
#        -   At various vegetation heights
#        -   At various elevation angles



#    5)  Calculate RHCP vs. LHCP reflections
#        -   At various SMCs
#        -   At various vegetation densities and heights
#        -   Calculate signal at L1 and L2 frequencies
