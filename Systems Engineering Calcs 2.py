"""
---------------------------
Systems Engineering Calcs 2
---------------------------

Divirod, 8 Mar 17, M. Estes

Changes from Systems Engineering Calcs 1:
    -   Moved methods to separate file, System_Calculations.py, with class, systemCalcs
    -   Added calculations of vegetation loss at different elevation angles, vegetation types

Basic systems engineering calculations for the Divirod Soil Moisture Monitoring System:
    1)  Calculate intensity pattern of Fresnel zone
        -   Calculate effective spot size at FWHM - plot total power vs. spot radius
    2)  Calculate reflection spot size
        -   Calculate spot size vs. sin(EL) and vs. radius and vs. receiver height
        -   Calculate overlap of adjacent samples for various sample times
        -   Calculate azimuthal overlap of adjacent satellite tracks vs. radius
    3)  Calculate effect of two receivers at difference heights
        -   Spot resolution
        -   Covariance between signals
    4)  Calculate effect of vegetation on signal
        -   At various vegetation densities
        -   At various vegetation heights
        -   At various elevation angles
    5)  Calculate RHCP vs. LHCP reflections
        -   At various SMCs
        -   At various vegetation densities and heights
        -   Calculate signal at L1 and L2 frequencies
"""

import numpy as np
import pandas as pd
import scipy.special as spec
import scipy.signal as sig
from math import sin, cos, tan, exp, pi, asin, acos, atan, sqrt
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from Constants import Constants
from System_Calculations import SystemCalcs

c = Constants()
sc = SystemCalcs()

numElevs = 217
elevation = np.linspace(3.0, 75.0, numElevs)
azimuth = np.zeros(numElevs)

numHeights = 6
rcvrHeight = np.linspace(5.0, 30.0, numHeights)

deltaPhase = np.zeros((numHeights, numElevs), dtype='complex')
sinEL = np.sin(c.degToRad * elevation)
fringeFreq = (4.0 * np.pi / c.gpsWavelength) * rcvrHeight
for i in range(numHeights):
    deltaPhase[i, :] = fringeFreq[i] * sinEL

gpsAmplitude = 10.0**(sc.calcGPSPowerAtGround(elevation) / 20.0)

#  Create antenna parameters...
#
#       Antenna vector [x y z] describes its orientation, amplitude gain, phase, and polarization response
#
#           where   x is along the east-west axis and points east,
#                   y is along the vertical axis and points up, and
#                   z is along the north-south axis and points south
#

maxGain = 1.0 + 0.0j
antenna_s = maxGain * np.array([1.0, 0.0, 0.0], dtype='complex')
antenna_p = maxGain * np.array([0.0, 1.0, 0.0], dtype='complex')
antenna_lhc = maxGain * c.halfSqrtTwo * np.array([1.0, 1.0j, 0.0], dtype='complex')
antenna_rhc = maxGain * c.halfSqrtTwo * np.array([1.0, -1.0j, 0.0], dtype='complex')

#  Set up results dataframe...
columnNames = ['Azimuth', 'Elevation', 'Soil Type', 'SMC', 'Veg Type', 'Veg Height',
               'Reflection Coef (s)', 'Reflection Coef (p)', 'E Direct (s)', 'E Direct (p)',
               'E Reflected (s)', 'E Reflected (p)', 'SNR (s)', 'SNR (p)', 'SNR (lhc)', 'SNR (rhc)']
dataSet = pd.DataFrame(np.zeros((numElevs, len(columnNames))), columns=columnNames)
dataSet[['Soil Type', 'Veg Type']] = dataSet[['Soil Type', 'Veg Type']].astype('str')
dataSet[['Reflection Coef (s)', 'Reflection Coef (p)']] = dataSet[['Reflection Coef (s)', 'Reflection Coef (p)']].astype('complex')
dataSet[['E Direct (s)', 'E Direct (p)']] = dataSet[['E Direct (s)', 'E Direct (p)']].astype('complex')
dataSet[['E Reflected (s)', 'E Reflected (p)']] = dataSet[['E Reflected (s)', 'E Reflected (p)']].astype('complex')
results = dataSet[:0]

print('Systems Engineering Calcs 2:  Variables set up...')

#  Create output PDF file and start plotting...
outputFile = "Spot Size Plots - 3 Apr 17.pdf"

with PdfPages(outputFile) as pdf:

    #    1)  Calculate intensity pattern of the first Fresnel zone
    #        -   Calculate effective spot size at FWHM - plot total power vs. spot radius

    numDia = 101
    diameter = np.linspace(-1.0, 1.0, numDia)
    radius = diameter[int(0.5 * numDia):]

    spotPattern = sc.calcAiryDisk(2.0 * c.airyDiskZero, 2.0 * c.airyDiskZero, diameter * c.airyDiskZero, 0.0)
    spotPower = sc.calcAiryDiskPower(2.0 * c.airyDiskZero, diameter[int(0.5 * numDia):] * c.airyDiskZero)
    spotPower /= spotPower.max()

    fig, axs = plt.subplots()
    axs.plot(diameter, spotPattern, label='Intensity Pattern')
    axs.plot(radius, spotPower, label='Encircled Power')
    axs.set_title('Fresnel Zone Intensity Pattern and Encircled Power')
    axs.set_xlabel('Relative Fresnel Zone Diameter')
    axs.set_ylabel('Relative Intensity/Power')
    axs.legend()
    axs.grid()
    plt.show()
    plt.close()
    pdf.savefig(fig)

    #    2)  Calculate reflection spot size
    #        -   Calculate spot size vs. sin(EL) and vs. radius and vs. receiver height
    #        -   Calculate overlap of adjacent samples for various sample times
    #        -   Calculate azimuthal overlap of adjacent satellite tracks vs. radius

    colNames = ['Receiver Height', 'sin(EL)', 'Spot Width', 'Spot Length 1', 'Spot Length 2', 'Spot Distance',
                'Theta 1', 'Theta 2']
    spot = pd.DataFrame(np.zeros((numElevs, len(colNames))), columns=colNames)
    allSpots = pd.DataFrame(np.zeros((0, len(colNames))), columns=colNames)

    #  Calculate and plot results...
    for h in range(numHeights):
        spot[['Spot Width', 'Spot Length 1', 'Spot Length 2', 'Spot Distance', 'Theta 1', 'Theta 2']] \
            = sc.getReflectionSpot(rcvrHeight[h], elevation)
        spot['Receiver Height'] = rcvrHeight[h]
        spot['sin(EL)'] = np.sin(c.degToRad * elevation)

        allSpots = pd.concat((allSpots, spot), axis=0)

        fig, axs = plt.subplots()
        axs.plot(spot['sin(EL)'], spot['Spot Width'])
        plotTitle = 'Spot Width vs. sin(EL), Receiver Height = ' + str(rcvrHeight[h]) + ' m'
        axs.set_title(plotTitle)
        axs.set_xlabel('sin(Elevation)')
        axs.set_ylabel('Spot Width (m)')
        axs.grid()
        plt.show()
        plt.close()
        pdf.savefig(fig)

        fig, axs = plt.subplots()
        axs.plot(spot['sin(EL)'], spot['Spot Length 1'] + spot['Spot Length 2'])
        plotTitle = 'Spot Length vs. sin(EL), Receiver Height = ' + str(rcvrHeight[h]) + ' m'
        axs.set_title(plotTitle)
        axs.set_xlabel('sin(Elevation)')
        axs.set_ylabel('Spot Length (m)')
        axs.grid()
        plt.show()
        plt.close()
        pdf.savefig(fig)

        fig, axs = plt.subplots()
        axs.plot(spot['sin(EL)'], spot['Spot Distance'])
        plotTitle = 'Spot Distance vs. sin(EL), Receiver Height = ' + str(rcvrHeight[h]) + ' m'
        axs.set_title(plotTitle)
        axs.set_xlabel('sin(Elevation)')
        axs.set_ylabel('Spot Distance (m)')
        axs.grid()
        plt.show()
        plt.close()
        pdf.savefig(fig)

        fig, axs = plt.subplots()
        axs.errorbar(spot['Spot Distance'], spot['Spot Width'], fmt='bo', xerr=0.5 * spot['Spot Width'])
        plotTitle = 'Spot Width vs. Spot Distance, Receiver Height = ' + str(rcvrHeight[h]) + ' m'
        axs.set_title(plotTitle)
        axs.set_xlabel('Spot Distance (m)')
        axs.set_ylabel('Spot Width (m)')
        axs.grid()
        plt.show()
        plt.close()
        pdf.savefig(fig)

        fig, axs = plt.subplots()
        axs.errorbar(spot['Spot Distance'], spot['Spot Length 1'] + spot['Spot Length 2'], fmt='go',
                     xerr=[spot['Spot Length 1'], spot['Spot Length 2']])
        plotTitle = 'Spot Length vs. Spot Distance, Receiver Height = ' + str(rcvrHeight[h]) + ' m'
        axs.set_title(plotTitle)
        axs.set_xlabel('Spot Distance (m)')
        axs.set_ylabel('Spot Length (m)')
        axs.grid()
        plt.show()
        plt.close()
        pdf.savefig(fig)

    #  Calculate and plot maximum spot length, width, and distance vs. receiver height...
    sin3deg = sin(c.degToRad * 3.0)
    maxSpotDistance = allSpots[allSpots['sin(EL)'] == sin3deg]
    maxSpotLength = allSpots[allSpots['sin(EL)'] == sin3deg]
    maxSpotWidth = allSpots[allSpots['sin(EL)'] == sin3deg]

    fig, axs = plt.subplots()
    axs.semilogy(rcvrHeight, maxSpotDistance['Spot Distance'], label='Max Spot Distance')
    axs.semilogy(rcvrHeight, maxSpotLength[' 1'] + maxSpotLength['Spot Length 1'], label='Max Spot Length')
    axs.semilogy(rcvrHeight, maxSpotWidth['Spot Width'], label='Max Spot Width')
    axs.set_title('Max Spot Distance vs. Receiver Height')
    axs.set_xlabel('Receiver Height (m)')
    axs.set_ylabel('Spot Distance (m)')
    axs.legend()
    axs.grid()
    plt.show()
    plt.close()
    pdf.savefig(fig)

    #  Calculate and plot Fresnel zone diameter vs. sin(EL) for three different equations...
    h = 10.0
    r1 = 2.0 * np.sqrt(c.gpsWavelength * h * sc.calcSatelliteDistance(elevation)
                       / (h + sc.calcSatelliteDistance(elevation) * np.sin(c.degToRad * elevation)))
    r2 = 2.0 * np.sqrt(c.gpsWavelength * h / np.sin(c.degToRad * elevation))
    r3 = 2.0 * np.sqrt(c.gpsWavelength * h / np.sin(c.degToRad * elevation)
                       + (c.gpsWavelength / (2.0 * np.sin(c.degToRad * elevation)))**2.0)

    fig, axs = plt.subplots()
    axs.plot(sinEL, r1, label='Wikipedia Equation')
    axs.plot(sinEL, r2, label='Simplified Wikipedia Equation')
    axs.plot(sinEL, r3, label='Larson Equation')
    axs.set_title('Comparison of Fresnel Zone Width Equations')
    axs.set_xlabel('sin(EL)')
    axs.set_ylabel('Fresnel Zone Diameter (m)')
    axs.grid()
    plt.show()
    plt.close()
    pdf.savefig(fig)

    #  Plot SNR interference fringe frequency vs. receiver height...
    fig, axs = plt.subplots()
    axs.plot(rcvrHeight, (0.5 / np.pi) * fringeFreq)
    axs.set_title('SNR Interference Fringe Frequency')
    axs.set_xlabel('Receiver Height (m)')
    axs.set_ylabel('Frequency (fringes per 90 deg elev)')
    axs.grid()
    plt.show()
    plt.close()
    pdf.savefig(fig)

    plt.close('all')

    print('Systems Engineering Calcs 2:  Spot size plots printed...')

    #    3)  Calculate effect of multiple receivers at difference heights
    #        -   Spot resolution
    #        -   Ability to resolve SMC depth for a given point on the ground
    #        -   Covariance between signals

    #    4)  Calculate effect of soil type, soil moisture, and vegetation on signal
    #        -   At various vegetation densities
    #        -   At various vegetation heights
    #        -   At various elevation angles
    #        -   For the four fundamental polarization types
    #        -   At L1 and L2 frequencies

    #  Create arrays of soil types and dielectric constants and reflectivities...
    numMoisture = 6
    moisture = np.linspace(0.0, 0.5, numMoisture)
    numFields = c.fields.shape[0]
    epsilon = np.zeros((numMoisture, numFields), dtype='complex')
    soilEpsilon = epsilon

    rs = np.zeros(numElevs, dtype='complex')
    rp = np.zeros(numElevs, dtype='complex')

    mu = np.array([1.0 + 0.0j])
    depth = np.array([0.0])
    one = np.ones(1)

    numWaves = 2
    waves = np.zeros((numWaves, 4), dtype='complex')

    #  Calculate and plot bare soil reflectivities vs. moisture...
    for i in range(numFields):
        soilEpsilon[:, i] = sc.calcSoilEpsilon(c.fields[i, 0], c.fields[i, 2], moisture)
        epsilon = soilEpsilon

        #  Create reflectivity magnitude and phase plots for given field...
        fig1, axs1 = plt.subplots(2, 2)
        fig2, axs2 = plt.subplots(2, 2)
        fig3, axs3 = plt.subplots(2, 2)

        for j in range(numMoisture):
            #  Calculate ground reflection coefficients...
            rs, rp = sc.calcReflectionCoefs(np.asarray(epsilon[j, i]) * one, mu, depth, elevation)

            #  Fill results dataframe...
            dataSet[['Azimuth', 'Elevation']] = list(zip(azimuth, elevation))
            dataSet['Soil Type'] = str(i)
            dataSet['SMC'] = moisture[j]
            dataSet['Veg Type'] = 'Bare'
            dataSet[['Reflection Coef (s)', 'Reflection Coef (p)']] = list(zip(rs, rp))
            dataSet['E Direct (s)'] = (1.0 + 0.0j) * c.halfSqrtTwo * gpsAmplitude
            dataSet['E Direct (p)'] = (0.0 - 1.0j) * c.halfSqrtTwo * gpsAmplitude
            dataSet['Reflection Coef (s)'] = rs
            dataSet['Reflection Coef (p)'] = rp
            dataSet['E Reflected (s)'] = rs * dataSet['E Direct (s)'] * np.exp(-1.0j * deltaPhase[0])
            dataSet['E Reflected (p)'] = rp * dataSet['E Direct (p)'] * np.exp(-1.0j * deltaPhase[0])

            for k in range(numElevs):
                waves[0, 0] = azimuth[k]
                waves[0, 1] = elevation[k]
                waves[0, 2] = dataSet.loc[k, 'E Direct (s)']
                waves[0, 3] = dataSet.loc[k, 'E Direct (p)']
                waves[1, 0] = azimuth[k]
                waves[1, 1] = -elevation[k]
                waves[1, 2] = dataSet.loc[k, 'E Reflected (s)']
                waves[1, 3] = dataSet.loc[k, 'E Reflected (p)']

                dataSet.loc[k, 'SNR (s)'] = sc.calcReceivedSNR(sc.calcAntennaSignal(waves, antenna_s))
                dataSet.loc[k, 'SNR (p)'] = sc.calcReceivedSNR(sc.calcAntennaSignal(waves, antenna_p))
                dataSet.loc[k, 'SNR (lhc)'] = sc.calcReceivedSNR(sc.calcAntennaSignal(waves, antenna_lhc))
                dataSet.loc[k, 'SNR (rhc)'] = sc.calcReceivedSNR(sc.calcAntennaSignal(waves, antenna_rhc))

            #  Plot reflectivities vs. elevation for given field, moisture...
            legend = 'SMC = ' + str(moisture[j])
            axs1[0, 0].plot(elevation, np.absolute(dataSet['Reflection Coef (s)']), label=legend)
            axs1[0, 1].plot(elevation, np.absolute(dataSet['Reflection Coef (p)']), label=legend)
            axs1[1, 0].plot(elevation, np.absolute(0.5 * (dataSet['Reflection Coef (s)'] - dataSet['Reflection Coef (p)'])), label=legend)
            axs1[1, 1].plot(elevation, np.absolute(0.5 * (dataSet['Reflection Coef (s)'] + dataSet['Reflection Coef (p)'])), label=legend)

            axs2[0, 0].plot(elevation, np.angle(dataSet['Reflection Coef (s)']), label=legend)
            axs2[0, 1].plot(elevation, np.angle(dataSet['Reflection Coef (p)']), label=legend)
            axs2[1, 0].plot(elevation, np.angle(0.5 * (dataSet['Reflection Coef (s)'] - dataSet['Reflection Coef (p)'])), label=legend)
            axs2[1, 1].plot(elevation, np.angle(0.5 * (dataSet['Reflection Coef (s)'] + dataSet['Reflection Coef (p)'])), label=legend)

            axs3[0, 0].plot(sinEL, dataSet['SNR (s)'], label=legend)
            axs3[0, 1].plot(sinEL, dataSet['SNR (p)'], label=legend)
            axs3[1, 0].plot(sinEL, dataSet['SNR (lhc)'], label=legend)
            axs3[1, 1].plot(sinEL, dataSet['SNR (rhc)'], label=legend)

            # Append dataSet to results dataframe...
            results = pd.concat([results, dataSet])

        plotTitle1 = 'Bare Soil Reflectivity (Mag), S-Pol, Field ' + str(i+1)
        plotTitle2 = 'Bare Soil Reflectivity (Mag), P-Pol, Field ' + str(i+1)
        plotTitle3 = 'Bare Soil Reflectivity (Mag), LHC-Pol, Field ' + str(i+1)
        plotTitle4 = 'Bare Soil Reflectivity (Mag), RHC-Pol, Field ' + str(i+1)
        axs1[0, 0].set_title(plotTitle1, fontdict={'fontsize': 'small'})
        axs1[0, 1].set_title(plotTitle2, fontdict={'fontsize': 'small'})
        axs1[1, 0].set_title(plotTitle3, fontdict={'fontsize': 'small'})
        axs1[1, 1].set_title(plotTitle4, fontdict={'fontsize': 'small'})
        axs1[0, 0].set_xlabel('Elevation (deg)', fontdict={'fontsize': 'small'})
        axs1[0, 1].set_xlabel('Elevation (deg)', fontdict={'fontsize': 'small'})
        axs1[1, 0].set_xlabel('Elevation (deg)', fontdict={'fontsize': 'small'})
        axs1[1, 1].set_xlabel('Elevation (deg)', fontdict={'fontsize': 'small'})
        axs1[0, 0].set_ylabel('Reflectivity', fontdict={'fontsize': 'small'})
        axs1[0, 1].set_ylabel('Reflectivity', fontdict={'fontsize': 'small'})
        axs1[1, 0].set_ylabel('Reflectivity', fontdict={'fontsize': 'small'})
        axs1[1, 1].set_ylabel('Reflectivity', fontdict={'fontsize': 'small'})
        axs1[0, 0].tick_params(labelsize='x-small')
        axs1[0, 1].tick_params(labelsize='x-small')
        axs1[1, 0].tick_params(labelsize='x-small')
        axs1[1, 1].tick_params(labelsize='x-small')
        axs1[0, 0].legend(fontsize='xx-small')
        axs1[0, 1].legend(fontsize='xx-small')
        axs1[1, 0].legend(fontsize='xx-small')
        axs1[1, 1].legend(fontsize='xx-small')
        axs1[0, 0].grid()
        axs1[0, 1].grid()
        axs1[1, 0].grid()
        axs1[1, 1].grid()
        fig1.tight_layout()

        plotTitle1 = 'Bare Soil Reflectivity (Phase), S-Pol, Field ' + str(i+1)
        plotTitle2 = 'Bare Soil Reflectivity (Phase), P-Pol, Field ' + str(i+1)
        plotTitle3 = 'Bare Soil Reflectivity (Phase), LHC-Pol, Field ' + str(i+1)
        plotTitle4 = 'Bare Soil Reflectivity (Phase), RHC-Pol, Field ' + str(i+1)
        axs2[0, 0].set_title(plotTitle1, fontdict={'fontsize': 'small'})
        axs2[0, 1].set_title(plotTitle2, fontdict={'fontsize': 'small'})
        axs2[1, 0].set_title(plotTitle3, fontdict={'fontsize': 'small'})
        axs2[1, 1].set_title(plotTitle4, fontdict={'fontsize': 'small'})
        axs1[0, 0].set_xlabel('Elevation (deg)', fontdict={'fontsize': 'small'})
        axs1[0, 1].set_xlabel('Elevation (deg)', fontdict={'fontsize': 'small'})
        axs1[1, 0].set_xlabel('Elevation (deg)', fontdict={'fontsize': 'small'})
        axs1[1, 1].set_xlabel('Elevation (deg)', fontdict={'fontsize': 'small'})
        axs2[0, 0].set_ylabel('Reflectivity', fontdict={'fontsize': 'small'})
        axs2[0, 1].set_ylabel('Reflectivity', fontdict={'fontsize': 'small'})
        axs2[1, 0].set_ylabel('Reflectivity', fontdict={'fontsize': 'small'})
        axs2[1, 1].set_ylabel('Reflectivity', fontdict={'fontsize': 'small'})
        axs2[0, 0].tick_params(labelsize='x-small')
        axs2[0, 1].tick_params(labelsize='x-small')
        axs2[1, 0].tick_params(labelsize='x-small')
        axs2[1, 1].tick_params(labelsize='x-small')
        axs2[0, 0].legend(fontsize='xx-small')
        axs2[0, 1].legend(fontsize='xx-small')
        axs2[1, 0].legend(fontsize='xx-small')
        axs2[1, 1].legend(fontsize='xx-small')
        axs2[0, 0].grid()
        axs2[0, 1].grid()
        axs2[1, 0].grid()
        axs2[1, 1].grid()
        fig2.tight_layout()

        plotTitle1 = 'Bare Soil SNR (s), S-Pol, Field ' + str(i + 1)
        plotTitle2 = 'Bare Soil SNR (p), P-Pol, Field ' + str(i + 1)
        plotTitle3 = 'Bare Soil SNR (lhc), LHC-Pol, Field ' + str(i + 1)
        plotTitle4 = 'Bare Soil SNR (rhc), RHC-Pol, Field ' + str(i + 1)
        axs3[0, 0].set_title(plotTitle1, fontdict={'fontsize': 'small'})
        axs3[0, 1].set_title(plotTitle2, fontdict={'fontsize': 'small'})
        axs3[1, 0].set_title(plotTitle3, fontdict={'fontsize': 'small'})
        axs3[1, 1].set_title(plotTitle4, fontdict={'fontsize': 'small'})
        axs3[0, 0].set_xlabel('sin(Elevation)', fontdict={'fontsize': 'small'})
        axs3[0, 1].set_xlabel('sin(Elevation)', fontdict={'fontsize': 'small'})
        axs3[1, 0].set_xlabel('sin(Elevation)', fontdict={'fontsize': 'small'})
        axs3[1, 1].set_xlabel('sin(Elevation)', fontdict={'fontsize': 'small'})
        axs3[0, 0].set_ylabel('SNR (dB)', fontdict={'fontsize': 'small'})
        axs3[0, 1].set_ylabel('SNR (dB)', fontdict={'fontsize': 'small'})
        axs3[1, 0].set_ylabel('SNR (dB)', fontdict={'fontsize': 'small'})
        axs3[1, 1].set_ylabel('SNR (dB)', fontdict={'fontsize': 'small'})
        axs3[0, 0].tick_params(labelsize='x-small')
        axs3[0, 1].tick_params(labelsize='x-small')
        axs3[1, 0].tick_params(labelsize='x-small')
        axs3[1, 1].tick_params(labelsize='x-small')
        axs3[0, 0].legend(fontsize='xx-small')
        axs3[0, 1].legend(fontsize='xx-small')
        axs3[1, 0].legend(fontsize='xx-small')
        axs3[1, 1].legend(fontsize='xx-small')
        axs3[0, 0].grid()
        axs3[0, 1].grid()
        axs3[1, 0].grid()
        axs3[1, 1].grid()
        fig3.tight_layout()
        plt.show()
        plt.close()
        pdf.savefig(fig1)
        pdf.savefig(fig2)
        pdf.savefig(fig3)

        plt.close('all')

        print('Systems Engineering Calcs 2:  Bare soil - Field # ', i, ' plots printed...')

    #  Create vegetation types and dielectric constants...
    alfalfaEpsilon = 1.06 + 0.013j
    soyEpsilon = 1.04 + 0.004j

    #  Calculate and plot vegetation reflectivities vs. height (depth); use Hallikainen's Field 2 (loam)...

    numDepth = 2
    numVegHeight = 6
    mu = np.array([1.0 + 0.0j, 1.0 + 0.0j])
    vegHeight = np.linspace(0.0, 1.0, numVegHeight)
    depth = np.zeros((numVegHeight, numDepth))
    depth[:, 0] = vegHeight

    #  1.  Vegetation = soybeans
    epsilon = np.zeros((numMoisture, numDepth), dtype='complex')
    epsilon[:, 0] = soyEpsilon
    epsilon[:, 1] = soilEpsilon[:, 1]

    for i in range(numVegHeight):

        #  Create reflectivity magnitude plots for different vegetation heights...
        fig1, axs1 = plt.subplots(2, 2)
        fig2, axs2 = plt.subplots(2, 2)
        fig3, axs3 = plt.subplots(2, 2)

        for j in range(numMoisture):
            #  Calculate ground reflection coefficients...
            rs, rp = sc.calcReflectionCoefs(epsilon[j, :], mu, depth[i, :], elevation)

            #  Fill results dataframe...
            dataSet[['Azimuth', 'Elevation']] = list(zip(azimuth, elevation))
            dataSet['Soil Type'] = str(i)
            dataSet['SMC'] = moisture[j]
            dataSet['Veg Type'] = 'Soy'
            dataSet[['Reflection Coef (s)', 'Reflection Coef (p)']] = list(zip(rs, rp))
            dataSet['E Direct (s)'] = (1.0 + 0.0j) * c.halfSqrtTwo * gpsAmplitude
            dataSet['E Direct (p)'] = (0.0 - 1.0j) * c.halfSqrtTwo * gpsAmplitude
            dataSet['Reflection Coef (s)'] = rs
            dataSet['Reflection Coef (p)'] = rp
            dataSet['E Reflected (s)'] = rs * dataSet['E Direct (s)'] * np.exp(-1.0j * deltaPhase[0])
            dataSet['E Reflected (p)'] = rp * dataSet['E Direct (p)'] * np.exp(-1.0j * deltaPhase[0])

            for k in range(numElevs):
                waves[0, 0] = azimuth[k]
                waves[0, 1] = elevation[k]
                waves[0, 2] = dataSet.loc[k, 'E Direct (s)']
                waves[0, 3] = dataSet.loc[k, 'E Direct (p)']
                waves[1, 0] = azimuth[k]
                waves[1, 1] = -elevation[k]
                waves[1, 2] = dataSet.loc[k, 'E Reflected (s)']
                waves[1, 3] = dataSet.loc[k, 'E Reflected (p)']

                dataSet.loc[k, 'SNR (s)'] = sc.calcReceivedSNR(sc.calcAntennaSignal(waves, antenna_s))
                dataSet.loc[k, 'SNR (p)'] = sc.calcReceivedSNR(sc.calcAntennaSignal(waves, antenna_p))
                dataSet.loc[k, 'SNR (lhc)'] = sc.calcReceivedSNR(sc.calcAntennaSignal(waves, antenna_lhc))
                dataSet.loc[k, 'SNR (rhc)'] = sc.calcReceivedSNR(sc.calcAntennaSignal(waves, antenna_rhc))

            #  Plot reflectivities vs. elevation for given field, moisture...
            legend = 'SMC = ' + str(moisture[j])
            axs1[0, 0].plot(elevation, np.absolute(dataSet['Reflection Coef (s)']), label=legend)
            axs1[0, 1].plot(elevation, np.absolute(dataSet['Reflection Coef (p)']), label=legend)
            axs1[1, 0].plot(elevation, np.absolute(0.5 * (dataSet['Reflection Coef (s)'] - dataSet['Reflection Coef (p)'])), label=legend)
            axs1[1, 1].plot(elevation, np.absolute(0.5 * (dataSet['Reflection Coef (s)'] + dataSet['Reflection Coef (p)'])), label=legend)

            axs2[0, 0].plot(elevation, np.angle(dataSet['Reflection Coef (s)']), label=legend)
            axs2[0, 1].plot(elevation, np.angle(dataSet['Reflection Coef (p)']), label=legend)
            axs2[1, 0].plot(elevation, np.angle(0.5 * (dataSet['Reflection Coef (s)'] - dataSet['Reflection Coef (p)'])), label=legend)
            axs2[1, 1].plot(elevation, np.angle(0.5 * (dataSet['Reflection Coef (s)'] + dataSet['Reflection Coef (p)'])), label=legend)

            axs3[0, 0].plot(sinEL, dataSet['SNR (s)'], label=legend)
            axs3[0, 1].plot(sinEL, dataSet['SNR (p)'], label=legend)
            axs3[1, 0].plot(sinEL, dataSet['SNR (lhc)'], label=legend)
            axs3[1, 1].plot(sinEL, dataSet['SNR (rhc)'], label=legend)

            # Append dataSet to results dataframe...
            results = pd.concat([results, dataSet])

        plotTitle1 = 'Soybean Reflectivity (Mag), S-Pol, Veg Height ' + str(vegHeight[i])
        plotTitle2 = 'Soybean Reflectivity (Mag), P-Pol, Veg Height ' + str(vegHeight[i])
        plotTitle3 = 'Soybean Reflectivity (Mag), LHC-Pol, Veg Height ' + str(vegHeight[i])
        plotTitle4 = 'Soybean Reflectivity (Mag), RHC-Pol, Veg Height ' + str(vegHeight[i])
        axs1[0, 0].set_title(plotTitle1, fontdict={'fontsize': 'small'})
        axs1[0, 1].set_title(plotTitle2, fontdict={'fontsize': 'small'})
        axs1[1, 0].set_title(plotTitle3, fontdict={'fontsize': 'small'})
        axs1[1, 1].set_title(plotTitle4, fontdict={'fontsize': 'small'})
        axs1[0, 0].set_xlabel('Elevation (deg)', fontdict={'fontsize': 'small'})
        axs1[0, 1].set_xlabel('Elevation (deg)', fontdict={'fontsize': 'small'})
        axs1[1, 0].set_xlabel('Elevation (deg)', fontdict={'fontsize': 'small'})
        axs1[1, 1].set_xlabel('Elevation (deg)', fontdict={'fontsize': 'small'})
        axs1[0, 0].set_ylabel('Reflectivity', fontdict={'fontsize': 'small'})
        axs1[0, 1].set_ylabel('Reflectivity', fontdict={'fontsize': 'small'})
        axs1[1, 0].set_ylabel('Reflectivity', fontdict={'fontsize': 'small'})
        axs1[1, 1].set_ylabel('Reflectivity', fontdict={'fontsize': 'small'})
        axs1[0, 0].tick_params(labelsize='x-small')
        axs1[0, 1].tick_params(labelsize='x-small')
        axs1[1, 0].tick_params(labelsize='x-small')
        axs1[1, 1].tick_params(labelsize='x-small')
        axs1[0, 0].legend(fontsize='xx-small')
        axs1[0, 1].legend(fontsize='xx-small')
        axs1[1, 0].legend(fontsize='xx-small')
        axs1[1, 1].legend(fontsize='xx-small')
        axs1[0, 0].grid()
        axs1[0, 1].grid()
        axs1[1, 0].grid()
        axs1[1, 1].grid()
        fig1.tight_layout()

        plotTitle1 = 'Soybean Reflectivity (Phase), S-Pol, Veg Height ' + str(vegHeight[i])
        plotTitle2 = 'Soybean Reflectivity (Phase), P-Pol, Veg Height ' + str(vegHeight[i])
        plotTitle3 = 'Soybean Reflectivity (Phase), LHC-Pol, Veg Height ' + str(vegHeight[i])
        plotTitle4 = 'Soybean Reflectivity (Phase), RHC-Pol, Veg Height ' + str(vegHeight[i])
        axs2[0, 0].set_title(plotTitle1, fontdict={'fontsize': 'small'})
        axs2[0, 1].set_title(plotTitle2, fontdict={'fontsize': 'small'})
        axs2[1, 0].set_title(plotTitle3, fontdict={'fontsize': 'small'})
        axs2[1, 1].set_title(plotTitle4, fontdict={'fontsize': 'small'})
        axs2[0, 0].set_xlabel('Elevation (deg)', fontdict={'fontsize': 'small'})
        axs2[0, 1].set_xlabel('Elevation (deg)', fontdict={'fontsize': 'small'})
        axs2[1, 0].set_xlabel('Elevation (deg)', fontdict={'fontsize': 'small'})
        axs2[1, 1].set_xlabel('Elevation (deg)', fontdict={'fontsize': 'small'})
        axs2[0, 0].set_ylabel('Reflectivity', fontdict={'fontsize': 'small'})
        axs2[0, 1].set_ylabel('Reflectivity', fontdict={'fontsize': 'small'})
        axs2[1, 0].set_ylabel('Reflectivity', fontdict={'fontsize': 'small'})
        axs2[1, 1].set_ylabel('Reflectivity', fontdict={'fontsize': 'small'})
        axs2[0, 0].tick_params(labelsize='x-small')
        axs2[0, 1].tick_params(labelsize='x-small')
        axs2[1, 0].tick_params(labelsize='x-small')
        axs2[1, 1].tick_params(labelsize='x-small')
        axs2[0, 0].legend(fontsize='xx-small')
        axs2[0, 1].legend(fontsize='xx-small')
        axs2[1, 0].legend(fontsize='xx-small')
        axs2[1, 1].legend(fontsize='xx-small')
        axs2[0, 0].grid()
        axs2[0, 1].grid()
        axs2[1, 0].grid()
        axs2[1, 1].grid()
        fig2.tight_layout()

        plotTitle1 = 'Soybean SNR (s), S-Pol, Veg Height ' + str(vegHeight[i])
        plotTitle2 = 'Soybean SNR (p), P-Pol, Veg Height ' + str(vegHeight[i])
        plotTitle3 = 'Soybean SNR (lhc), LHC-Pol, Veg Height ' + str(vegHeight[i])
        plotTitle4 = 'Soybean SNR (rhc), RHC-Pol, Veg Height ' + str(vegHeight[i])
        axs3[0, 0].set_title(plotTitle1, fontdict={'fontsize': 'small'})
        axs3[0, 1].set_title(plotTitle2, fontdict={'fontsize': 'small'})
        axs3[1, 0].set_title(plotTitle3, fontdict={'fontsize': 'small'})
        axs3[1, 1].set_title(plotTitle4, fontdict={'fontsize': 'small'})
        axs3[0, 0].set_xlabel('sin(Elevation)', fontdict={'fontsize': 'small'})
        axs3[0, 1].set_xlabel('sin(Elevation)', fontdict={'fontsize': 'small'})
        axs3[1, 0].set_xlabel('sin(Elevation)', fontdict={'fontsize': 'small'})
        axs3[1, 1].set_xlabel('sin(Elevation)', fontdict={'fontsize': 'small'})
        axs3[0, 0].set_ylabel('SNR (dB)', fontdict={'fontsize': 'small'})
        axs3[0, 1].set_ylabel('SNR (dB)', fontdict={'fontsize': 'small'})
        axs3[1, 0].set_ylabel('SNR (dB)', fontdict={'fontsize': 'small'})
        axs3[1, 1].set_ylabel('SNR (dB)', fontdict={'fontsize': 'small'})
        axs3[0, 0].tick_params(labelsize='x-small')
        axs3[0, 1].tick_params(labelsize='x-small')
        axs3[1, 0].tick_params(labelsize='x-small')
        axs3[1, 1].tick_params(labelsize='x-small')
        axs3[0, 0].legend(fontsize='xx-small')
        axs3[0, 1].legend(fontsize='xx-small')
        axs3[1, 0].legend(fontsize='xx-small')
        axs3[1, 1].legend(fontsize='xx-small')
        axs3[0, 0].grid()
        axs3[0, 1].grid()
        axs3[1, 0].grid()
        axs3[1, 1].grid()
        fig3.tight_layout()
        plt.show()
        plt.close()
        pdf.savefig(fig1)
        pdf.savefig(fig2)
        pdf.savefig(fig3)

        plt.close('all')

        print('Systems Engineering Calcs 2:  Soybeans - Veg Height # ', i, ' plots printed...')

    #  2.  Vegetation = alfalfa
    epsilon = np.zeros((numMoisture, numDepth), dtype='complex')
    epsilon[:, 0] = alfalfaEpsilon
    epsilon[:, 1] = soilEpsilon[:, 1]

    for i in range(numVegHeight):

        #  Create reflectivity magnitude plots for different vegetation heights...
        fig1, axs1 = plt.subplots(2, 2)
        fig2, axs2 = plt.subplots(2, 2)
        fig3, axs3 = plt.subplots(2, 2)

        for j in range(numMoisture):
            rs, rp = sc.calcReflectionCoefs(epsilon[j, :], mu, depth[i, :], elevation)

            #  Fill results dataframe...
            dataSet[['Azimuth', 'Elevation']] = list(zip(azimuth, elevation))
            dataSet['Soil Type'] = str(i)
            dataSet['SMC'] = moisture[j]
            dataSet['Veg Type'] = 'Alfalfa'
            dataSet[['Reflection Coef (s)', 'Reflection Coef (p)']] = list(zip(rs, rp))
            dataSet['E Direct (s)'] = (1.0 + 0.0j) * c.halfSqrtTwo * gpsAmplitude
            dataSet['E Direct (p)'] = (0.0 - 1.0j) * c.halfSqrtTwo * gpsAmplitude
            dataSet['Reflection Coef (s)'] = rs
            dataSet['Reflection Coef (p)'] = rp
            dataSet['E Reflected (s)'] = rs * dataSet['E Direct (s)'] * np.exp(-1.0j * deltaPhase[0])
            dataSet['E Reflected (p)'] = rp * dataSet['E Direct (p)'] * np.exp(-1.0j * deltaPhase[0])

            for k in range(numElevs):
                waves[0, 0] = azimuth[k]
                waves[0, 1] = elevation[k]
                waves[0, 2] = dataSet.loc[k, 'E Direct (s)']
                waves[0, 3] = dataSet.loc[k, 'E Direct (p)']
                waves[1, 0] = azimuth[k]
                waves[1, 1] = -elevation[k]
                waves[1, 2] = dataSet.loc[k, 'E Reflected (s)']
                waves[1, 3] = dataSet.loc[k, 'E Reflected (p)']

                dataSet.loc[k, 'SNR (s)'] = sc.calcReceivedSNR(sc.calcAntennaSignal(waves, antenna_s))
                dataSet.loc[k, 'SNR (p)'] = sc.calcReceivedSNR(sc.calcAntennaSignal(waves, antenna_p))
                dataSet.loc[k, 'SNR (lhc)'] = sc.calcReceivedSNR(sc.calcAntennaSignal(waves, antenna_lhc))
                dataSet.loc[k, 'SNR (rhc)'] = sc.calcReceivedSNR(sc.calcAntennaSignal(waves, antenna_rhc))

            #  Plot reflectivities vs. elevation for given field, moisture...
            legend = 'SMC = ' + str(moisture[j])
            axs1[0, 0].plot(elevation, np.absolute(dataSet['Reflection Coef (s)']), label=legend)
            axs1[0, 1].plot(elevation, np.absolute(dataSet['Reflection Coef (p)']), label=legend)
            axs1[1, 0].plot(elevation, np.absolute(0.5 * (dataSet['Reflection Coef (s)'] - dataSet['Reflection Coef (p)'])), label=legend)
            axs1[1, 1].plot(elevation, np.absolute(0.5 * (dataSet['Reflection Coef (s)'] + dataSet['Reflection Coef (p)'])), label=legend)

            axs2[0, 0].plot(elevation, np.angle(dataSet['Reflection Coef (s)']), label=legend)
            axs2[0, 1].plot(elevation, np.angle(dataSet['Reflection Coef (p)']), label=legend)
            axs2[1, 0].plot(elevation, np.angle(0.5 * (dataSet['Reflection Coef (s)'] - dataSet['Reflection Coef (p)'])), label=legend)
            axs2[1, 1].plot(elevation, np.angle(0.5 * (dataSet['Reflection Coef (s)'] + dataSet['Reflection Coef (p)'])), label=legend)

            axs3[0, 0].plot(sinEL, dataSet['SNR (s)'], label=legend)
            axs3[0, 1].plot(sinEL, dataSet['SNR (p)'], label=legend)
            axs3[1, 0].plot(sinEL, dataSet['SNR (lhc)'], label=legend)
            axs3[1, 1].plot(sinEL, dataSet['SNR (rhc)'], label=legend)

            # Append dataSet to results dataframe...
            results = pd.concat([results, dataSet])

        plotTitle1 = 'Alfalfa Reflectivity (Mag), S-Pol, Veg Height ' + str(vegHeight[i])
        plotTitle2 = 'Alfalfa Reflectivity (Mag), P-Pol, Veg Height ' + str(vegHeight[i])
        plotTitle3 = 'Alfalfa Reflectivity (Mag), LHC-Pol, Veg Height ' + str(vegHeight[i])
        plotTitle4 = 'Alfalfa Reflectivity (Mag), RHC-Pol, Veg Height ' + str(vegHeight[i])
        axs1[0, 0].set_title(plotTitle1, fontdict={'fontsize': 'small'})
        axs1[0, 1].set_title(plotTitle2, fontdict={'fontsize': 'small'})
        axs1[1, 0].set_title(plotTitle3, fontdict={'fontsize': 'small'})
        axs1[1, 1].set_title(plotTitle4, fontdict={'fontsize': 'small'})
        axs1[0, 0].set_xlabel('Elevation (deg)', fontdict={'fontsize': 'small'})
        axs1[0, 1].set_xlabel('Elevation (deg)', fontdict={'fontsize': 'small'})
        axs1[1, 0].set_xlabel('Elevation (deg)', fontdict={'fontsize': 'small'})
        axs1[1, 1].set_xlabel('Elevation (deg)', fontdict={'fontsize': 'small'})
        axs1[0, 0].set_ylabel('Reflectivity', fontdict={'fontsize': 'small'})
        axs1[0, 1].set_ylabel('Reflectivity', fontdict={'fontsize': 'small'})
        axs1[1, 0].set_ylabel('Reflectivity', fontdict={'fontsize': 'small'})
        axs1[1, 1].set_ylabel('Reflectivity', fontdict={'fontsize': 'small'})
        axs1[0, 0].tick_params(labelsize='x-small')
        axs1[0, 1].tick_params(labelsize='x-small')
        axs1[1, 0].tick_params(labelsize='x-small')
        axs1[1, 1].tick_params(labelsize='x-small')
        axs1[0, 0].legend(fontsize='xx-small')
        axs1[0, 1].legend(fontsize='xx-small')
        axs1[1, 0].legend(fontsize='xx-small')
        axs1[1, 1].legend(fontsize='xx-small')
        axs1[0, 0].grid()
        axs1[0, 1].grid()
        axs1[1, 0].grid()
        axs1[1, 1].grid()
        fig1.tight_layout()

        plotTitle1 = 'Alfalfa Reflectivity (Phase), S-Pol, Veg Height ' + str(vegHeight[i])
        plotTitle2 = 'Alfalfa Reflectivity (Phase), P-Pol, Veg Height ' + str(vegHeight[i])
        plotTitle3 = 'Alfalfa Reflectivity (Phase), LHC-Pol, Veg Height ' + str(vegHeight[i])
        plotTitle4 = 'Alfalfa Reflectivity (Phase), RHC-Pol, Veg Height ' + str(vegHeight[i])
        axs2[0, 0].set_title(plotTitle1, fontdict={'fontsize': 'small'})
        axs2[0, 1].set_title(plotTitle2, fontdict={'fontsize': 'small'})
        axs2[1, 0].set_title(plotTitle3, fontdict={'fontsize': 'small'})
        axs2[1, 1].set_title(plotTitle4, fontdict={'fontsize': 'small'})
        axs2[0, 0].set_xlabel('Elevation (deg)', fontdict={'fontsize': 'small'})
        axs2[0, 1].set_xlabel('Elevation (deg)', fontdict={'fontsize': 'small'})
        axs2[1, 0].set_xlabel('Elevation (deg)', fontdict={'fontsize': 'small'})
        axs2[1, 1].set_xlabel('Elevation (deg)', fontdict={'fontsize': 'small'})
        axs2[0, 0].set_ylabel('Reflectivity', fontdict={'fontsize': 'small'})
        axs2[0, 1].set_ylabel('Reflectivity', fontdict={'fontsize': 'small'})
        axs2[1, 0].set_ylabel('Reflectivity', fontdict={'fontsize': 'small'})
        axs2[1, 1].set_ylabel('Reflectivity', fontdict={'fontsize': 'small'})
        axs2[0, 0].tick_params(labelsize='x-small')
        axs2[0, 1].tick_params(labelsize='x-small')
        axs2[1, 0].tick_params(labelsize='x-small')
        axs2[1, 1].tick_params(labelsize='x-small')
        axs2[0, 0].legend(fontsize='xx-small')
        axs2[0, 1].legend(fontsize='xx-small')
        axs2[1, 0].legend(fontsize='xx-small')
        axs2[1, 1].legend(fontsize='xx-small')
        axs2[0, 0].grid()
        axs2[0, 1].grid()
        axs2[1, 0].grid()
        axs2[1, 1].grid()
        fig2.tight_layout()

        plotTitle1 = 'Alfalfa SNR (s), S-Pol, Veg Height ' + str(vegHeight[i])
        plotTitle2 = 'Alfalfa SNR (p), P-Pol, Veg Height ' + str(vegHeight[i])
        plotTitle3 = 'Alfalfa SNR (lhc), LHC-Pol, Veg Height ' + str(vegHeight[i])
        plotTitle4 = 'Alfalfa SNR (rhc), RHC-Pol, Veg Height ' + str(vegHeight[i])
        axs3[0, 0].set_title(plotTitle1, fontdict={'fontsize': 'small'})
        axs3[0, 1].set_title(plotTitle2, fontdict={'fontsize': 'small'})
        axs3[1, 0].set_title(plotTitle3, fontdict={'fontsize': 'small'})
        axs3[1, 1].set_title(plotTitle4, fontdict={'fontsize': 'small'})
        axs3[0, 0].set_xlabel('sin(Elevation)', fontdict={'fontsize': 'small'})
        axs3[0, 1].set_xlabel('sin(Elevation)', fontdict={'fontsize': 'small'})
        axs3[1, 0].set_xlabel('sin(Elevation)', fontdict={'fontsize': 'small'})
        axs3[1, 1].set_xlabel('sin(Elevation)', fontdict={'fontsize': 'small'})
        axs3[0, 0].set_ylabel('SNR (dB)', fontdict={'fontsize': 'small'})
        axs3[0, 1].set_ylabel('SNR (dB)', fontdict={'fontsize': 'small'})
        axs3[1, 0].set_ylabel('SNR (dB)', fontdict={'fontsize': 'small'})
        axs3[1, 1].set_ylabel('SNR (dB)', fontdict={'fontsize': 'small'})
        axs3[0, 0].tick_params(labelsize='x-small')
        axs3[0, 1].tick_params(labelsize='x-small')
        axs3[1, 0].tick_params(labelsize='x-small')
        axs3[1, 1].tick_params(labelsize='x-small')
        axs3[0, 0].legend(fontsize='xx-small')
        axs3[0, 1].legend(fontsize='xx-small')
        axs3[1, 0].legend(fontsize='xx-small')
        axs3[1, 1].legend(fontsize='xx-small')
        axs3[0, 0].grid()
        axs3[0, 1].grid()
        axs3[1, 0].grid()
        axs3[1, 1].grid()
        fig3.tight_layout()
        plt.show()
        plt.close()
        pdf.savefig(fig1)
        pdf.savefig(fig2)
        pdf.savefig(fig3)

        plt.close('all')

        print('Systems Engineering Calcs 2:  Alfalfa - Veg Height # ', i, ' plots printed...')

