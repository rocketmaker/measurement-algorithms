#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 10 10:17:27 2017

@author: Greg Wirth
"""

import csv
import os
import numpy as np
import matplotlib.pyplot as plt
import sys
from divirod3 import *

# build list of input files...
def getResultsFiles( iSeries, setName, maxPeaks=1000):
    '''
    Given a series number (iSeries) and set name (setName), 
    return list of results files.
    '''
    inFileList = list()
    for iPeak in range(maxPeaks):
        prefix = '/Users/gwirth/tmp/data/series_%d_set_%s_peak_%d' % (iSeries, setName, iPeak)
        resultsFile = "%s-results.csv" % prefix
        if os.path.isfile( resultsFile):
            inFileList.append(resultsFile)
        
    return inFileList
    
def getXcString(xc):
    '''
    Given a floating point value for xc, return an appropriately rounded string representation.
    '''
    v = float(xc)
    xcString = "%.4f" % v
    return xcString
    
def getAllData( inFileList):
    '''
    Given list of results files, read in all of the results into a list.
    '''
    allData = list()
    for resultsFile in inFileList:
        with open( resultsFile) as f:
            reader = csv.DictReader(f)
            for row in reader:
                outcome = int(float(row['outcome']))
                if outcome == 1:
                    outRow = dict()
                    outRow['xcString'] = getXcString(row['xc'])
                    
                    # convert remaining items to float...
                    for key in row:
                        outRow[key] = float(row[key])
                    allData.append(outRow)
                    
    return allData
    
def listOfDictsToArray( allDataList, key):
    '''
    Given a list of dicts, extract all elements for the given key and return as a numpy array.
    '''
    
    # allocate an array of appropriate size...
    a = list()
    
    # loop over keys...
    for row in allDataList:
        a.append( row[key])
        
    return np.array(a)
    
def listOfDictsToList( allDataList, key):
    '''
    Given a list of dicts, extract all elements for the given key and return as a list.
    '''
    
    # allocate an array of appropriate size...
    a = list()
    
    # loop over keys...
    for row in allDataList:
        a.append(row[key])
        
    return a
    
def getArray( key, allDataList, xc):
    '''
    Given the data list (allDataList), a bin name (xc), and a key (key),
    return array of data for key.
    '''
    
    bufList = list()
    
    for row in allDataList:
        if row['xcString'] == xc:
            bufList.append( row[key])
    
    # convert list to numpy arrays...
    buf = np.array(bufList)
    return buf
    
def relativePhase( phase):
    '''
    Given phase array (phase), return phase relative to min value.
    '''
    
    # make sure phases are all on range...
    p2 = fixPhaseRange( phase)
    
    phaseMin = np.amin(p2)
    return p2 - phaseMin
    
def getPhaseChange( iSeries, setName, time0, plot=False):
    '''
    Given a series number (iSeries), set name (setName), and a time (t0),
    return time and phase change relative to min value.
    '''
    
    # get filenames...
    inFileList = getResultsFiles( iSeries, setName)

    # read data...
    allDataList = getAllData(inFileList)

    # extract the xcstring field...
    xcStringList = listOfDictsToList( allDataList, 'xcString')
    xcListUniq = sorted(list(set(xcStringList)))
    
    # create output arrays...
    n = len(xcListUniq)
    phaseArray = np.zeros(n)
    azArray = np.zeros(n)
    elArray = np.zeros(n)

    # loop over bins in this peak...
    for i in range(n):
        
        xc = xcListUniq[i]
        
        # get the input arrays...
        time  = getArray('time', allDataList, xc)
        az    = getArray('az', allDataList, xc)
        el    = getArray('el', allDataList, xc)
        phase = getArray('phaseDeg', allDataList, xc)
        
        # turn phase to relative value...
        if plot:
            plotPhaseRange(phase)
        deltaPhase = relativePhase(phase)

        # interpolate to determine phase at time time0...
        p0 = myInterpolate( time0, time, deltaPhase)
        phaseArray[i] = p0
        
        # get the average az/el...
        azArray[i] = np.mean(az)
        elArray[i] = np.mean(el)
        
        if plot:
            if p0 is None:
                print("No plot (p0 is not valid)")
            else:
                plt.plot( time, deltaPhase)
                plt.title ("Series=%d Set=%s xc=%s t0=%.2f p0=%2f" % (iSeries, setName, xc, time0, p0))  
                plt.axvline( x=time0, color='r')
                plt.axhline( y=p0, color='r')
                plt.show()
        
    return phaseArray, azArray, elArray
    
def myInterpolate( x0, x, y):
    '''
    Given arrays (x,y), use linear interpolation to 
    locate value at x0.  For points that lie outside 
    of range, return None.
    '''
    
    if x0 < np.amin(x) or x0 > np.amax(x):
        y0 = None
    else:
        y0 = np.interp(x0, x, y)
        
    return y0
    
import time
start_time = time.time()

def getGoodSeries( csvFile, verbose=False):
    '''
    Return list of good series and sets.
    '''
    
    # allocate output lists...
    goodSeries = list()
    goodSet = list()

    # read CSV file...
    with open( csvFile, 'r') as f:
        reader = csv.reader( f)
        
        # loop over rows...
        for inRow in reader:
            goodSeries.append(int(inRow[0]))
            goodSet.append(inRow[1])
        
    if verbose:
        for a,b in zip(goodSeries,goodSet):
            print(a,b)
            
    return goodSeries,goodSet
    
def generateMapDataFile( time0):

    H0= 317.930632444 # cm
    csvFile = 'goodSeries.csv'

    # write the mapdata file...
    mapDataFile = '/Users/gwirth/tmp/data/mapData_time_%.3f.csv' % time0
    with open( mapDataFile, 'w') as f:
        w = csv.writer(f)
        w.writerow(['phase','az','el','x','y'])

        goodSeries,goodSet = getGoodSeries( csvFile, verbose=False)
        for iSeries,setName in zip(goodSeries,goodSet):
            phaseArray, azArray, elArray = getPhaseChange( iSeries, setName, time0, plot=False)
            for phase,az,el in zip(phaseArray, azArray, elArray):
        
                # convert az/el to x/y...
                x,y =  AzEl2XY(az,el,H0) 
            
                # store results...
                w.writerow([phase,az,el,x,y])
                
for t in range(1,15):
    print("time=", t)
    generateMapDataFile(t)
    
end_time = time.time()
print("execution time was %.1f " % ( end_time - start_time))
