#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
----------------------
Process Matlab Data 1.py
----------------------

Divirod, 20 Mar 17, M. Estes

This script:  
1)  Reads in GPS data from the Matlab file that Javier created from the first prototype sensors.  Data is
    over-sampled at every 1 second; only read in every nth data points (sample at n seconds)

2)  Copies that data into a pandas dataframe, gpsData:
    
    Columns:            Time    EL    AZ    SNR
    Row indexes:        Series #, Set #, Sample #
    Rows:               only active data
    
3)  Filters out "good" data in a pandas dataframe, gpsGoodData, where:
    
    3 deg < EL < 75 deg
    100 deg < AZ < 260 deg
    Avg SNRin a set > 30 dB
    No fliers:  10 dB < SNR < 100 dB

4)  Decimates azimuth and elevation data by finding the mid-pints of data steps and fitting the data to a
    cubic spline

5)  Linearizes (from dB) and fits the DC average of SNR data

6)  Normalized out the antenna response from the linear SNR data

7)  Decimates the linear SNR data by finding the mid-points of data steps

8)  Fits the sinusoidal frequency of the decimated SNR data points to find H0 (mean) and Heff (time-varying)

9)  Fits the sinusoidal DC offset, amplitude, and phase of the decimated SNR data points to find its time-varying
    amplitude and phase

10) Map time-varying SNR fringe phase to reflection coefficient phase and then to SMC

11) Plots contour maps of SMC, reflection coefficients, Heff, SNR amplitude, SNR phase

12) Saves plots and processed data to files
    
"""

import numpy as np
import pandas as pd
import scipy.io as sio
from scipy.interpolate import interp1d
import scipy.signal as sig
from scipy.optimize import curve_fit, minimize
from numpy import pi
from math import sin, cos, exp, asin
import matplotlib.pyplot as plt

def findSets (seriesNum, series):
    """
    Given a data series (pandas dataframe), finds the sample indexes at which the sets 
    in the series start and stop.  The method returns a new index tuple array
    with the values for 'Set #' filled in for each row.  Set numbers start at 0.
    
    The method finds set boundaries by finding the sample points at which both 
    the satellite azimuth position and the sample timestamps both have large jumps.  
    The method returns the updated series.
    """
    #seriesNum = series.index.values[0][0]

    #  Set threshold values for delta time and azimuth...
    dtThresh = 3000         #  Seconds
    daThresh = 5           #  Degrees

    #  Calculate delta time and azimuth arrays...
    t1 = series.loc[(seriesNum, 0), 'Time'].as_matrix()
    numPts = len(t1)
    t2 = t1[1:]
    t1 = t1[:numPts-1]
    dt = t2 - t1
    
    a1 = series.loc[(seriesNum, 0), 'AZ'].as_matrix()
    a1 = a1.astype(int)
    a2 = a1[1:]
    a1 = a1[:numPts-1]
    da = np.absolute(a2 - a1)
    
    #  Find indexes where both dt > dtThresh and da > daThresh...
    indexes = np.where(((dt >= dtThresh) & (da >= daThresh)) | (dt < 0))

    #  Create index list of set numbers...
    setNums = np.zeros(0, dtype=int)
    numSets = len(indexes[0])
    
    last = -1

    for i in range(numSets):
        oneSet = np.zeros(indexes[0][i] - last)
        oneSet[:] = i
        setNums = np.concatenate([setNums, oneSet])
        last = indexes[0][i]
        
    lastSet = np.zeros(numPts - last)
    lastSet[:] = numSets
    setNums = np.concatenate([setNums, lastSet])
    
    #  Update series index columns...
    seriesNums = np.zeros(numPts)
    seriesNums[:] = seriesNum
    sampleNums = np.arange(numPts)
    rowIndexes = zip(seriesNums, setNums, sampleNums)
    seriesIndex = pd.MultiIndex.from_tuples(list(rowIndexes), names=('Series #', 'Set #', 'Sample #'))
    
    return seriesIndex

def createSeries (seriesNum, numPts):
    """
    Creates a new pandas dataframe to contain a series.  seriesNum is the 
    series label.  Method returns the single series dataframe.
    """
    #  Set default set number label...
    setNum = 0
    
    #  Create Pandas dataframe...
    colNames = ['Time', 'EL', 'AZ', 'SNR']
    rowNames = [[int(seriesNum)],[int(setNum)], np.arange(numPts)]
    rowIndex = pd.MultiIndex.from_product(rowNames, names=('Series #', 'Set #', 'Sample #'))
    data = pd.DataFrame(np.zeros((numPts, 4)), index= rowIndex, columns=colNames)
    
    return data

def filterSeries (seriesNum, series):
    """
    Filters the dataframe passed as rawData and returns a pandas dataframe
    with the filtered data
    """
    
    #  Filter Values...
    elMin = 3.0
    elMax = 75.0
    azMin = 100.0
    azMax = 260.0
    snrMin = 10.0
    snrMax = 100.0
    avgSNRmin = 30.0
        
    #  Filter each set for avg SNR...
    numSets = int(series.index.get_level_values(1).max())+1
    
    filteredData = series
    for i in range(numSets):
        if filteredData.loc[(seriesNum, i), 'SNR'].mean() < avgSNRmin:
            filteredData = filteredData.drop(i, level=1)
    
    #  Filter for Azimuth, Elevation, and "fliers"...
    filteredData = filteredData[(filteredData['EL'] >= elMin) & (filteredData['EL'] <= elMax) & 
                                (filteredData['AZ'] >= azMin) & (filteredData['AZ'] <= azMax) &
                                (filteredData['SNR'] >= snrMin) & (filteredData['SNR'] <= snrMax) &
                                (filteredData['Time'] > 0)]

    return filteredData

def smoothAzEl_spline (seriesNum, series):
    """
    Smooths the azimuth and elevation data by fitting it to a cubic polynomial.
    series is one series of the filtered dataframe.  This method
    breaks the data up into sets
    """

    #  Set up thresholds for azimuth and elevation angle changes...
    dAzThresh = 1
    dElThresh = 1

    azFit = np.zeros(0)
    elFit = np.zeros(0)

    #  Loop over sets of data...
    setNums = series.index.get_level_values(1).unique()

    for i in setNums:

        #  Get set data...
        t = series.loc[(seriesNum, i),'Time'].as_matrix()
        numPts = t.shape[0]
        setStartIndex = series.loc[(seriesNum, i)].index.get_level_values(0)[0]

        az1 = series.loc[(seriesNum, i), 'AZ'].as_matrix()
        az2 = az1[1:]
        az1 = az1[:numPts - 1]
        dAz = np.absolute(az2 - az1)

        el1 = series.loc[(seriesNum, i), 'EL'].as_matrix()
        el2 = el1[1:]
        el1 = el1[:numPts - 1]
        dEl = np.absolute(el2 - el1)

        #  Find indexes where transitions occur...
        azIndexes = np.where(dAz >= dAzThresh)
        elIndexes = np.where(dEl >= dElThresh)

        numAzTransitions = len(azIndexes[0])
        numElTransitions = len(elIndexes[0])

        tAz = np.zeros(numAzTransitions)
        yAz = np.zeros(numAzTransitions)
        tEl = np.zeros(numElTransitions)
        yEl = np.zeros(numElTransitions)

        #  Create arrays with AZ and EL transition points (t, y)...
        for j in range(numAzTransitions):
            t1 = series.loc[(seriesNum, i, setStartIndex + azIndexes[0][j]), 'Time']
            t2 = series.loc[(seriesNum, i, setStartIndex + azIndexes[0][j] + 1), 'Time']
            tAz[j] = 0.5*(t1 + t2)
            az1 = series.loc[(seriesNum, i, setStartIndex + azIndexes[0][j]), 'AZ']
            az2 = series.loc[(seriesNum, i, setStartIndex + azIndexes[0][j] + 1), 'AZ']
            yAz[j] = 0.5*(az1 + az2)

        for j in range(numElTransitions):
            t1 = series.loc[(seriesNum, i, setStartIndex + elIndexes[0][j]), 'Time']
            t2 = series.loc[(seriesNum, i, setStartIndex + elIndexes[0][j] + 1), 'Time']
            tEl[j] = 0.5*(t1 + t2)
            el1 = series.loc[(seriesNum, i, setStartIndex + elIndexes[0][j]), 'EL']
            el2 = series.loc[(seriesNum, i, setStartIndex + elIndexes[0][j] + 1), 'EL']
            yEl[j] = 0.5*(el1 + el2)

        #  Special Cases:
        #  1) If all values are identical (no transitions), just return input
        #  2) If only one transition, return line with zero slope at mid-point of transition
        #  3) Otherwise, perform spline interpolation
        if numAzTransitions == 0:
            az = series.loc[(seriesNum, i), 'AZ']
        elif numAzTransitions == 1:
            az = np.zeros(numPts, dtype=float)
            az[:] = 0.5*(series.loc[(seriesNum, i), 'AZ'].max() + series.loc[(seriesNum, i), 'AZ'].min())

        # Define interpolation function and interpolate to points of set...
        else:
            #  Add starting point that extrapolates existing slope...
            t0 = series.loc[(seriesNum, i, setStartIndex), 'Time']

            if tAz[0] > t0:
                az0 = (t0 - tAz[0]) * ((yAz[1] - yAz[0]) / (tAz[1] - tAz[0])) + yAz[0]
                tAz = np.insert(tAz, 0, t0)
                yAz = np.insert(yAz, 0, az0)

            # Add ending point that extrapolates existing slope...
            tLast = series.loc[(seriesNum, i, setStartIndex + numPts - 1), 'Time']

            if tAz[numAzTransitions] < tLast:
                azLast = (tLast - tAz[numAzTransitions]) * ((yAz[numAzTransitions] - yAz[numAzTransitions - 1])
                                                            / (tAz[numAzTransitions] - tAz[numAzTransitions - 1])) + yAz[numAzTransitions]
                tAz = np.append(tAz, tLast)
                yAz = np.append(yAz, azLast)

            fAz = interp1d(tAz, yAz, kind='cubic')
            az = fAz(t)

        if numElTransitions == 0:
            el = series.loc[(seriesNum, i), 'EL']
        elif numElTransitions == 1:
            el = np.zeros(numPts, dtype=float)
            el[:] = 0.5 * (series.loc[(seriesNum, i), 'EL'].max() + series.loc[(seriesNum, i), 'EL'].min())

        # Define interpolation function and interpolate to points of set...
        else:
            #  Add starting point that extrapolates existing slope...
            t0 = series.loc[(seriesNum, i, setStartIndex), 'Time']

            if tEl[0] > t0:
                el0 = (t0 - tEl[0]) * ((yEl[1] - yEl[0]) / (tEl[1] - tEl[0])) + yEl[0]
                tEl = np.insert(tEl, 0, t0)
                yEl = np.insert(yEl, 0, el0)

            # Add ending point that extrapolates existing slope...
            tLast = series.loc[(seriesNum, i, setStartIndex + numPts - 1), 'Time']

            if tEl[numElTransitions] < tLast:
                elLast = (tLast - tEl[numElTransitions]) * ((yEl[numElTransitions] - yEl[numElTransitions - 1])
                                                            / (tEl[numElTransitions] - tEl[numElTransitions - 1])) + yEl[numElTransitions]
                tEl = np.append(tEl, tLast)
                yEl = np.append(yEl, elLast)

            fEl = interp1d(tEl, yEl, kind='cubic')
            el = fEl(t)

        azFit = np.concatenate((azFit, az))
        elFit = np.concatenate((elFit, el))

    smoothAzEl = pd.DataFrame({'AZ Smooth': azFit, 'EL Smooth': elFit}, index=series.index)

    return smoothAzEl

def linearizeSNR (setData):
    """
    Given one set of SNR data, convert the SNR data (in dB) into linearized form.
    :param seriesNum:   series number of data
    :param setNum:      set number of data
    :param setData:     one set of dataframe containing SNR data
    :return: linearSet: pandas dataframe of linearized SNR data
    """

    #  Convert from dB to linear SNR...
    linSNR = np.power(10.0, setData['SNR'].values / 20.0)

    return linSNR

def calcReflectionSpot (setData):
    """
    Given one set of linearized SNR data, calculate the Fresnel zone spot radius (from sensor), diameter (perpendicular
    to the radius), and length (along the radius).
    :param seriesNum:   series number of data
    :param setNum:      set number of data
    :param setData:     one set of dataframe containing SNR data
    :return: spotData:  a (numPts x 3) numpy array of spot distance, diameter, and length
    """

    #  Constants...
    h = 2.16                #  Height of sensor (m)
    dSat = 2.02E+7          #  Orbital height of satellite (m)
    c = 2.998E+8            #  Speed of light (m/s)
    freq = 1.575E+9         #  Frequency of GPS carrier (Hz)
    wavelength = c / freq   #  Microwave wavelength (m)

    #  Calculate reflection spot distance (along ground)...
    rSpot = h / np.tan(pi / 180.0 * setData['EL Smooth'].values)

    #  Calculate reflection spot diameter and length...
    dSensor = h / np.sin(pi / 180.0 * setData['EL Smooth'].values)
    fzDia = 2.0 * np.sqrt(wavelength * dSat * dSensor / (dSat + dSensor))
    fzLen = fzDia / np.sin(pi / 180.0 * setData['EL Smooth'].values)

    numPts = setData.shape[0]
    spotData = np.zeros((numPts, 3))
    spotData[:, 0] = rSpot
    spotData[:, 1] = fzDia
    spotData[:, 2] = fzLen

    return spotData

def calcH0 (setData):
    """
    Given a data set, use the Lomb-Scargle periodogram method to calculate the frequency of max power and use this
    value to calculate the average sensor height, H0.
    :param seriesNum:   series number of data
    :param setNum:      set number of data
    :param setData:     one set of dataframe containing SNR data
    :return: h0Data:    a numpy (numPts x 1) array of H0 values (one value per data point)
    """

    #  Constants...
    aMin = 1
    aMax = 200
    aStep = 0.1
    c = 2.998E+8            #  Speed of light (m/s)
    freq = 1.575E+9         #  Frequency of GPS carrier (Hz)
    wavelength = c / freq   #  Microwave wavelength (m)
    degToRad = pi / 180.0       #  Converts angle in degrees to radians

    #  Set up data arrays...
    sinEL = np.sin(degToRad * setData['EL Smooth'].values)
    power = setData['SNR Linear'].values - setData['SNR Average'].values
    freq = np.arange(aMin, aMax, aStep)

    #  Check for flat elevation data (return if so)...
    if sinEL.min() == sinEL.max():
        return np.nan

    #  Get periodogram...
    pdGram = sig.lombscargle(sinEL, power, freq)

    #  Find peak frequency...
    iMax = np.argmax(pdGram)
    fMax = freq[iMax]

    #  Calculate H0...
    H0 = fMax * wavelength / (4.0 * pi)

    return H0

def calcHeff (xData, yData, window):
    """
    Given a data chunk (subset of set, use the Lomb-Scargle periodogram method to calculate the effective frequency of
    max power and use this value to calculate the effective sensor height, Heff.
    :param xData:           numpy array (numPts x 1) of smoothed elevation data
    :param yData:           numpy array (numPts x 1) of smoothed, zero-offset linear SNR data
    :return: Heff:          numpy array (numPts x 1) of H0 values (one value per data point)
    """
    #  Constants...
    aMin = 1
    aMax = 200
    aStep = 0.1
    c = 2.998E+8                #  Speed of light (m/s)
    freq = 1.575E+9             #  Frequency of GPS carrier (Hz)
    wavelength = c / freq       #  Microwave wavelength (m)
    degToRad = pi / 180.0       #  Converts angle in degrees to radians

    #  Set up data arrays...
    sinEL = np.sin(degToRad * xData)
    power = yData
    freq = np.arange(aMin, aMax, aStep)

    #  Check for flat elevation data (return if so)...
    if sinEL.min() == sinEL.max():
        return np.nan

    #  Get periodogram...
    pdGram = sig.lombscargle(sinEL, power, freq)

    #  Find peak frequency...
    iMax = np.argmax(pdGram)
    fMax = freq[iMax]

    #  Calculate Heff...
    Heff = fMax * wavelength / (4.0 * pi)

    return Heff

def sinusoidModel (x, amp, freq, phase):
    """
    Given an array of independent values (x) and parameters (amp, freq, phase), computes a sinusoid.
    :return:    An array of dependent (y) values
    """

    return amp * np.sin(freq * x + phase)

def calcFitChiSq (guess, xData, yData, freq):
    """

    :param xData:
    :param yData:
    :return:
    """

    #  Calculate model fit...
    amp = guess[0]
    phase = guess[1]
    yModel = sinusoidModel(xData, amp, freq, phase)

    #  Calculate chi squared...
    numPts = yData.shape[0]
    chiSq = np.sum((yModel - yData)**2) / numPts

    return chiSq

def calcAmpPhaseFits (setData):
    """
    Given a set of data, calculates the nonlinear fit of the linear SNR vs. sin(EL) to sinusoidal segments.  Calculates
    sinusoid amplitude and phase as fitting parameters at each data point, and fits segments of one wavelength.
    :param seriesNum:   series number of data
    :param setNum:      set number of data
    :param setData:     one set of dataframe containing SNR data
    :return:  fitData:  a (numPts x 2) ndarray with best fit amplitude and phase at each data point
    """

    #  Constants...
    c = 2.998E+8                #  Speed of light (m/s)
    freq = 1.575E+9             #  Frequency of GPS carrier (Hz)
    wavelength = c / freq       #  Microwave wavelength (m)
    degToRad = pi / 180.0       #  Converts angle in degrees to radians
    radToDeg = 1.0 / degToRad   #  Converts angle in radians to degrees
    ampMin = 1.0                #  Linear sinusoidal fringe amplitude, upper bound for fit (V)
    ampMax = 100.               #  Linear sinusoidal fringe amplitude, upper bound for fit (V)
    freqMin = 100.              #  Fringe frequency, lower bound for fit (rad/sin(EL))
    freqMax = 200.              #  Fringe frequency, upper bound for fit (rad/sin(EL))
    phaseMin = -pi              #  Fringe phase, lower bound for fit (rad)
    phaseMax = pi               #  Fringe phase, upper bound for fit (rad)

    #  Get average interferogram frequency from H0...
    avgFreq = 4.0 * pi * setData['H0'].mean() / wavelength

    #  Calculate fringe wavelength (in units of sinEL)...
    #waveFringe = wavelength * sinEL / (setData['H0'].values * (1.0 - np.cos(2.0 * degToRad * setData['EL Smooth'].values)))
    waveFringe = 0.06
    freqFringe = 1.0 / waveFringe

    #  Find start and end points (1/2 waveFringe from each end of data set)...
    minEL = setData['EL Smooth'].min()
    maxEL = setData['EL Smooth'].max()
    startElev = radToDeg * asin(sin(degToRad * minEL) + 0.5 * waveFringe)
    endElev = radToDeg * asin(sin(degToRad * maxEL) - 0.5 * waveFringe)
    startPt = (np.abs(setData['EL Smooth'] - startElev)).argmin()
    endPt = (np.abs(setData['EL Smooth'] - endElev)).argmin()

    #  Make startPt the first point in time...
    if startPt > endPt:
        start = endPt
        endPt = startPt
        startPt = start

    #  Check if data set is less than one fringe wavelength long...
    if (sin(degToRad * maxEL) - sin(degToRad * minEL)) < waveFringe:
        print('calcAmpPhaseFits:  EL range too small')
        return np.nan

    # Loop over data, every n points, fitting one fringe cycle at each nth point...

    #  Set up data arrays for processing...
    n = 10
    numPts = setData.shape[0]
    numJ = int(numPts / float(n))
    firstPt = setData.index.get_level_values(0).min()
    fitParams = np.zeros((numPts, 2))
    yFit = np.zeros((numJ, 1))
    chiSq = np.zeros((numJ, 1))
    heff = np.zeros((numJ, 1))
    j = 0

    for i in range(startPt, endPt+1, n):

        #  Data index doesn't start at 0 - create index that does...
        # j = i - setData.index.get_level_values(0).min()

        #  Calculate first and last point of the data segment to fit...
        currentElev = setData.loc[(i), 'EL Smooth']
        startElev = radToDeg * asin(sin(degToRad * currentElev) - 0.5 * waveFringe)
        endElev = radToDeg * asin(sin(degToRad * currentElev) + 0.5 * waveFringe)
        startFringe = (np.abs(setData['EL Smooth'] - startElev)).argmin()
        endFringe = (np.abs(setData['EL Smooth'] - endElev)).argmin()

        #  Make startPt the first point in time...
        if startFringe > endFringe:
            start = endFringe
            endFringe = startFringe
            startFringe = start

        #  Get x and y data to fit...
        data = setData.loc[startFringe:endFringe]
        xData = np.sin(degToRad * data['EL Smooth'].values)
        yData = data['SNR Smooth'].values - data['SNR Average'].values
        xmidPt = int(0.5*(endFringe - startFringe))
        # heff[j] = calcHeff(xData, yData)
        # localFreq = 4.0 * pi * heff[j] / wavelength

        #  Set initial guess at fit parameters (amp, freq, phase)...
        if np.sum(fitParams[j-1]) != 0 and j >= 1:
            initialGuess = fitParams[j-1]
        else:
            if yData[xmidPt] > 0.0:
                phase = pi / 2.0
            else:
                phase = -pi / 2.0
            initialGuess = np.array([40.0, phase])

        #  Fit data to sinusoidal model...
        try:
            res = minimize(calcFitChiSq, initialGuess, args=(xData, yData, avgFreq), method='nelder-mead', options={'xtol': 1e-8, 'disp': False})
            # pfit, pcov = curve_fit(sinusoidModel, xData, yData, p0=initialGuess, bounds=([ampMin, freqMin, phaseMin], [ampMax, freqMax, phaseMax]))

        except RuntimeError:
            print('calcAmpPhaseFits:  RuntimeError - least squares minimization failed.')
        except ValueError:
            print('calcAmpPhaseFits:  ValueError - either xdata or ydata contain NaNs or incompatible options used.')
        else:
            # print('calcAmpPhaseFits:  Curve fit successfully.')

            #yFit[j] = sinusoidModel(sin(degToRad * setData.loc[(i), 'EL Smooth']), pfit[0], pfit[1], pfit[2])
            #fitParams[j] = pfit

            yFit[j] = sinusoidModel(sin(degToRad * setData.loc[(i), 'EL Smooth']), res.x[0], avgFreq, res.x[1])
            fitParams[j] = [res.x[0], res.x[1]]
            chiSq[j] = calcFitChiSq (res.x, xData, yData, avgFreq)

            # print('Point ', i, ' with fit params: ', fitParams[j], ' startFringe = ', startFringe, ' endFringe = ', endFringe)

        j += 1

    # Interpolate between fit params to fill in all data points...
    numJused = j
    results = np.zeros((numPts, 5))
    i = (startPt - firstPt) + n

    for j in range(1, numJused):

        #  Create interpolation functions...
        ampInterp = interp1d([j - 1, j], [fitParams[j - 1, 0], fitParams[j, 0]])
        phaseInterp = interp1d([j - 1, j], [fitParams[j - 1, 1], fitParams[j, 1]])
        yInterp = interp1d([j - 1, j], [yFit[j - 1, 0], yFit[j, 0]])
        chiSqInterp = interp1d([j - 1, j], [chiSq[j - 1, 0], chiSq[j, 0]])

        #  Create fractional x values to interpolate between successive j values...
        xNew = np.linspace(j-1, j, n)

        #  Interpolate parameters...
        results[i-n:i, 0] = ampInterp(xNew)
        results[i-n:i, 1] = phaseInterp(xNew)
        results[i-n:i, 2] = yInterp(xNew)
        results[i-n:i, 3] = chiSqInterp(xNew)
        results[i-n:i, 4] = 0.0

        i += n

    return results


#----------------
#   Main Loop
#----------------

#  Read in the Matlab file...
infile = 'feis.mat'
mat = sio.loadmat(infile)
blob = mat['S']

#  Get number of series in dataset...
numSeries = len(blob['time'][0,:])
#numSeries = 3

print('Matlab read complete.  NumSeries = ', numSeries)

gpsData = createSeries(0, 0)
gpsGoodData = gpsData

#  Copy every series to a pandas dataframe (gpsData)...
for i in range(numSeries):

    #  Create series i...
    numPts = blob['active'][0,i][0,0]
    
    series = createSeries(i, numPts)
    series['Time'] = blob['time'][0,i][:numPts,0]
    series['EL'] = blob['el'][0,i][:numPts,0]
    series['AZ'] = blob['az'][0,i][:numPts,0]
    series['SNR'] = blob['snr'][0,i][:numPts,0]
    
    print('Series', i, 'created.')

    #  Add series raw data to gpsData...
    gpsData = pd.concat([gpsData, series], axis=0)

    print('Data added to gpsData.')

    #  Filter data...
    filteredData = filterSeries(i, series)    
    
    print('Series data filtered.')

    numFiltPts = filteredData.shape[0]
    
    #  Find and label sets...
    if numFiltPts:
        filteredData.index = findSets(i, filteredData)
    
        print('Sets created.')
    
        #  Smooth azimuth and elevation data...
        #fitAzEl = smoothAzEl_polynomial(i, filteredData)
        fitAzEl = smoothAzEl_spline(i, filteredData)
        filteredData = filteredData.join(fitAzEl)

        print('AZ and EL data smoothed.')

        # Add filteredData series to gpsGoodData...
        gpsGoodData = pd.concat([gpsGoodData, filteredData], axis=0)

        print('Filtered data added to gpsGoodData.')

#  Start transforming and fitting SNR data...
seriesNums = gpsGoodData.index.get_level_values(0).unique()
#seriesNums = seriesNums[:6]

#  Create new columns in gpsGoodData...
gpsGoodData['Time Days'] = np.nan
gpsGoodData['SNR Linear'] = np.nan
gpsGoodData['SNR Average'] = np.nan
gpsGoodData['SNR Smooth'] = np.nan
gpsGoodData['Spot Distance'] = np.nan
gpsGoodData['Spot Diameter'] = np.nan
gpsGoodData['Spot Length'] = np.nan
gpsGoodData['H0'] = np.nan
gpsGoodData['Amplitude'] = np.nan
gpsGoodData['Phase'] = np.nan
gpsGoodData['SNR Fit'] = np.nan
gpsGoodData['Chi Squared'] = np.nan
gpsGoodData['Heff'] = np.nan

#  Create time column in units of days...
gpsGoodData['Time Days'] = gpsGoodData['Time'] / (3600. * 24.)

print('Time (days) created.')

for i in seriesNums:

    setNums = gpsGoodData.loc[(i)].index.get_level_values(0).unique()

    #  Select set numbers to calculate...
    #if max(setNums) > 15:
    #    setNums = setNums[-6:]
    #else:
    #    setNums = setNums[-3:]

    #setNums = setNums[:2]

    for j in setNums:
        setData = gpsGoodData.loc[(i, j)]

        #  Linearize SNR in set...
        gpsGoodData.loc[(i,j), 'SNR Linear'] = linearizeSNR(setData)

        #  Fit DC average to SNR in set...
        gpsGoodData.loc[(i,j), 'SNR Average'] = fitSNRAvg(setData)

        print('Series ', i, ', set ', j, ' linearized and averaged.')

        #  Filter (smooth) SNR data...
        gpsGoodData.loc[(i,j), 'SNR Smooth'] = filterSNR(setData)

        #  Calculate reflection spot distance, diameter, length..
        gpsGoodData.loc[(i,j), ['Spot Distance', 'Spot Diameter', 'Spot Length']] = calcReflectionSpot(setData)

        print('Series ', i, ', set ', j, ' spot distance, diameter, and length calculated.')

        #  Calculate effective height, H0...
        gpsGoodData.loc[(i, j), 'H0'] = calcH0(setData)

        print('Series ', i, ', set ', j, ' H0 calculated.')

        #  Calculate amplitude and phase fits...
        gpsGoodData.loc[(i,j), ['Amplitude', 'Phase', 'SNR Fit', 'Chi Squared', 'Heff']] = calcAmpPhaseFits(setData)

        print('Series ', i, ', set ', j, ' set amplitude and phase fits calculated.')

#  Save gpsGoodData to .csv file...
datasetName = 'Feis'
filename = 'gpsGoodData ' + datasetName + ' Dataset.csv'

gpsGoodData.to_csv(filename)
