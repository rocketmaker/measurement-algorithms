"""
---------------------
    KML_Test.py
---------------------

Divirod, 14 Apr 17, M. Estes

This script tests the simplekml package and the Matplotlib_Simplekml_Overlays.py wrapper functions
for generating Google Earth data overlays.
"""

import numpy as np
import matplotlib.pyplot as plt
from math import pi
from Constants import Constants
from simplekml import (Kml, OverlayXY, ScreenXY, Units, RotationXY,
                       AltitudeMode, Camera)
from Matplotlib_Simplekml_Overlays import PltToSimpleKML

c = Constants()
kplt = PltToSimpleKML()


#  Set up geometric variables...
num_el = 500
num_az = 40
h = 3.0
lon0, lat0 = -105.176821, 40.026373

az = np.linspace(30.0, 330., num_az)
min_el = c.degToRad * 3.
max_el = c.degToRad * 75.
sin_el = np.linspace(np.sin(min_el), np.sin(max_el), num_el)
el = np.arcsin((sin_el))
tan_el = np.sin(el)
r = h / tan_el

#  Create test data...
lat = np.zeros((num_az, num_el))
lon = np.zeros((num_az, num_el))
snr = np.zeros((num_az, num_el))

for a in range(num_az):
    snr[a,:] = np.sin(4.*pi*h/c.gpsWavelength * sin_el) + 2.

    for e in range(num_el):
        lat[a,e] = lat0 + r[e]*np.cos(c.degToRad*az[a]) / c.ONE_DEG_LAT_M
        lon[a,e] = lon0 + r[e]*np.sin(c.degToRad*az[a]) / c.ONE_DEG_LON_M

#  Create matplotlib figure and axes objects...
fig, ax = kplt.gearth_fig(llcrnrlon=lon.min(),
                          llcrnrlat=lat.min(),
                          urcrnrlon=lon.max(),
                          urcrnrlat=lat.max(),
                          pixels=1024)

#  Plot test data on contour map...
ax.pcolormesh(lon, lat, snr)

#  Save test data plot as .png image file...
fig.savefig('overlay1.png', transparent=True, format='png')

#  Create .kml file for Google Earth...
kplt.make_kml(llcrnrlon=lon.min(), llcrnrlat=lat.min(),
              urcrnrlon=lon.max(), urcrnrlat=lat.max(),
              figs=['overlay1.png'], kmlfile='munson_overlay1.kml',
              altitude=1.E+2, tilt=0.)

#  Close the plot objects...
ax.clear()
fig.clear()
plt.close()